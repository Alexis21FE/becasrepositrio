﻿<?php session_start();?><?php
//session_start();
	include("z_script/db_class.php");
$_SESSION["var_ban"]=6;


if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Colegio") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
	
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}

$band_error_msg=(isset ($_GET["red_msg"]) ? $_GET["red_msg"]: "");
$band_green_msg=(isset ($_GET["green_msg"]) ? $_GET["green_msg"]: "");
$band_modf_msg=(isset ($_GET["modf_msg"]) ? $_GET["modf_msg"]: "");	

$x_tip_bus=''; if(!empty($_POST['tip_bus'])) { $x_tip_bus=$_POST['tip_bus'];}
$query_ciclo="Select * from ciclos";
$res=$pdo->query($query_ciclo);
foreach ($res as $key => $value) {
  $ciclo_config=$value['ciclo_config'];
}

if($ciclo_config=='ac'){
  $table="inf_alum";
  $mes_estado_cc="mes in(8,9,10,11,12,1,2,3,4,5,6)";
  $mes_pago="mes_pago in(8,9,10,11,12,1,2,3,4,5,6)";
}else{
  $table="inf_alum_pp";
  $mes_estado_cc="mes in(8,9,10,11,12,1,2)";
  $mes_pago="mes_pago in(8,9,10,11,12,1,2)";
}
$_SESSION['table']=$table;
$_SESSION['ciclo_config']=$ciclo_config;
	
	extract($_POST, EXTR_PREFIX_ALL, "x");

	
	
	//paginador
	if(isset($_GET['page'])) //verifica pagina
	{
    	$page= $_GET['page'];
	}else{
   	 	$page=1;
	}
	
	if(isset($x_busc)){
		
		if($x_tip_bus == NULL && $x_input_bus == NULL){
		
			$consulta="SELECT * FROM ".$table." where alum_colg='Universidad Panamericana'";
			
		}
		else 
			if($x_tip_bus == "candidato"){
				$consulta="SELECT * FROM ".$table." where estado_alumno='candidato' ";
		
			}
			else 
				if($x_tip_bus == "sin_validacion"){
					$consulta="SELECT * FROM ".$table." where estado_alumno='sin_validacion' ";
		
				}
				else 
					if($x_tip_bus == "no_candidato"){
						$consulta="SELECT * FROM ".$table." where estado_alumno='no_candidato' ";		
					}						
	}else if(isset($x_resg_busc)){
		
		$consulta="SELECT * FROM ".$table." where alum_colg='Universidad Panamericana'";
		
	}else{
	
		$consulta="SELECT * FROM ".$table." where alum_colg='Universidad Panamericana'";
		
	}
	
	$datos=$pdo->query($consulta);
	$num_rows=$datos->rowCount();
	$rows_per_page= 50;
	$lastpage= ceil($num_rows / $rows_per_page);
	$page=(int)$page;
 	$band_del=0;
	if($page > $lastpage)
	{
    	$page= $lastpage;
	}
 
	if($page < 1)
	{
    	$page=1;
	}
  
	$limit= 'LIMIT '. ($page -1) * $rows_per_page . ',' .$rows_per_page;
	$consulta .=" $limit";
	$peliculas=$pdo->query($consulta);
	
	////

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php
            include_once("z_script/header.php");
        ?>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
		<div class="container_12">

          <div style="clear:both;"></div>
            
          
                
                <!-- Notification boxes -->
                
                <?php
					if($band_green_msg=="display_mod"){
						
					?>
                    <span class="notification n-success"> Alumno modificado exitosamente.</span>
                    <?php
					}
					?>	
                
                <?php
					if($band_error_msg=="display"){
						
					?>
                    <span class="notification n-success"> Alumno eliminado exitosamente.</span>
                    <?php
					}
					?>	
                
              <div class="bottom-spacing">
                
                  <div class="float-left">
                  		<form action="reporte_up_col.php" method="post" enctype="multipart/form-data">
                        <table width="517" border="0" cellspacing="5">
                          <tr>
                          	<td width="137">
                            	<select id="tip_bus" name="tip_bus">
                                    <option value="" selected="selected">Selecciona..</option>
                                    <option value="candidato">Candidato</option>
                                    <option value="sin_validacion">Sin Validación</option>
                                    <option value="no_candidato">No Candidato</option>
                              	</select>
                            </td>
                            <!--<td width="142"><input name="input_bus" id="input_bus" type="text" /></td>-->
                            <td width="100">
                            	<input class="submit-green" style="width:90px; height:30px"  name="busc" id="busc" type='submit'  value="Buscar"/>
                            </td>
                            <td width="102">
                            <?php if($x_tip_bus != NULL){ ?>
                         	 
                           <input class="submit-green" style="width:90px; height:30px"  name="resg_busc" id="resg_busc" type='submit'  value="Regresar" />
                             
							<?php }?>
                            </td>
                          </tr>
                        </table>
					</form>
                  		
                		
                </div>
				
				
				
				 <div class="float-right">
                <table width="224" border="0" cellspacing="5">
                  <tr>
					<td width="155"><a href="nusuario_up.php">                     
                    <input class="submit-green" style="width:117px; height:30px"  name="enviar" id="enviar" type='submit'  value="Nuevo Usuario" />                    </a></td>     
                          </tr>
                        </table>
                </div>
				
				
                  
                <div class="float-right">
                <table width="224" border="0" cellspacing="5">
                  <tr>
											  
                      <!--      <td width="155"><a href="actions/al_up_ex.php">
                     
                            <input class="submit-green" style="width:90px; height:30px"  name="enviar" id="enviar" type='submit'  value="Exportar" />-->
                     
                  </a></td>
                           
                          </tr>
                        </table>
           		  
                           
                </div>
              </div>
                  
                <!-- Example table -->
                <div class="module">
                	<h2><span>Alumnos UP</span></h2>
                    
                    <div class="module-table-body">
                    	<form action="">
                        <table id="myTable" class="tablesorter">
                        	
                                <tr>
				    <th width="7%">Id Familia</th>
                    <th width="8%">Matrícula</th>
                    <th width="14%">Nombre del Alumno</th>
                    <th width="20%">Nombre de Familia</th>
                    <th width="18%">Carrera</th>
                    <th width="12%">Opciones</th> 
                                </tr>
                                
                            <?php
								if($page == 1){
								
								$band_id=1;
								
								}else{
								
								$band_id=($page * $rows_per_page) - ($rows_per_page) + 1;
								
								}
							
							
								foreach ($peliculas as $row){
								$x_id_fam		= $row["id_fam"];	
								$matricula		= $row["alum_mat"];
								$nombre_alumno		= $row["alum_name"];
								$nombre_fam		= $row["alum_ap"]." ".$row["alum_am"];
								$carrera		= $row["carrera"];		
								
								$sql_bie="SELECT * FROM carreras WHERE carrera='$carrera'"; 
								$result_bie=$pdo->query($sql_bie);								
								foreach ($result_bie as $row){
											$ncarrera	= $row["nombre"];
								}
								if($carrera=='')
									$ncarrera="";
								?>
                            <tbody>
                                <tr>
                                   
									<td><?php echo $x_id_fam;?></td>
									<td><?php echo $matricula;?></td>	
                                    <td><?php echo $nombre_alumno;?></td>
                                    <td><?php echo utf8_encode($nombre_fam);?></td>
                                    <td><?php echo $ncarrera;?></td>
                                    
                                    <td>
									
                                    	<a href="al_update_up_col.php?id=<?php echo $x_id_fam;?>&matricula=<?php echo $matricula;?>"><img src="images/bin.gif" width="16" height="16" alt="delete" /></a>
                                     <a href="actions/al_del_up.php?id=<?php echo $x_id_fam;?>&matricula=<?php  echo $matricula;?>"><img src="images/minus-circle.gif" width="16" height="16" alt="not published" /></a>
									
                                    </td>
                                    
                                </tr>
                            </tbody>
                          <?php 
						  		$band_id++; 
						   }  
						   ?>
                        </table>
                        </form>
                    	<div style="clear: both"></div>
                  </div> <!-- End .module-table-body -->
                </div> <!-- End .module -->
                <div style="float:left;">
                Modificar: <img src="images/bin.gif" width="16" height="16" alt="Modificar" /> &nbsp; &nbsp; Eliminar: <img src="images/minus-circle.gif" width="16" height="16" alt="delete" />
                </div>
                <div class="pagination">           
                		 <?php
							//UNA VEZ Q MUESTRO LOS DATOS TENGO Q MOSTRAR EL BLOQUE DE PAGINACIÓN SIEMPRE Y CUANDO HAYA MÁS DE UNA PÁGINA
							if($num_rows != 0)
							{
							   $nextpage= $page +1;
							   $prevpage= $page -1;
						?>
						<?php
						//SI ES LA PRIMERA PÁGINA DESHABILITO EL BOTON DE PREVIOUS, MUESTRO EL 1 COMO ACTIVO Y MUESTRO EL RESTO DE PÁGINAS
								  if($page == 1) 
								   {
									?>
                       <!-- <a href="" class="button"><span><img src="images/arrow-180-small.gif"  height="9" width="12" alt="Previous" /> Anterior</span></a>-->
                        <div class="numbers">
                            <span>Paginas:</span> 
                            <span class="current">1</span> 
                            <span>|</span>
                            
                            <?php
							  for($i= $page+1; $i<= $lastpage ; $i++)
							  {
							?> 
                            	<a href="report_up_col.php?page=<?php echo $i;?>"><?php echo $i;?></a> 
                            	<span>|</span>
                            <?php 
							  }
							?>
                            
                            <?php
                             //Y SI LA ULTIMA PÁGINA ES MAYOR QUE LA ACTUAL MUESTRO EL BOTON NEXT O LO DESHABILITO
										if($lastpage >$page )
										{
									?>    
                               </div>        
                       	  <a href="report_up_col.php?page=<?php echo $nextpage;?>" class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> 
                         <?php
										}else{
									?>
                       					</div>
                                   			<!-- <a class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> -->
                        <?php
										}
									} else{
									//EN CAMBIO SI NO ESTAMOS EN LA PÁGINA UNO HABILITO EL BOTON DE PREVIUS Y MUESTRO LAS DEMÁS
								?>
                       <a href="report_up_col.php?page=<?php echo $prevpage;?>" class="button"><span><img src="images/arrow-180-small.gif"  height="9" width="12" alt="Previous" /> Anterior</span></a>
                      <div class="numbers"> 
                      <span>Paginas:</span>
                      
                        <?php
										 for($i= 1; $i<= $lastpage ; $i++)
										 {
													   //COMPRUEBO SI ES LA PÁGINA ACTIVA O NO
											if($page == $i)
											{
								?>     
                                  
                            
                                <span class="current"><a><?php echo $i;?></a></span><span>|</span>
                                
                                <?php
											}
											else
											{
								?>
                                
                                    
                                <a href="report_up_col.php?page=<?php echo $i;?>"><?php echo $i;?></a>
                                <span>|</span>
                                
                                <?php
											}
										}
								?>
                                
                                <?php		
										 //Y SI NO ES LA ÚLTIMA PÁGINA ACTIVO EL BOTON NEXT     
										if($lastpage >$page )
										{   
								?>   
                            </div>   
                       <a href="report_up_col.php?page=<?php echo $nextpage;?>" class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> 
                      
                                <?php
										}
									 else
										{
								?>  
                                   </div> 
                                   <!--<a class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> -->
                                   
								   <?php
										}
								}     
							  }
							?>
                            
                       <div style="clear: both;"></div> 
                     </div>
                
                

                
			</div> <!-- End .grid_12 -->
                
            <!-- Categories list --><!-- End .grid_6 -->
            
            <!-- To-do list --><!-- End .grid_6 -->
          <div style="clear:both;"></div>
            
            <!-- Form elements --><!-- End .grid_12 -->
                
            <!-- Settings-->
            <div class="grid_6">
                 <!-- End .module -->
            </div> <!-- End .grid_6 -->
                
            <!-- Password --><!-- End .grid_6 -->
          <div style="clear:both;"></div><!-- End .grid_3 --><!-- End .grid_3 --><!-- End .grid_6 -->

            
          <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
		
         <?php include_once("z_script/footer.php") ?>
	</body>
</html>