<?php

session_start();
if($_SESSION["tipo_priv"]!="Administrador"){
	header("location: index.php");
}

                   
include("z_script/db_class.php");
$id_user = $_GET["id"];
$sql="SELECT * FROM adm_users WHERE id_user='".$id_user."'"; 				

$result=$pdo->query($sql);

foreach($result as $row){ 
$name 	= $row["name"]; 
$ape_pat 	= $row["apellido_p"];
$ape_mat 	= $row["apellido_m"];
$us_coleg	= $row["colegio"];
$tip_priv	= $row["tipo_priv"];
$usuario	= $row["user"];
$password	= $row["pass"];

}
				
					

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
    <script src="js/vald_info.js" type="text/javascript"></script>
	
		<?php
            include_once("z_script/header.php");
        ?>
        <style type="text/css">
		div.module table td  {
		background-color: #ffffff;
		padding: 5px;
		border-right: 0px solid #ffffff;
		}

		div.module table {
			width: 0%;
			margin: 0 0 10px 0;
			border-left: 0px solid #d9d9d9;
			border-bottom: 0px solid #d9d9d9;
			
			}
		</style>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
	<div class="container_12">
            <!-- Dashboard icons -->
             <!-- End .grid_7 -->
             <!-- Account overview -->
            <div class="grid_5">
                
                <div style="clear:both;"></div>
            </div> <!-- End .grid_5 -->
            
            <div style="clear:both;"></div>
             <div class="grid_12">
                <div class="module">
                
                  <h2><span>Modificar Usuario: <?php echo ($name)." ". ($ape_pat) ." ". ($ape_mat) ;?></span></h2>
                    <div class="module-table-body">
                    <form name="mod_usuarios" method="post" action="actions/users_mof.php?id=<?php echo $id_user; ?>" enctype="multipart/form-data" >
                    
                     <table width="500" border="0" style="border-left: 0px solid #d9d9d9; border-bottom: 0px solid #d9d9d9; border-right: 0px sold #ffffff; margin-left:10px; margin-top:10px;">
                          <tr>
                            <td width="189">Nombre:</td>
                            <td width="195"><input id="name" name="name" value="<?php echo ($name);?>"/></td>
                            <td width="812"><div id="msg_red_<?php echo $x_cont_msg=0; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
                          <tr>
                            <td width="189">Apellido Paterno:</td>
                            <td width="195"><input id="ape_pat" name="ape_pat" value="<?php echo ($ape_pat);?>"/></td>
                            <td width="812"><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
                          <tr>
                            <td width="189">Apellido Materno:</td>
                            <td width="195"><input id="ape_mat" name="ape_mat" type="text" value="<?php echo ($ape_mat);?>" /></td>
                            <td width="812"><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
                          <tr>
                            <td>Colegio:</td>
                            <td><input name="us_coleg" id="us_coleg" type="text" value="<?php echo ($us_coleg);?>" /></td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
                          <tr>
                            <td>Privilegios:</td>
                            <td><select name="tip_priv" id="tip_priv">
                            		<option value="">Selecciona</option>
                                    <option value="Administrador"  <?php if($tip_priv=="Administrador"){echo "selected='selected'";} ?> >Administrador</option>
                                   
                                    <option value="Colegio" <?php if($tip_priv=="Colegio"){echo "selected='selected'";} ?>>Colegio</option>
                                    <option value="Coordinador" <?php if($tip_priv=="Coordinador"){echo "selected='selected'";} ?>>Coordinador</option>
                                    
                            	</select></td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
                          <div id="celda" style="display:none;">
                          <tr >
                          
                          <td>
                          Usuario:
                          </td>
                          <td><input type="text" id="usuario" name="usuario" value="<?php echo ($usuario);?>"/></td>
                          <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
                          </div>
                          <tr>
                          
                            <td>Nueva Contraseña:</td>
                            <td><input type="password" name="password" id="password" /></td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
                          <tr>
                            <td>Repite Contraseña:</td>
                            <td><input type="password" name="new_pass" id="new_pass"/></td>
                            <td><div id="msg_red_<?php $x_cont_msg++; echo $x_cont_msg; ?>" class="notification-input ni-error" style="display:none;"></div></td>
                          </tr>
                          
                     </table>
                    
                     <fieldset>
                            <input class="submit-green" style="width:100px; height:30px; float:left; margin-left:12px;"  name="enviar" id="enviar" type='button'  value="Aceptar" onclick="return val_mod()" />
                            </fieldset>
                     
				 </form>
                 </div> 
                     <!-- End .module-body -->
                </div>  <!-- End .module -->
                
        		<div style="clear:both;"></div>
            </div> <!-- End .grid_12 -->

          <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
		
        <?php include_once("z_script/footer.php") ?>   
        
	</body>
</html>