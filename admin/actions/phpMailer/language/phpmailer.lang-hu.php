<?php
/**
 * PHPMailer language file.
 * Hungarian Version
 */

$PHPMAILER_LANG = array();

$PHPMAILER_LANG["provide_address"]      = 'Meg kell adnod legal&aacute;bb egy ' .
                                          'c&iacute;mzett email c&iacute;met.';
$PHPMAILER_LANG["mailer_not_supported"] = ' levelezõ nem t&aacute;mogatott.';
$PHPMAILER_LANG["execute"]              = 'Nem tudtam v&eacute;grehajtani: ';
$PHPMAILER_LANG["instantiate"]          = 'Nem sikerült p&eacute;ld&aacute;nyos&iacute;tani a mail funkci&oacute;t.';
$PHPMAILER_LANG["authenticate"]         = 'SMTP Hiba: Sikertelen autentik&aacute;ci&oacute;.';
$PHPMAILER_LANG["from_failed"]          = 'Az al&aacute;bbi Felad&oacute; c&iacute;m hib&aacute;s: ';
$PHPMAILER_LANG["recipients_failed"]    = 'SMTP Hiba: Az al&aacute;bbi ' .
                                          'c&iacute;mzettek hib&aacute;sak: ';
$PHPMAILER_LANG["data_not_accepted"]    = 'SMTP Hiba: Nem elfogadhat&oacute; adat.';
$PHPMAILER_LANG["connect_host"]         = 'SMTP Hiba: Nem tudtam csatlakozni az SMTP host-hoz.';
$PHPMAILER_LANG["file_access"]          = 'Nem sikerült el&eacute;rni a következõ f&aacute;jlt: ';
$PHPMAILER_LANG["file_open"]            = 'F&aacute;jl Hiba: Nem sikerült megnyitni a következõ f&aacute;jlt: ';
$PHPMAILER_LANG["encoding"]             = 'Ismeretlen k&oacute;dol&aacute;s: ';
$PHPMAILER_LANG["signing"]              = 'Signing Error: ';

?>
