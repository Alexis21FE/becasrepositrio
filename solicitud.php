<?php 
ini_set("session.cookie_lifetime","7200");
ini_set("session.gc_maxlifetime","7200");
session_start();  
include 'admin/z_script/db_class.php';
$tipo_beca=$_SESSION['tipo_beca'];
$usuario=$_SESSION['usuario'];
$ciclo_e=date('Y');
$table=$_SESSION['table'];
$sql_donar_beca="Select * from user_fam where fam_user='".$usuario."'";
$sql_termino_solicitud="Select * from solc_term where usuario='".$usuario."'";
$res_solicitud_end=$pdo->query($sql_termino_solicitud);
$cuenta=$res_solicitud_end->rowCount();
$sql_carreras_up="Select * from carreras order by nombre asc";
$res_carreras_up=$pdo->query($sql_carreras_up);
$alumnos_sin_validacion="Select * from ".$table." where usuario='".$usuario."' and alum_colg='Universidad Panamericana' and estado_alumno!='candidato'";
$res_alumnos_up=$pdo->query($alumnos_sin_validacion);
if($res_alumnos_up->rowCount()>=1){
  $deshabilitar_boton='disabled="disabled"';
}else{
  $deshabilitar_boton='';
}
if($cuenta!=0){
    $termino_solicitud='S';
}else{
    $termino_solicitud='N';
}

$res_donarbeca=$pdo->query($sql_donar_beca);
foreach($res_donarbeca as $row){
    $donar_beca=$row['donar_beca'];
    $acepta_terminos=$row['acepta_term'];
}

$sql_hijos="Select * from ".$table." where usuario='".$usuario."'";
$donar_beca='No';  
$res_hijos=$pdo->query($sql_hijos);
$count_hijos=$res_hijos->rowCount();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Grupo Colmenares | Iniciar sesion </title>
<link rel="stylesheet" href="css/login/screen.css" type="text/css" media="screen" title="default" />
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
<!--  jquery core -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script
  src="https://code.jquery.com/jquery-1.10.0.min.js"
  integrity="sha256-2+LznWeWgL7AJ1ciaIG5rFP7GKemzzl+K75tRyTByOE="
  crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="js/sweetalert2.all.min.js"></script>
<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=lccy2w2yeg3u02h95zrkhuu9zdyzfhsv8dl2u5ft1fkvrcqp"></script>

<!-- Custom jquery scripts -->

<script src="js/jquery/custom_jquery.js" type="text/javascript"></script>

<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
<script src="js/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
<style>
    .col{margin-top:auto;}
    .row{margin-bottom: 1%;}
    label{color:black;}
</style>
<script type="text/javascript">
$(document).ready(function(){
$(document).pngFix( );
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();
});
</script>

</head>
<body > 


<!-- Modal -->
<?php if($termino_solicitud=='S'){?>   
       <div style="width:100%;height: 10%;">
        <img  style="width: 100%;" height="100px" src="images/cabecera_becas.png">
    </div>
     <a href="admin/logout.php" id="logout"><button class="btn-danger" style="margin-top: -35px;
    /* margin-right: -100%; */
    margin-left: 90%;
    position: absolute;
    width: 9%;
    height: 5%" type="button" name="cerrar_sesion">Cerrar sesion</button></a>
   <a href="admin/logout.php" id="logout"><button class="btn-danger" style="margin-top: -35px;
    /* margin-right: -100%; */
    margin-left: 90%;
    position: absolute;
    width: 9%;
    height: 5%" type="button" name="cerrar_sesion">Cerrar sesion</button></a>
        <div style="width: 100%;margin: auto;margin-top: 10%;">
         <div class="row">
           <div class="col"></div>
           <div class="col"></div>
           <div class="col">Solicitud de becas Terminada</div>
           <div class="col"></div>
           <div class="col"></div>
         </div>
            <div class="row">
           <div class="col"></div>
           <div class="col"></div>
           <div class="col">    <a href="funciones/mailer.php?i=<?php echo $x_id_fam;?>&a=<?php echo $x_alum_mat;?>&usuario=<?php echo $usuario;?>&ciclo=<?php echo $ciclo_e;?>" target="_blank">

                              <button id="imprimir_solc" class="btn btn-warning">Imprimir y enviar solicitud de beca</button></a></div>
           <div class="col"></div>
           <div class="col"></div>
         </div>
        </div>
       <?php }elseif($acepta_terminos!='Si'){?>
          <div style="width:100%;height: 10%;">
        <img  style="width: 100%;" height="100px" src="images/cabecera_becas.png">
    </div>
         <a href="admin/logout.php" id="logout"><button class="btn-danger" style="margin-top: -35px;
    /* margin-right: -100%; */
    margin-left: 90%;
    position: absolute;
    width: 9%;
    height: 5%" type="button" name="cerrar_sesion">Cerrar sesion</button></a>
        <div style="width: 80%;margin: auto;margin-top: 7%;"class="module-body"> En cumplimiento con la <strong>Ley Federal de Protección de Datos Personales en Posesión de los Particulares</strong> y con el fin de <strong>Asegurar la protección y privacidad de los datos personales, así como regular el acceso, rectificación, cancelación y oposición del manejo de los mismos</strong>, Liceo del Valle, A.C. informa que la recolección de datos podrán ser usados con fines promocionales, informativos y estadísticos relacionados con la operación administrativa diaria del Colegio.<br /><br />
                       En cualquier momento, el titular de los datos podrá hacer uso de sus derechos de acceso, rectificación, cancelación y oposición.<br /><br />
                       Si desea más información, podrá consultar la versión completa de este documento, en el sitio web del Colegio en el apartado <a href="http://liceodelvalle.edu.mx/descargas/aviso_privacidad.pdf" target="_blank">Aviso de Privacidad.</a><br /><br />
                         
                       <form action="" id="form_acepta" method="post" enctype="multipart/form-data">                          
                       <div style="float:left; width:650px;"> 
                      	 <h6>
                          <input name="usuario" type="hidden" value="<?php echo $usuario;?>">   
                          <input name="accion" type="hidden" value="acepta_terminos">   
                    	  <input name="rad_priv" type="radio" value="acep_term" checked="checked" /> 
                    	He leído y acepto los términos y condiciones de uso de información.<br />
						</h6>
                       </div>  
                    
                     </form>
                   </div>
                    <div style="float: right; width:100px;"> 
                    <br>  
                       <button type="button" class="btn btn-success" id="btn_sig2" name="btn_sig2" value="Siguiente">Siguiente</button>
                    </div> 
               
       <?php }elseif($donar_beca=='' and $tipo_beca=='bec_famnum_pp'){?>
         <div style="width:100%;height: 10%;">
        <img  style="width: 100%;" height="100px" src="images/cabecera_apoyando_familias.png">
    </div>
              <div style="width: 80%;margin: auto;margin-top: 7%;"class="module-body"> En las instituciones que integran Grupo Colmenares estamos comprometidos con el desarrollo integral de nuestras familias. Es por ello que creamos el fondo Familias Apoyando a Familias, al cual puedes aportar donando tu Apoyo por Familia Numerosa.<br /><br />
                      Con tu ayuda, familias que lo necesitan serán beneficiadas con beca para que sus hijos continúen construyendo su futuro con la formación y valores que caracterizan  a nuestras instituciones.<br /><br />
                      Tu ayuda, su futuro<br /><br /><br /><br />
                      <center>¿Deseas donar tu Apoyo por Familia Numerosa a Familas Apoyando Familias?</center>   
                       <form action="#" id="form_donar_beca" method="post" enctype="multipart/form-data">                          
                       <div style="    width: 80%;
    margin: auto;"> 
                      	 <h6>
                          <input name="usuario" type="hidden" value="<?php echo $usuario;?>"/>   
                          <input name="accion" type="hidden" value="donar_beca"/>
                          <br />
                          <center>
                    	  <input name="donar_beca" type="radio" value="Si" />
                       Si
                       		&nbsp;	&nbsp;&nbsp;
                    	  <input name="donar_beca" type="radio" value="No"  /> 
                    	No
                          </center><br />
						</h6>
                       </div>  
                    
                    <div style="float: right; width:100px;"> 
                    <br />
                    
                     </form>
                      <button type="button" class="btn btn-success" id="btn_donar_beca" name="btn_beca" type="button">Siguiente</button>
                    </div> 
                   	                                      
                  </div>
       <?php }elseif($donar_beca=='Si' and $tipo_beca=='bec_famnum_pp'){?>
         <div style="width:100%;height: 10%;">
        <img  style="width: 100%;" height="100px" src="images/cabecera_becas.png">
    </div>
        <div style="width: 100%;margin: auto;margin-top: 10%;">
          <center>Gracias por donar su beca al fondo de Familias Apoyando a Familias</center>
            
        </div>
       <?php }else {
           if($tipo_beca=='bec_famnum_pp'){?>
             <div style="width:100%;height: 13%;">
        <img  style="width: 100%;" height="100px" src="images/cabecera_becas.png">
    </div>
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModalUP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLabel">Agregar Alumno UP</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form name="form_alumno_up" id="form_alumno_up">
          <div class="col"><label>Matricula</label>
              <input type="hidden" name="usuario" value="<?php echo $usuario;?>">
              <input type="hidden" name="accion" value="agregar_alumno_UP">
              <input class="form-control" name="matricula_up" required="required"/>
              </div>
              <div class="col"><label>Nombre</label>
                  <input class="form-control" name="nombre_up" required="required"/>
                  <label>Apellido Paterno</label>
                  <input class="form-control" name="ap_up" required="required"/>
                  <label>Apellido Materno</label>
                  <input class="form-control" name="am_up" required="required"/>
              </div>
              <div class="col"><label>Carrera</label>
                  <select class="form-control" name="carrera_up" required="required">
                              <?php foreach($res_carreras_up as $row){?>
                      <option value="<?php echo $row['carrera'];?>"><?php echo $row['nombre'];?></option>
                                  <?php }?>
                  </select>
              </div> 
              <div class="col"><label>Semestre</label>
              <input class="form-control" name="semestre" required="required"/>
             
          </div>
              <div class="col"><label>Edad</label>
              <input class="form-control" name="edad_up" required="required"/>
             
          </div>
          </form>
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
       <button type="button" class="btn btn-success" id="agregar_alumno_up">Agregar Alumno</button>
      </div>
    </div>
  </div>
</div>
 <a href="admin/logout.php" id="logout"><button class="btn-danger" style="margin-top: -35px;
    /* margin-right: -100%; */
    margin-left: 90%;
    position: absolute;
    width: 9%;
    height: 5%" type="button" name="cerrar_sesion">Cerrar sesion</button></a>
        <div id="container" style="    width: 80%;
    margin: auto;
    margin-top: 2%;
    height: auto;">
          
        
          <div class="row">
                <div class="col"></div>
                <div class="col"></div>
                <div class="col">Alumnos activos</div>
                <div class="col"></div>
                <div class="col"></div>
            </div>
            <hr>
            <div id="alum-list">
            <div class="row">
                <div class="col"><h6>Nombre Alumno</h6></div>
                <div class="col"><h6>Colegio Alumno</h6></div>
                <div class="col"></div>
            </div><?php 
            foreach($res_hijos as $row){?>
            <div class="row">
                <div class="col"><?php echo $row['alum_name']." ".$row['alum_ap']." ".$row['alum_am'];?></div>
                <div class="col"><?php echo $row['alum_colg'];?></div>
                <div class="col"><?php echo ($row['estado_alumno']=='sin_validacion')?'Sin Validacion':'';?></div>
                
            </div>
                <?php }  ?>
                </div>
            <div class="row">
                <div class="col"></div>
                <div class="col"></div>
                <div class="col">
                <?php if($count_hijos<3){?>
<button class="btn btn-warning" id="hijos_up_btn"> Siguiente</button>
                <?php }else{?>
                   <button class="btn btn-warning" id="next_form" <?php echo $deshabilitar_boton;?> >Siguiente</button>
                <?php }?>
                   </div>
                 <div class="col">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModalUP">
  Agregar Alumno UP
</button>
               </div>
          
            </div>
            </div>
            
           <?php
           
           }elseif($tipo_beca=='bec_intof'){?>
              
  <div style="width:100%;height: 13%;">
        <img  style="width: 100%;" height="100px" src="images/cabecera_becas.png">
    </div>
     <a href="admin/logout.php" id="logout"><button class="btn-danger" style="margin-top: -35px;
    /* margin-right: -100%; */
    margin-left: 90%;
    position: absolute;
    width: 9%;
    height: 5%" type="button" name="cerrar_sesion">Cerrar sesion</button></a>
        <div id="container" style="    width: 80%;
    margin: auto;
    margin-top: 2%;
    height: auto;">
    <div class="row">
      
    <center>  <h5>Estimada familia, su cuenta ya se encuentra habilitada para que pueda realizar su solicitud.</h5></center>
    </div>
    <hr>
    <div class="row">
      Podrá pagar su estudio socioeconómico con la ficha de pago dando click en el siguiente botón.
    </div>
          <div class="row">
              <div class="col"><a href="funciones/referencia_banca.php" target="_blank"><button class="btn btn-warning" type="button"> Referencia bancaria</button></a></div>
            <div class="col"></div>
            
            <div class="col"></div>
            <div class="col">
                 <div class="col"></div>
              </div>
            </div><br><br><br>
            <div class="row">
              <div class="col"></div>
              <div class="col"></div>
              <div class="col"></div>
              <div class="col"></div>
              <div class="col">
                 
                  <button class="btn btn-info" id="beca_int_of">Continuar</button></div>
            </div>
            </div>
  <?php         }elseif($tipo_beca=='bec_orfandad'){?>
            <div style="width:100%;height: 13%;">
        <img  style="width: 100%;" height="100px" src="images/cabecera_becas.png">
    </div>
     <a href="admin/logout.php" id="logout"><button class="btn-danger" style="margin-top: -35px;
    /* margin-right: -100%; */
    margin-left: 90%;
    position: absolute;
    width: 9%;
    height: 5%" type="button" name="cerrar_sesion">Cerrar sesion</button></a>
     <div id="container" style="    width: 80%;
    margin: auto;
    margin-top: 2%;
    height: auto;">
          <div class="row">
              <div class="col">
                 
                  <label>Continuar con la solicitud</label>
                  <button class="btn btn-info" id="beca_orfandad">Conituar</button>
              </div>
        
            </div>
            </div>
       <?php } }?>
      <div class="footer">
        <img src="admin/images/pies_pagonalogos.png" style="width: 100%;
    margin-top: 2%;">
    </div>
</body>
<script>
  $("#hijos_up_btn").click(function(){
      Swal.fire({
  type: 'error',
  title:'Error',
  text:'Favor de revisar los requisitos para solicitar Apoyo por Familia numerosa',
 footer: '<a href="https://sites.google.com/colmenares.org.mx/becas/tipos-de-becas-y-apoyos/apoyo-por-familia-numerosa?authuser=0" target="_blank">Para más informacion clic aquí</a>' 
});
  });
 
        $("#btn_sig2").click(function(){
        var form = $("#form_acepta").serialize();
        $.ajax({
            url:"funciones/funciones.php",
            data:form,
            datatype:"json",
            type:"post",
            success: function(response){
            if(response.status==true){
                window.location.reload();
            }
        },
                error: function(){}
            });
        });

$("#btn_donar_beca").click(function(){
         
         var form=$("#form_donar_beca :input").serialize();
            $.ajax({
                url:"funciones/funciones.php",
                data:form,
                type:"post",
                datatype:"json",
                success:function(response){
                    if(response.status==true){
                        window.location.reload();
                    }
        },
                    error:function(){}
                });
            });
            $("#next_form").click(function(){
                          Swal.fire({
  title: 'Estimada familia',
  text: "Le recordamos que cuenta con 15 min para realizar la solicitud. Por favor tenga informacion a la mano.",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#1e7e34',
  cancelButtonColor: '#d33',
  cancelButtonText: 'Cancelar',
  confirmButtonText: 'Continuar'
}).then((result) => {
  if (result.value) {
           $.ajax({
            url:"funciones/formularios.php",
            datatype:"html",
            type:"post",
            data:{
                formulario:"APN"
            },
            success:function(response){
            $("#container").empty();
            $("#container").html(response);
                    }
                
                });

  }
});
     
            });
            $("#agregar_alumno_up").click(function(){
            var form= $("#form_alumno_up").serialize();
          $.ajax({
              url:"funciones/funciones.php",
              data:form,
              datatype:"json",
              type:"post",
              success:function(response){
                  $("#next_form").prop('disabled',true);
                  swal(response.mensaje);
                  var campo='<div class="row">'
                +'<div class="col">'+response.nombre_up+'</div>'          
                +'<div class="col">'+response.colegio+'</div>'          
                +'<div class="col">'+response.estado+'</div>'          
                + '</div>';
                  $("#alum-list").append(campo);
                  $("#exampleModalUP").modal('hide');
                    $('#exampleModalUP').modal('toggle');
                    
        $('#form_alumno_up').trigger("reset");
                 
        },
                  error:function(){}
                        });
                    });
                    $("#beca_int_of").click(function(){
                      Swal.fire({
  title: 'Estimada familia',
  text: "Le recordamos que cuenta con 15 min para realizar la solicitud. Por favor tenga informacion a la mano.",
  type: 'warning',
  showCancelButton: true,
  cancelButtonColor: '#d33',
  confirmButtonColor: '#1e7e34',
  cancelButtonText: 'Cancelar',
  confirmButtonText: 'Continuar'
}).then((result) => {
  if (result.value) {
    $.ajax({
                    url:"funciones/formularios.php",
                    data:{formulario:"INT_OF"},
                    type:"post",
                    datatype:"html",
                    success:function(response){
                    $("#container").empty();
            $("#container").html(response);
        },
                    error:function(){}
                    
            });
  }
});
                
        });
        $("#beca_orfandad").click(function(){
                    $.ajax({
                    url:"funciones/formularios.php",
                    data:{formulario:"orfandad"},
                    type:"post",
                    datatype:"html",
                    success:function(response){
                    $("#container").empty();
            $("#container").html(response);
        },
                    error:function(){}
                    
            });
        });
                    function eliminar_dep(id_dep){
  $.ajax({
    url:"funciones/funciones.php",
    data:{
      id_dep:id_dep,
      accion:"eliminar_dep"
    },
    datatype:"json",
    type:"post",
    success:function(response){
      swal(response.mensaje);
      $("#fam_"+id_dep).remove();
    },
    error:function(){}
  });
}
function delete_carro(id_veh){
   $.ajax({
    url:"funciones/funciones.php",
    data:{
      id_dep:id_veh,
      accion:"eliminar_carro"
    },
    datatype:"json",
    type:"post",
    success:function(response){
      swal(response.mensaje);
      $("#veh_"+id_veh).remove();
    },
    error:function(){}
  });
}
function suma_gastos(){
  var $suma=0;
  var $suma_aux=0;
$("#gastos_div").find('input')
        .each(function() {
          if($(this).attr('id')!='total_suma'){

          if($(this).val()==''){
            $(this).val(0);
          }
            $suma_aux=parseFloat($(this).val());
         
            $suma=$suma_aux+$suma; 
       
              $suma_aux=0;

          }
        });
        $("#total_suma").val($suma);
}
function suma_ingresos(){
  var $suma=0;
  var $suma_aux=0;
$("#ingresos_div").find('input')
        .each(function() {
          if($(this).attr('id')!='ing_to'){

          if($(this).val()==''){
            $(this).val(0);
          }
            $suma_aux=parseFloat($(this).val());
         
            $suma=$suma_aux+$suma; 
       
              $suma_aux=0;

          }
        });
        $("#ing_to").val($suma);
}
function verificar_vacios(div_id){
  
  var x=0;
  console.log($("#"+div_id).find('input:text,select,number'));
$("#"+div_id).find('input:text,select,number').each(function(){
if($(this).attr('id')!='bie_ncl'){

  if($(this).val()==''){
   $(this).addClass('is-invalid');
    x+=1;
  }
}
if($(this).attr('id')=='padre_celular' ){
if($(this).val().length < 10){
      $(this).addClass('is-invalid');
    x+=1;
  }
}

if($(this).attr('id')=='madre_celular' ){
if($(this).val().length <10){
      $(this).addClass('is-invalid');
    x+=1;
  }   
}
if($(this).attr('id')=='domimicilio_fam' ){
  var dom=$("#domimicilio_fam").val();
var dom_=dom.length;
if(dom_<5){
      $(this).addClass('is-invalid');
    x+=1;
  }   
}


if($(this).attr('id')=='tel_casa' ){
if($(this).val().length < 8){
      $(this).addClass('is-invalid');
    x+=1;
  }
}


});
if(x>=1){
  return false;
}else{
  return true;
}
}
$("#imprimir_solc").click(function(){
  swal.fire('Si solicitud se muestra en blanco favor de iniciar sesión otra vez y reimprimirla.','','warning');
});
</script>
</html>