<?php
session_start();
$_SESSION["var_ban"]=1;
if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] == "Administrador" || $_SESSION["tipo_priv"] == "Colegio" || $_SESSION["tipo_priv"] == "Coordinador" ) // If the user IS NOT logged in, forward them back to the login page
	{
		
	} else{
	 header("location: index.php");	
	}	
	
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
	<?php
		include_once("z_script/header.php");
	?>
   
	</head> 

	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
       
		<div class="container_12">
 	
      
            
            
                    <div style="text-align: center;">
                        <h5>Bienvenido <?php echo $_SESSION["colegio"]; ?>
                        </h5></br>
                     <div class="module-body">
                     <?php if($_SESSION["tipo_priv"]== "Administrador"){ ?>
                     
                        <p>En este CMS puedes ingresar a los paneles libremente y puedes crear, modificar y eliminar usuarios.<br /><br />
 
¿Algun problema con el CMS?, sugerencias, consejos, etc.. por favor <a href="mailto:leticia.lopez@colmenares.org.mx">contáctanos.</a></p>
					<?php } else if($_SESSION["tipo_priv"]== "Colegio" || $_SESSION["tipo_priv"]== "Coordinador" ){?>
                    
                    <p>En este CMS puedes ingresar a los paneles de consulta de las solicitudes terminadas.<br /><br />
 
¿Algun problema con el CMS?, sugerencias, consejos, etc.. por favor <a href="mailto:leticia.lopez@colmenares.org.mx">contáctanos.</a></p>
                    
                    <?php }?>
                     </div> <!-- End .module-body -->

                </div>  <!-- End .module -->
        		<div style="clear:both;"></div>
            <!-- End .grid_12 -->

            
          <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
		
          <?php include_once("z_script/footer.php") ?>
        
	</body>

</html>