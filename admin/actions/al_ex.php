<?php 
session_start();

if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Administrador") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}

$lib_rel_path = (count(explode("/", $_SERVER['SCRIPT_NAME']))-2 > 0 ? str_repeat("../", count(explode("/", $_SERVER['SCRIPT_NAME']))-2) : "");

include_once("../z_script/db_class.php");
include_once("../z_script/header.php");
include_once("../z_script/PHPExcel/Classes/PHPExcel.php");
 if($_SESSION['periodo']=='periodo_actual'){
 	$table="inf_alum";
 }else{
 	$table="inf_alum_pp";
 }
$consulta="SELECT * FROM ".$table." ORDER BY id_fam ASC";
$result=$pdo->query($consulta);
$i = 4;
$band=1;

$objPHPExcel = new PHPExcel();

$objPHPExcel->
	getProperties()
		->setCreator("Becas Administrator")
		->setLastModifiedBy("Becas Administrator")
		->setTitle("Informacion Alumnos")
		->setSubject("Usuarios")
		->setDescription("Documento generado por Colmenares");

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('B1', 'Información de los Alumnos')
			->setCellValue('A3', 'ID Familia')
			->setCellValue('B3', 'Matrícula alumno')
			->setCellValue('C3', 'Nombre')
			->setCellValue('D3', 'Ap. Paterno')
			->setCellValue('E3', 'Ap. Materno')
			->setCellValue('F3', 'Sección a ingresar')
			->setCellValue('G3', 'Grado a ingresar')
			->setCellValue('H3', 'Grupo a ingresar')
			->setCellValue('I3', 'Colegio a ingresar')
			->setCellValue('J3', '% de beca solicitado');


?>
  <table id="myTable" class="tablesorter table">
                            <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Matrícula del alumno</th>
                                    <th>Familia</th>
                                    <th >Nombre del alumno</th>
                                    <th >Colegio a ingresar</th>
                                    <th>Sección a ingresar</th>
                                    <th>Grado a ingresar</th>
                                    <th>Grupo a ingresar</th>
                                    
                                    <th>% de beca</th>
                                    <th>Carta</th>
                                    <th>Opciones</th>
                                </tr>
                        </thead>
                            <tbody>
                                <?php
								
									foreach ($result as $row){
						$x_id_fam		= $row["id_fam"];
						$x_alum_mat		= $row["alum_mat"];
						$x_alum_name 	= $row["alum_name"];
						$x_alum_ap 		= $row["alum_ap"];
						$x_alum_am 		= $row["alum_am"];
						$x_alum_sec		= $row["alum_sec"];
						$x_alum_grd		= $row["alum_grd"];
						$x_alum_gru		= $row["alum_gru"];
						$x_alum_colg	= $row["alum_colg"];
						$x_alum_porc	= $row["alum_porc"];
						$usuario	= $row["usuario"];
						$carta	= $row["carta"];					
											$name_del_al=$x_alum_name." ".$x_alum_ap." ".$x_alum_am;
				
			if($usuario!='')	
			{						
				$rsCol = $pdo->query("SELECT nombre_bd FROM colegios, inf_alum_pp where inf_alum_pp.alum_colg=colegios.nombre and inf_alum_pp.alum_mat=$x_alum_mat and inf_alum_pp.usuario='$usuario'"); 				
			}
			else
			{
				$rsCol = $pdo->query("SELECT nombre_bd FROM colegios, inf_alum_pp where inf_alum_pp.alum_colg=colegios.nombre and inf_alum_pp.alum_mat=$x_alum_mat");
                                
                        }
			foreach($rsCol as $rCol){
				$bd_al = $rCol['nombre_bd'] ;
			}
			  $dsn = "mysql:host=$db_server;dbname=$bd_al";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
			
			$rs = $pdo->query("SELECT nuevo_ingreso FROM alumnos where alumno=$x_alum_matk"); 
			foreach($rs as $r){
				$n_ingreso = $r['nuevo_ingreso'] ;
			}
			
			  $dsn = "mysql:host=$db_server;dbname=$bd_becas";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
			
			
										if($x_alum_sec!=21){
											if($x_alum_sec==1 and $x_alum_grd==6)
											{
												
													$s=1;
													$g=6;
												
												
											}
											else
											{
												if($x_alum_sec==2 and $x_alum_grd==3)
												{
													
														$s=2;
														$g=3;
													
												}
												else
												{
													
														$s=$x_alum_sec;
														$g=$x_alum_grd;													
													
													if($x_alum_sec==.5 and $x_alum_grd==5)
													{
														
															$s=.5;
															$g=5;
														
													}
													else
													{
														
															$s=$x_alum_sec;
															$g=$x_alum_grd;
														
													}
													if($x_alum_sec==3 and $x_alum_grd==6)
													{														
															$s=3;
															$g=6;
														
													}
													else
													{
														
															$s=$x_alum_sec;
															$g=$x_alum_grd;
														
													}
													
													
												}
											}
										}
			
								?>
                                <tr> 
               <td class="align-center"><?php echo $band_id;?></td>
               <td class="align-center"><?php echo $x_alum_mat;?></td>
               <td class="align-center"><?php echo $x_id_fam;?></td>
               <td><?php echo $x_alum_name." ".$x_alum_ap." ".$x_alum_am;?></td>
				<td><?php echo $x_alum_colg;?></td>
                <td><?php echo $s;?></td>
                <td><?php echo $g;?></td>
                <td><?php echo $x_alum_gru;?></td>
                <td><?php echo $x_alum_porc;?></td>
                <td><?php echo $carta;?></td>
                                    <td>
<a href="al_update.php?id=
<?php echo $x_id_fam;?>&m=
<?php echo $x_alum_mat; ?>&usuario=
<?php echo $usuario; ?>">
<i class="far fa-edit"></i></a>
<a onclick="opcion(<?php echo $x_id_fam." , '".$name_del_al."' , ".$x_alum_mat;?>);">
<i class="fas fa-user-minus"></i></a>
                                    </td>
                                </tr>
                                
                                <?php
								$band_id++; 
									}
								?>
                                
                            </tbody>
                        </table>   
<script type="text/javascript">
    $(document).ready(function(){
         $("#myTable").table2excel({

    // exclude CSS class

    exclude: ".noExl",

   name: "Worksheet Name",

    filename: "Reporte.xls" //do not include extension

  });

 var  url="../report.php";
       setTimeout('closemyself()',2000);
    });
      function closemyself() {
 window.opener=self;
 window.close();
 //self.close();
}
</script>