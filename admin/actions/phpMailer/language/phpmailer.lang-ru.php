<?php
/**
 * PHPMailer language file.
 * Russian Version by Alexey Chumakov <alex@chumakov.ru>
 */

$PHPMAILER_LANG = array();

$PHPMAILER_LANG["provide_address"]      = '�����&oacute;&eacute;���, ������� ���� &aacute;� ���&iacute; ����� e-mail ' .
                                          '���&oacute;������.';
$PHPMAILER_LANG["mailer_not_supported"] = ' - �������&eacute; ������ &iacute;� ��������������.';
$PHPMAILER_LANG["execute"]              = '&Iacute;�������&iacute;� �����&iacute;��� ����&iacute;�&oacute;: ';
$PHPMAILER_LANG["instantiate"]          = '&Iacute;�������&iacute;� ���&oacute;����� �&oacute;&iacute;���� mail.';
$PHPMAILER_LANG["authenticate"]         = '���&aacute;�� SMTP: ���&aacute;�� �����������.';
$PHPMAILER_LANG["from_failed"]          = '&Iacute;����&iacute;�&eacute; ����� �����������: ';
$PHPMAILER_LANG["recipients_failed"]    = '���&aacute;�� SMTP: �������� �� ����&oacute;���� ' .
                                          '������� ���&oacute;������&eacute; &iacute;� &oacute;������: ';
$PHPMAILER_LANG["data_not_accepted"]    = '���&aacute;�� SMTP: ��&iacute;&iacute;�� &iacute;� ���&iacute;���.';
$PHPMAILER_LANG["connect_host"]         = '���&aacute;�� SMTP: &iacute;� &oacute;������ ������������ � ������&oacute; SMTP.';
$PHPMAILER_LANG["file_access"]          = '&Iacute;�� ����&oacute;�� � ��&eacute;�&oacute;: ';
$PHPMAILER_LANG["file_open"]            = '��&eacute;����� ���&aacute;��: &iacute;� &oacute;������ ������� ��&eacute;�: ';
$PHPMAILER_LANG["encoding"]             = '&Iacute;�������&iacute;�&eacute; ��� ���������: ';
$PHPMAILER_LANG["signing"]              = 'Signing Error: ';

?>

