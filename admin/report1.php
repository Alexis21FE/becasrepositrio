<?php 
session_start();
$_SESSION["var_ban"]=3;


if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Administrador") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
	
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}

$band_error_msg=(isset ($_GET["red_msg"]) ? $_GET["red_msg"]: "");
$band_green_msg=(isset ($_GET["green_msg"]) ? $_GET["green_msg"]: "");
$band_modf_msg=(isset ($_GET["modf_msg"]) ? $_GET["modf_msg"]: "");	
	
	extract($_POST, EXTR_PREFIX_ALL, "x");
	include("z_script/db_class.php");
	
	//paginador
	if(isset($_GET['page'])) //verifica pagina
	{
    	$page= $_GET['page'];
	}else{
   	 	$page=1;
	}
	
	if(isset($x_busc)){
		
		if($x_tip_bus == NULL && $x_input_bus == NULL){
			$consulta="SELECT * FROM user_fam";
		}else if($x_tip_bus == "user"){
				$consulta="SELECT * FROM user_fam where fam_user LIKE'%".$x_input_bus."%'";
		
		}else if($x_tip_bus == "name"){
				$consulta="SELECT * FROM user_fam where nombre_familia LIKE('%".$x_input_bus."%')";
		
		}else if($x_tip_bus == "pass"){
				$consulta="SELECT * FROM user_fam where fam_pass LIKE'%".$x_input_bus."%'";
		
		}else if($x_tip_bus == "ade"){
				$consulta="SELECT * FROM user_fam where fam_ade LIKE'%".$x_input_bus."%'";
		
		}else if($x_tip_bus == "term"){
				$consulta="SELECT * FROM user_fam where fam_term LIKE'%".$x_input_bus."%'";
		
		}else if($x_tip_bus == "soc"){
				$consulta="SELECT * FROM user_fam where pago_soc LIKE'%".$x_input_bus."%'";
		
		}else if($x_tip_bus == "cod"){
				$consulta="SELECT * FROM user_fam where cod_verif LIKE'%".$x_input_bus."%'";
		
		}else if($x_tip_bus == "inc"){
				$consulta="SELECT * FROM user_fam where fech_inc LIKE'%".$x_input_bus."%'";
		
		}else if($x_tip_bus == "fin"){
		
		$consulta="SELECT * FROM user_fam where fech_fin LIKE'%".$x_input_bus."%'";
		}
		
	}else if(isset($x_resg_busc)){
		
		$consulta="SELECT * FROM user_fam";
		
	}else{
	
		$consulta="SELECT * FROM user_fam";
		
	}
            $consulta.=" order by nombre_familia asc";
	$datos=$pdo->query($consulta);
	$num_rows=$datos->rowCount();
       
	$rows_per_page= 100;
	$lastpage= ceil($num_rows / $rows_per_page);
	$page=(int)$page;
 	$band_del=0;
	if($page > $lastpage)
	{
    	$page= $lastpage;
	}
 
	if($page < 1)
	{
    	$page=1;
	}
  
	$limit= 'LIMIT '. ($page -1) * $rows_per_page . ',' .$rows_per_page;
	$consulta .=" $limit";
	$peliculas=$pdo->query($consulta);
            
	////

?>
<!DOCTYPE>
<html>
	<head>
		<?php
            include_once("z_script/header.php");
        ?>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
		<div class="container_12">

          <div style="clear:both;"></div>
            
          
                
                <!-- Notification boxes -->
                
                <?php
					if($band_green_msg=="display_mod"){
						
					?>
                    <span class="notification n-success"> Usuario Familia modificado exitosamente.</span>
                    <?php
					}
					?>	
                
                <?php
					if($band_error_msg=="display"){
						
					?>
                    <span class="notification n-success"> Usuario Familia eliminado exitosamente.</span>
                    <?php
					}
					?>	
                
          
               <center>  <label>
                          	Usuarios de Familias para Solicitud de Beca
                        </label></center>
                    <hr>
                  		<form action="report.php" method="post" enctype="multipart/form-data">
                        <div class="row">
                         
                          	<div class="col">
                            	<select class="form-control" id="tip_bus" name="tip_bus">
                                    <option value="" selected="selected">Selecciona..</option>
                                    <option value="name">Nombre Familia</option>
                                    <option value="user">Usuario</option>
                                    <option value="pass">Contraseña</option>
                                    <option value="term">Acep. Términos</option>
                                    <option value="soc">Pago Soc.</option>
                                    <option value="cod">Referencia Bancaria.</option>
                                    <option value="inc">Fecha Inicial</option>
                                    <option value="fin">Fecha Final</option>
                              	</select>
                            </div>
                            <div class="col"><input class="form-control" name="input_bus" id="input_bus" type="text" /></div>
                            <div class="col">
                            	<button class="btn btn-success" name="busc" id="busc" type='submit'  >Buscar</button>
                            </div>
                            
                            <?php if(isset($x_busc) && $x_tip_bus != NULL && $x_input_bus != NULL){ ?>
                         	<div class="col"> 
                           <button class="btn btn-success" name="resg_busc" id="resg_busc" type='submit' >Regresar</button>
                             
                            </div>
							<?php }?>
                         
                       
					</form>
                  		
                		

                  
               
           
                          
						  <div class="col">
                            	<!--a href="nusuario.php">
                                 
                                 <button class="btn btn-success"  name="nuevo" id="nuevo" type='submit'  >Nuevo usuario</button>
                               
                              	</a>
                                                 
                            	<a href="import.php">
                                 
                                 <button class="btn btn-success" name="enviar" id="enviar" type='submit'>Importar</button>
                               
                              	</a--></div><div class="col">
                          <a  href="actions/users_fam_ex.php" target="_blank">
                     
                                    <button class="btn btn-success" name="enviar" id="enviar" type='button'> Exportar</button>
                     
                  </a></div>
                           
                        </div>   
                       
           		  
                           
                </div>
              </div>
                  
                <!-- Example table -->
                <div class="module" style="padding:2%;">
                                    
                    <div class="module-table-body">
                    	<form action="">
                        <table id="myTable" class="tablesorter table" >
                            
                        	 <thead class="thead-dark">
                                <tr>
				    <th width="7%">Usuario</th>
                    <th >Nombre Familia</th>
                    <th >Contraseña</th>
                    <!--th >Adeudo</th-->
                    <th >Acep. Términos</th>
                    <th >Pago Soc.</th>
                    <th >Referencia Bancaria</th>
                    <th >Fecha Incial</th>
                    <th >Fecha Final</td>
                    <th >Donar beca</td>
                    <th >Observaciones</td>
                    <th >Estudio Socioeconomico</td>
                    <th >Opciones</th> 
                                </tr>
                                 </thead>   
                            <?php
								if($page == 1){
								
								$band_id=1;
								
								}else{
								
								$band_id=($page * $rows_per_page) - ($rows_per_page) + 1;
								
								}
							
                                                        foreach ($peliculas as $row){
								$x_id_fam		= $row["id_fam"];	
								$x_fam_user		= $row["fam_user"];
								$x_fam_pass		= $row["fam_pass"];
								$x_fam_ade		= $row["fam_ade"];
								$x_fam_term		= $row["fam_term"];
									$x_acep_term	 = $row["acepta_term"];
								$x_pago_soc		= $row["pago_soc"];
								$x_cod_verif	= $row["cod_verif"];
								$x_fech_inc		= $row["fech_inc"];
								$x_fech_fin		= $row["fech_fin"];
								$x_obser_fam	= $row["obser_fam"];
								$x_donar_beca	= $row["donar_beca"];
								$nom_familia	= $row["nombre_familia"];
								
								$rs_ = $pdo->query("SELECT concat(inf_familia.ft_ap,' ',inf_familia.mt_ap) as fam FROM inf_familia where inf_familia.id_fam=$x_id_fam and inf_familia.usuario='$x_fam_user'");
                                                                foreach ($rs_ as $r_){
										$nombre_fam=$r_['fam'];
									}
									

									if($nombre_fam=='' or $nombre_fam==' ')
									{
										
										$rs_ = $pdo->query("SELECT nombre_familia  FROM user_fam where fam_user ='$x_fam_user'"); 
										foreach($rs_ as $r_)
										{
											$nombre_fam = $r_['nombre_familia'] ;
										}
										if($nombre_fam=='' or $nombre_fam==' ')
										{
										
										                

										$rs_ = $pdo->query("SELECT distinct(alum_colg) as c FROM inf_alum where usuario ='$x_fam_user' order by alum_mat desc");
										$alumnoColegio = array(); 
										foreach($rs_ as $r_)
										{
											$alumnoColegio[] = $r_['c'] ;
										}
										
										$rs_ = $pdo->query("SELECT nombre, nombre_bd FROM colegios where nombre='$alumnoColegio[0]'");
                                                                                foreach($rs_ as $r_){
												$bd = $r_['nombre_bd'] ;
												$colegio=$r_['nombre'] ;
											}
                                                                                        $dsn = "mysql:host=$db_server;dbname=$bd";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
											
					$rsfam_ = $pdo->query("SELECT nombre_familia FROM familias where familia=".$x_id_fam);
                                                                                    $nombre_fam="";
                                                                                    foreach($rsfam_ as $rfam_){
												$nombre_fam = $rfam_['nombre_familia'] ;
											}
											$dsn = "mysql:host=$db_server;dbname=$bd_becas";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
											///////////////////////////////////////////////////////////
									
									}
									}
									$rs_=$pdo->query("Select * from estudios_familias ef INNER JOIN empresas_estudios e ON ef.idempresa=e.idEmpresa where ef.userfam='".$x_fam_user."'");
									$empresa=null;
									foreach ($rs_ as $value) {
										$empresa=$value['Nombre'];
									}
								?>
                            <tbody>
                                <tr>
                                   
									<td><?php echo $x_fam_user;?></td>
									<td><?php echo $nom_familia;/*$nombre_fam;*/?></td>
									
                                    									
                                    <td><?php echo $x_fam_pass;?></td>
                                    <!--td><?php echo $x_fam_ade;?></td-->
                                    <td><?php echo $x_acep_term;?></td>
                                    <td><?php echo $x_pago_soc;?></td>
                                    <td><?php echo $x_cod_verif;?></td>
                                    <td><?php echo $x_fech_inc;?></td>
                                    <td><?php echo $x_fech_fin;?></td>
                                    <td><?php echo $x_donar_beca;?></td>
                                     <td><?php echo $x_obser_fam;?></td>
                                     <td><?php echo $empresa;?></td>
                                    
                                    <td>
                                    	<a href="users_fam_update.php?id=<?php echo $x_id_fam;?>&usuario=<?php echo $x_fam_user;?>"><i class="far fa-edit"></i></a>
                                     <a href="actions/users_fam_del.php?id=<?php echo $x_id_fam;?>&usuario=<?php echo $x_fam_user;?>"><i class="fas fa-user-minus"></i></a>
                                    </td>
                                    
                                </tr>
                            </tbody>
                          <?php 
						  		$band_id++; 
						   }  
						   ?>
                        </table>
                        </form>
                    	<div style="clear: both"></div>
                  </div> <!-- End .module-table-body -->
                </div> <!-- End .module -->
                <div style="float:left;">
                Modificar: <img src="images/bin.gif" width="16" height="16" alt="Modificar" /> &nbsp; &nbsp; Eliminar: <img src="images/minus-circle.gif" width="16" height="16" alt="delete" />
                </div>
                <div class="pagination">           
                		 <?php
							//UNA VEZ Q MUESTRO LOS DATOS TENGO Q MOSTRAR EL BLOQUE DE PAGINACIÓN SIEMPRE Y CUANDO HAYA MÁS DE UNA PÁGINA
							if($num_rows != 0)
							{
							   $nextpage= $page +1;
							   $prevpage= $page -1;
						?>
						<?php
						//SI ES LA PRIMERA PÁGINA DESHABILITO EL BOTON DE PREVIOUS, MUESTRO EL 1 COMO ACTIVO Y MUESTRO EL RESTO DE PÁGINAS
								  if($page == 1) 
								   {
									?>
                       <!-- <a href="" class="button"><span><img src="images/arrow-180-small.gif"  height="9" width="12" alt="Previous" /> Anterior</span></a>-->
                        <div class="numbers">
                            <span>Paginas:</span> 
                            <span class="current">1</span> 
                            <span>|</span>
                            
                            <?php
							  for($i= $page+1; $i<= $lastpage ; $i++)
							  {
							?> 
                            	<a href="report.php?page=<?php echo $i;?>"><?php echo $i;?></a> 
                            	<span>|</span>
                            <?php 
							  }
							?>
                            
                            <?php
                             //Y SI LA ULTIMA PÁGINA ES MAYOR QUE LA ACTUAL MUESTRO EL BOTON NEXT O LO DESHABILITO
										if($lastpage >$page )
										{
									?>    
                               </div>        
                       	  <a href="report.php?page=<?php echo $nextpage;?>" class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> 
                         <?php
										}else{
									?>
                       					</div>
                                   			<!-- <a class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> -->
                        <?php
										}
									} else{
									//EN CAMBIO SI NO ESTAMOS EN LA PÁGINA UNO HABILITO EL BOTON DE PREVIUS Y MUESTRO LAS DEMÁS
								?>
                       <a href="report.php?page=<?php echo $prevpage;?>" class="button"><span><img src="images/arrow-180-small.gif"  height="9" width="12" alt="Previous" /> Anterior</span></a>
                      <div class="numbers"> 
                      <span>Paginas:</span>
                      
                        <?php
										 for($i= 1; $i<= $lastpage ; $i++)
										 {
													   //COMPRUEBO SI ES LA PÁGINA ACTIVA O NO
											if($page == $i)
											{
								?>     
                                  
                            
                                <span class="current"><a><?php echo $i;?></a></span><span>|</span>
                                
                                <?php
											}
											else
											{
								?>
                                
                                    
                                <a href="report.php?page=<?php echo $i;?>"><?php echo $i;?></a>
                                <span>|</span>
                                
                                <?php
											}
										}
								?>
                                
                                <?php		
										 //Y SI NO ES LA ÚLTIMA PÁGINA ACTIVO EL BOTON NEXT     
										if($lastpage >$page )
										{   
								?>   
                            </div>   
                       <a href="report.php?page=<?php echo $nextpage;?>" class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> 
                      
                                <?php
										}
									 else
										{
								?>  
                                   </div> 
                                   <!--<a class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> -->
                                   
								   <?php
										}
								}     
							  }
							?>
                            
                       <div style="clear: both;"></div> 
                     </div>
                
                

                
			</div> <!-- End .grid_12 -->
                
            <!-- Categories list --><!-- End .grid_6 -->
            
            <!-- To-do list --><!-- End .grid_6 -->
          <div style="clear:both;"></div>
            
            <!-- Form elements --><!-- End .grid_12 -->
                
            <!-- Settings-->
            <div class="grid_6">
                 <!-- End .module -->
            </div> <!-- End .grid_6 -->
                
            <!-- Password --><!-- End .grid_6 -->
          <div style="clear:both;"></div><!-- End .grid_3 --><!-- End .grid_3 --><!-- End .grid_6 -->

            
          <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
		
         <?php include_once("z_script/footer.php") ?>
	</body>
</html>