﻿<?php session_start();
//session_start();
$_SESSION["var_ban"]=9;


if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] == "") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
	
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}

$x_input_bus=''; if(!empty($_POST['input_bus'])) { $x_input_bus=$_POST['input_bus'];}

$band_error_msg=(isset ($_GET["red_msg"]) ? $_GET["red_msg"]: "");
$band_green_msg=(isset ($_GET["green_msg"]) ? $_GET["green_msg"]: "");
$band_modf_msg=(isset ($_GET["modf_msg"]) ? $_GET["modf_msg"]: "");	

$x_id_fam		= 0;
$nombre_familia	= "";
$acepta			= "";								
$beca_term		= "";
$tipo_bec		= "";
$colegio		="";
$query="";
$alumno=0;
$ciclo=0;

if($_SESSION["tipo_priv"]=='Colegio')
{
	$query1="select * from user_fam, inf_familia, inf_alum_pp where user_fam.fam_user=inf_familia.usuario 
and inf_familia.usuario=inf_alum_pp.usuario and fam_term='Si' and alum_colg='".$_SESSION["colegio"]."'
group by user_fam.id_fam,user_fam.fam_user,inf_familia.id_fam,inf_familia.ft_name,inf_alum_pp.id_fam,inf_alum_pp.alum_mat";
	$query_colegio="select * from user_fam, inf_familia, inf_alum_pp where user_fam.fam_user=inf_familia.usuario
and inf_familia.id_fam=inf_alum_pp.id_fam and fam_term='Si' and alum_colg='".$_SESSION["colegio"]."' 
group by user_fam.id_fam,user_fam.fam_user,inf_familia.id_fam,inf_familia.ft_name,inf_alum_pp.id_fam,inf_alum_pp.alum_mat";
	$query_familia="select * from user_fam, inf_familia, inf_alum_pp where user_fam.fam_user=inf_familia.usuario 
and inf_familia.usuario=inf_alum_pp.usuario and fam_term='Si' and alum_colg='".$_SESSION["colegio"]."' and user_fam.id_fam=$x_input_bus group by user_fam.id_fam,user_fam.fam_user,inf_familia.id_fam,inf_familia.ft_name,inf_alum_pp.id_fam,inf_alum_pp.alum_mat";
	$query_beca="select * from user_fam, inf_familia, inf_alum_pp, solc_term where user_fam.fam_user=inf_familia.usuario 
and inf_familia.usuario=inf_alum_pp.usuario and solc_term.usuario=inf_familia.usuario and fam_term='Si' and alum_colg='".$_SESSION["colegio"]."'
group by user_fam.id_fam,user_fam.fam_user,inf_familia.id_fam,inf_familia.ft_name,inf_alum_pp.id_fam,inf_alum_pp.alum_mat";
	$query_inc="select * from user_fam, inf_familia, inf_alum_pp where user_fam.fam_user=inf_familia.usuario 
and inf_familia.usuario=inf_alum_pp.usuario and fam_term='Si' and alum_colg='".$_SESSION["colegio"]."'
group by user_fam.id_fam,user_fam.fam_user,inf_familia.id_fam,inf_familia.ft_name,inf_alum_pp.id_fam,inf_alum_pp.alum_mat";
}
if($_SESSION["tipo_priv"]=='Coordinador')
{
	$query1="select * from user_fam, inf_familia, inf_alum_pp where user_fam.fam_user=inf_familia.usuario 
and inf_familia.usuario=inf_alum_pp.usuario and fam_term='Si' and alum_colg='".$_SESSION["colegio"]."'
group by user_fam.id_fam";
	$query_colegio="select * from user_fam, inf_familia, inf_alum_pp where user_fam.fam_user=inf_familia.usuario
and inf_familia.id_fam=inf_alum_pp.id_fam and fam_term='Si' and alum_colg='".$_SESSION["colegio"]."' 
group by user_fam.id_fam,user_fam.fam_user,inf_familia.id_fam,inf_familia.ft_name,inf_alum_pp.id_fam,inf_alum_pp.alum_mat";
	$query_familia="select * from user_fam, inf_familia, inf_alum_pp where user_fam.fam_user=inf_familia.usuario 
and inf_familia.usuario=inf_alum_pp.usuario and fam_term='Si' and alum_colg='".$_SESSION["colegio"]."' and user_fam.id_fam=$x_input_bus group by user_fam.id_fam,user_fam.fam_user,inf_familia.id_fam,inf_familia.ft_name,inf_alum_pp.id_fam,inf_alum_pp.alum_mat";
	$query_beca="select * from user_fam, inf_familia, inf_alum_pp, solc_term where user_fam.fam_user=inf_familia.usuario 
and inf_familia.usuario=inf_alum_pp.usuario and solc_term.usuario=inf_familia.usuario and fam_term='Si' and alum_colg='".$_SESSION["colegio"]."'
group by user_fam.id_fam,user_fam.fam_user,inf_familia.id_fam,inf_familia.ft_name,inf_alum_pp.id_fam,inf_alum_pp.alum_mat";
	
	$query_inc="select * from user_fam, inf_familia, inf_alum_pp where user_fam.fam_user=inf_familia.usuario 
and inf_familia.usuario=inf_alum_pp.usuario and fam_term='Si' and alum_colg='".$_SESSION["colegio"]."'
group by user_fam.id_fam,user_fam.fam_user,inf_familia.id_fam,inf_familia.ft_name,inf_alum_pp.id_fam,inf_alum_pp.alum_mat";
}
if($_SESSION["tipo_priv"]=='Administrador')
{
	$query1="select * from user_fam, inf_familia where user_fam.fam_user=inf_familia.usuario 
and fam_term='Si' order by user_fam.fam_user ";

	$query_colegio="select * from user_fam, inf_familia, inf_alum_pp where user_fam.fam_user=inf_familia.usuario
and inf_familia.id_fam=inf_alum_pp.id_fam and fam_term='Si' and inf_alum_pp.alum_colg='$x_input_bus' 
order by user_fam.fam_user";

	$query_familia="select * from user_fam, inf_familia where user_fam.fam_user=inf_familia.usuario 
and fam_term='Si' and user_fam.id_fam=$x_input_bus order by user_fam.fam_user";

	$query_beca="select * from user_fam, inf_familia, solc_term where user_fam.fam_user=inf_familia.usuario 
and solc_term.usuario=inf_familia.usuario and fam_term='Si' group by user_fam.id_fam,user_fam.fam_user,inf_familia.id_fam,inf_familia.ft_name,inf_alum_pp.id_fam,inf_alum_pp.alum_mat";
	
	$query_inc="select * from user_fam where fam_term='Si' group by user_fam.id_fam,user_fam.fam_user,inf_familia.id_fam,inf_familia.ft_name,inf_alum_pp.id_fam,inf_alum_pp.alum_mat";
}
									

$x_tip_bus=''; if(!empty($_POST['tip_bus'])) { $x_tip_bus=$_POST['tip_bus'];}
	
	extract($_POST, EXTR_PREFIX_ALL, "x");
	include("z_script/db_class.php");
	
	
	//paginador
	if(isset($_GET['page'])) //verifica pagina
	{
    	$page= $_GET['page'];
	}else{
   	 	$page=1;
	}
	
	if(isset($x_busc))
      {		
			if($x_tip_bus == NULL && $x_input_bus == NULL){
		
			//$consulta="select * from user_fam, inf_familia where user_fam.fam_user=inf_familia.usuario and fam_term='Si' order by user_fam.id_fam";
			$consulta=$query1;
			
			}						
            if($x_tip_bus == "beca_terminada"){
		    	$consulta=$query_beca;	
			}
			if($x_tip_bus == "colegio"){
		    	$consulta=$query_colegio;	
			}
			if($x_tip_bus == "familia"){
		    	$consulta=$query_familia;	
			}
			if($x_tip_bus == "inconclusa"){
		    	$consulta=$query_inc;	
			}	
								
		}else
		{
			
				$consulta=$query1;
		}   
	$datos=$pdo->query($consulta);
	$num_rows=$datos->rowCount();
	$rows_per_page= 50;
	$lastpage= ceil($num_rows / $rows_per_page);
	$page=(int)$page;
 	$band_del=0;
	if($page > $lastpage)
	{
    	$page= $lastpage;
	}
 
	if($page < 1)
	{
    	$page=1;
	}
  
	$limit= 'LIMIT '. ($page -1) * $rows_per_page . ',' .$rows_per_page;
	$consulta .=" $limit";
    
	$peliculas=$pdo->query($consulta);
	
	////

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php
            include_once("z_script/header.php");
        ?>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
		<div class="container_12">

          <div style="clear:both;"></div>
            
          
                
                <!-- Notification boxes -->
                
                <?php
					if($band_green_msg=="display_mod"){
						
					?>
                    <span class="notification n-success"> Alumno modificado exitosamente.</span>
                    <?php
					}
					?>	
                
                <?php
					if($band_error_msg=="display"){
						
					?>
                    <span class="notification n-success"> Alumno eliminado exitosamente.</span>
                    <?php
					}
					?>	
                
              <div class="bottom-spacing"></div>
                
                  
                  		<form action="report_ingresos.php" method="post" enctype="multipart/form-data">
                                    <div class="row">
                       
                          	<div class="col">
                            	<select class="form-control" id="tip_bus" name="tip_bus">
                                    <option value="" selected="selected">Selecciona..</option>
                                    <option value="colegio">Colegio</option>
                                    <option value="familia">Familia</option>
                                    <option value="inconclusa">Solicitud Inconclusa</option>
									<option value="beca_terminada">Beca Terminada</option>
                              	</select>
                            </div>
							<div class="col">
							
							<input class="form-control" name="input_bus" id="input_bus" type="text" /></td><td width="100">
                    </div>
                        <div class="col"><button class="btn btn-success" name="busc" id="busc" type='submit'  >Buscar</button>
								
                            </div>
                            <div class="col">
                            <?php if(isset($x_busc) && $x_tip_bus != NULL /*&& $x_input_bus != NULL*/){ ?>
                         	 
                           <input class="submit-green" style="width:90px; height:30px"  name="resg_busc" id="resg_busc" type='submit'  value="Regresar" />
                             
							<?php }?>
                            </div>
                         
                        
					</form>
                  		
                		
                
				
				
                 
                         
                            <div class="col">                            	
                            </div>
                          	<div class="col">                            	
                            </div>
                            <div class="col"><a href="actions/ingresos_ex.php">
                     
                                    <button class="btn btn-success" name="enviar" id="enviar" type='submit'> Exportar </button>
                     
                  </a></div>
                           
                       
</div>
           		  
               
                  
            
              </div>
                  
                <!-- Example table -->
                <div class="module">
                    <center>Familias que han ingresado.</center>
                    <hr>
                    <div class="module-table-body">
                    	<form action="">
                        <table id="myTable" class="tablesorter table">
                            <thead class="thead-dark">	
                                <tr>
				    <th>Id Familia</th>
					<!--<th width="5%">Alumno</th>-->
                    <th >Nombre Familia</th>
                    <th >Acepta Aviso de Privacidad</th>
                    <th >Beca terminada</th>
					<th >Tipo Beca</th>
					<th >Colegio</th>
					<th >Ciclo</th>
                   <!-- <th width="12%">Opciones</th> -->
                                </tr></thead>                                
                            <?php
								
								foreach ($peliculas as $row){
								$x_id_fam		= $row["id_fam"];
								if($x_tip_bus != "inconclusa"){//////si no es inconclusa
									$alumno			= $row["alum_mat"];
									$nombre_familia	= $row["ft_ap"]." ".$row["mt_ap"];
								}
								$acepta			= $row["fam_term"];
								
								if($_SESSION["tipo_priv"]=='Administrador')
								{
									$cc="SELECT * FROM inf_alum_pp WHERE id_fam=$x_id_fam"; 
									$result_g=$pdo->query($cc);
									foreach ( $result_g as $grd){
										$colegio	= $grd["alum_colg"];
									}
								}
								else
								{
									$colegio		= $row["alum_colg"];
								}
								//if($x_tip_bus == "beca_terminada"){
								
									////////
								
									$query_bec="select * from solc_term where id_fam=$x_id_fam";
									$result_b=$pdo->query($query_bec);
									foreach ($result_b as $bec){
										$beca_term	= $bec["id_acp_term"];
										$tipo_bec	= $bec["tip_bec"];
										$ciclo		= $bec["ciclo"];
										
									}
									//////
									
									/*$beca_term	= $row["id_acp_term"];
									$tipo_bec	= $row["tip_bec"];*/
								if($alumno>1)
								{	
									$coleg="SELECT * FROM inf_alum_pp WHERE alum_mat=$alumno";
									$result_c=$pdo->query($coleg);
									foreach ($result_c as $col){
										$colegio	= $col["alum_colg"];
									}
								}
								
								//}	
								if($ciclo==0)$ciclo="";
								?>
                             
								<?php
								if($x_tip_bus != "inconclusa"){
								?>     
									<tbody>
                                <tr>                             
									<td><?php echo $x_id_fam;?></td>
									<!--<td><?php// echo $alumno;?></td>-->
									<td><?php echo $nombre_familia;?></td>	
                                    <td><?php echo $acepta;?></td>
                                    <td><?php echo $beca_term;?></td>
                                    <td><?php echo $tipo_bec;?></td>
									<td><?php echo $colegio;?></td>
									<td><?php echo $ciclo;?></td>
								</tr>
                            </tbody>
								<?php
								}
								else
								{
								
									///datos
								
									$datos="SELECT * FROM user_fam where fam_term='Si'";
									$result_=$pdo->query($datos);
									foreach ( $result_ as $dat){
										$nombre_familiaa	= $dat["nombre_familia"];
										$x_id_famm	= $dat["id_fam"];
										$nombre_familiaa	= $dat["nombre_familia"];
										$term= $dat["fam_term"];
										//colegio
										$colegioo="";
										$colegg="SELECT * FROM inf_alum_pp WHERE id_fam=$x_id_famm order by alum_colg";
										$result_cc=$pdo->query($colegg);
										foreach ($result_cc as $coll){
											$colegioo	= $coll["alum_colg"];
										}
									
									
									$ct=0;
									$sqll="SELECT count(*) as c FROM solc_term WHERE id_fam=$x_id_famm"; 
									$resultt=$pdo->query($sqll);
									foreach ($resultt as $roww)
									{ 
											$ct 	= $roww["c"];									
									}
									
									////////
									if($ct<=0)
									{								
									?>
									<tbody>
                               		<tr>
										<td><?php echo $x_id_famm;?></td>
										<!--<td><?php// echo $alumno;?></td>-->
										<td><?php echo $nombre_familiaa;?></td>	
										<td><?php echo $term; ?></td>
										<td>No</td>
										<td></td>
										<td><?php echo $colegioo;?></td>
										<td></td>	
									</tr>
                            	</tbody>
								<?php	
									}
								  }///cierra foreach	SELECT * FROM user_fa					
								}
								?>
                                    
                                
                          <?php 
						  		$band_id++; 
						   }  
						   ?>
                        </table>
                        </form>
                    	
		
         <?php include_once("z_script/footer.php") ?>
	</body>
</html>