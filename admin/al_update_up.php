<?php 
session_start();
$_SESSION["var_ban"]=6;

if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] == "C") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
	
$matricula = $_GET["matricula"];

if($matricula=="")
{
	$matricula = $_POST["matricula"];
}

$fam_alum='';
	
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}
extract($_POST, EXTR_PREFIX_ALL, "x");
$fecha_year = date("Y");
$fech_month = date("m");
$fam_id = $_GET["id"];
$al_=0;
include("z_script/db_class.php");	
 if($_SESSION['periodo']=='periodo_actual'){
 	$table="inf_alum";
 }else{
 	$table="inf_alum_pp";
 }

$sqlA="SELECT count(alum_mat) as al FROM ".$table." WHERE alum_mat=$matricula"; 

 $resultA=$pdo->query($sqlA);

foreach ($resultA as $rowA){ 
		$alumActivo  = $rowA["al"];
		}
if($alumActivo<=0)
{
	echo" <script language='javascript'>alert('No hay alumnos activos para esta familia');</script>";
}
				
 $sql="SELECT * FROM ".$table." WHERE alum_mat='$matricula' and id_fam=$fam_id"; 
 $result=$pdo->query($sql);

foreach ($result as $row){ 
		$matr  = $row["alum_mat"];
		$nombre_alumno	 = $row["alum_name"];
		$apellidop 	 = $row["alum_ap"];
		$apellidom	 = $row["alum_am"];
		$carrera	 = $row["carrera"];
		$estado = $row["estado_alumno"];		
}
			

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php
            include_once("z_script/header.php");
        ?>
        <style type="text/css">
		div.module table td {
		background-color: #ffffff;
		padding: 5px;
		border-right: 0px solid #d9d9d9;
		}

		div.module table {
			width: 0%;
			margin: 0 0 10px 0;
			border-left: 0px solid #d9d9d9;
			border-bottom: 0px solid #d9d9d9;
			}
		</style>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
		<div class="container_12">

          <div style="clear:both;"></div>
            
            
            
            <div class="grid_12">
                <div class="module">
                	<h2><span>Modificar Alumno Up: <?php echo $nombre_alumno.' '.utf8_encode($apellidop).' '.utf8_encode($apellidom); ?></span></h2>
                    
                    <div class="module-table-body" >
                    	<div style="margin-left: 25px;margin-top: 10px;">
                        <form name="upd_user" action="actions/alum_mod_up.php?id=<?php echo $fam_id; ?>&matricula=<?php echo $matricula; ?>&usuario=<?php echo $usuario; ?>" method="post" enctype="multipart/form-data">
                    	  <table width="893" border="0" style="border-left: 0px solid #d9d9d9; border-bottom: 0px solid #d9d9d9;">
                    	    <tr>
                            <td width="134">Familia:</td>
                            <td width="306"><input type="text" name="fam" id="fam" value="<?php echo $fam_id; ?>" /></td>
                            <td width="439"></td>
          					</tr>
							<tr>
                            <td width="134">Matricula:</td>
                            <td width="306"><input type="text" name="matricula" id="matricula" value="<?php echo $matr; ?>" maxlength="5" /></td>
                            <td width="439"></td>
          					</tr>
                          <tr>
                            <td>Nombre alumno:</td>
                            <td><input type="text" name="nombre" id="nombre" value="<?php echo $nombre_alumno;?>" /></td>
                            <td></td>
                          </tr>
                          </tr>
                          <tr>
                            <td>Apellido paterno:</td>
                            <td><input type="text" name="apaterno" id="apaterno" value="<?php echo utf8_encode($apellidop);?>" /></td>
                            <td></td>
                          </tr>
                          <tr>
						  <tr>
                            <td>Apellido materno:</td>
                            <td><input type="text" name="amaterno" id="amaterno" value="<?php echo utf8_encode($apellidom);?>" /></td>
                            <td></td>
                          </tr>
						  <tr>
                            <td>Carrera:</td>
                            <td>
							<select id="carrera" name="carrera">
                                	<option value="" selected="selected">Selecciona...</option>
									<?php								
								$rs = $pdo->query ("select * from carreras");
								foreach($rs as $r){
									$carrera_base=$r['carrera'];
									$ncarrera_base=$r['nombre'];
									?>
									<option value="<?=$carrera_base;?>" <? if($carrera_base == $carrera){echo "selected='selected'"; }?> ><? echo $ncarrera_base;?> </option>
									<?php
								}
									?>
                               
                            	</select>
							</td>
                            <td></td>
                          </tr>
                          <tr>
                          <tr>
                            <td>Estado:</td>
                            <td><select id="estado" name="estado">
                            <option value="" selected="selected">Selecciona</option>
                            <option value="candidato" <? if($estado == "candidato"){echo "selected='selected'"; }?>>Candidato</option>
                            <option value="sin_validacion" <? if($estado == "sin_validacion"){echo "selected='selected'"; }?>>Sin Validación</option>
                            <option value="no_candidato" <? if($estado == "no_candidato"){echo "selected='selected'"; }?>>No Candidato</option>
                          </select></td>
                            <td></td>
                            </tr>
                                                                              
                    </table>
                       
                        
                        <h4>
                        <input class="submit-green" style="width:90px; height:30px; margin-top:20px"  name="enviar" id="enviar" type='submit'  value="Modificar" />
                        </h4>
                         </form>
                    	</div>
                    	<div style="clear: both"></div>
                  </div> <!-- End .module-table-body -->
                </div> <!-- End .module -->
				
				
				
				
				
                
			</div> 
          <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
		
         <?php include_once("z_script/footer.php") ?>
	</body>	
</html>