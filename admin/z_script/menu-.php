
<!-- Header -->
        <div id="header">
            <!-- Header. Status part -->
            <div id="header-status">    
                <div class="container_12">
                    <div class="grid_8">
					<div style=" float:left; color: #6dc6e7; margin-top:10px;">
                    Bienvenido <?php echo utf8_encode($_SESSION["colegio"]); ?>
                    </div>
                    </div>
                    <div class="grid_4">
                        <a href="logout.php" id="logout">
                        Cerrar Sesión
                        </a>
                    </div>
                </div>    
                <div style="clear:both;"></div>
            </div> <!-- End #header-status -->
            
            <!-- Header. Main part -->
            <div id="header-main">
                <div class="container_12">
                    <div class="grid_12">
                        <div id="logo">
                            
                            <ul id="nav">
                            
                            	<?php if($_SESSION["tipo_priv"]== "Coordinador"){?>	

                                <li <?php if($_SESSION["var_ban"]==1){echo "id='current'";}?> ><a href="panel_adm.php">Inicio</a></li>
                                <li <?php if($_SESSION["var_ban"]==3){echo "id='current'";}?> ><a href="inf_bec_coor.php">Consultas Becas Tzitlacalli</a></li>
                            
								<?php }elseif($_SESSION["tipo_priv"]== "Colegio"){?>	

                                <li <?php if($_SESSION["var_ban"]==1){echo "id='current'";}?> ><a href="panel_adm.php">Inicio</a></li>
                                <li <?php if($_SESSION["var_ban"]==3){echo "id='current'";}?> ><a href="inf_bec_colg.php">Consultas Becas</a></li>
                            
								<?php } elseif($_SESSION["tipo_priv"]== "Administrador"){?>
                            
                                <li <?php if($_SESSION["var_ban"]==1){echo "id='current'";}?> ><a href="panel_adm.php">Inicio</a></li>
                                <li <?php if($_SESSION["var_ban"]==2){echo "id='current'";}?> ><a href="users.php">Usuarios</a></li>
                                <li <?php if($_SESSION["var_ban"]==3){echo "id='current'";}?> ><a href="report.php">Consultas</a></li>
                                <li <?php if($_SESSION["var_ban"]==5){echo "id='current'";}?> ><a href="date_in.php">Fecha Inicio</a></li> 
                                <li <?php if($_SESSION["var_ban"]==6){echo "id='current'";}?> ><a href="soc_pr.php">Precio Estudio</a></li>
                                <li <?php if($_SESSION["var_ban"]==7){echo "id='current'";}?> ><a href="del_all_db.php">Reinicio Sistema</a></li>
								<li <?php if($_SESSION["var_ban"]==8){echo "id='current'";}?> ><a href="sincroniza_db.php">Sincronizar</a></li> 
                                <?php }?>
                            </ul>
                        </div><!-- End. #Logo -->
                    </div><!-- End. .grid_12-->
                    <div style="clear: both;"></div>
                </div><!-- End. .container_12 -->
            </div> <!-- End #header-main -->
            <div style="clear: both;"></div>
            <?php if($_SESSION["var_ban"]==3 || $_SESSION["var_ban"]==4){?>
            <!-- Sub navigation -->
            <div id="subnav">
            	<?php if($_SESSION["var_ban"]==3){?>
                <div class="container_12">
                    <div class="grid_12">
                        <ul>
                        		<?php if($_SESSION["tipo_priv"]== "Coordinador"){?>	

                            	<li><a href="inf_bec_coor.php">Becas Terminadas Tzitlacalli</a></li>
                                
								<?php } elseif($_SESSION["tipo_priv"]== "Colegio"){?>	

                            	<li><a href="inf_bec_colg.php">Becas Terminadas</a></li>
                            
								<?php } elseif($_SESSION["tipo_priv"]== "Administrador"){?>
                            
                                <li><a href="report.php">Usuarios Familia</a></li>
                                <li><a href="inf_ft.php">Información Padre</a></li>
                                <li><a href="inf_mt.php">Información Madre</a></li>
                                <li><a href="inf_al.php">Información Alumnos</a></li>
                                <li><a href="inf_bec.php">Becas Terminadas</a></li> 
                                <?php }?>
                            
                        </ul>
                    </div><!-- End. .grid_12-->
                </div><!-- End. .container_12 -->
                <?php }?>
                <?php if($_SESSION["var_ban"]==4 && $_SESSION["tipo_priv"]== "Administrador"){ ?>
                <div class="container_12">
                    <div class="grid_12">
                        <ul>
                            <li><a href="import.php">Importación Usuarios Familia</a></li>
                            <li><a href="import_ft.php">Importación Padre</a></li>
                            <li><a href="import_mt.php">Importación Madre</a></li>
                            <li><a href="import_al.php">Importación Alumnos</a></li>
                        </ul>
                    </div><!-- End. .grid_12-->
                </div><!-- End. .container_12 -->
                <?php }?>
                <div style="clear: both;"></div>
            </div> <!-- End #subnav -->
            <?php }?>        
        </div> <!-- End #header -->
        
		
        