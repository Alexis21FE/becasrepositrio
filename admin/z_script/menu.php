
<!-- Header -->
<div id="header">
    <!-- Header. Status part -->  
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" <?php echo ($_SESSION['periodo']=='proximo_periodo')?'style="background-color:#393992 !important;"':'style="background-color:black'?>>
        <a class="navbar-brand" href="#"> Bienvenido <?php echo $_SESSION["colegio"]; ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
                <?php if ($_SESSION["tipo_priv"] == "Colegio") { ?>	

                    <li class="nav-item" <?php if ($_SESSION["var_ban"] == 1) {
                    echo "id='current'";
                } ?> ><a class="nav-link" href="test.php">Inicio</a></li>
                    <li class="nav-item" <?php if ($_SESSION["var_ban"] == 3) {
                    echo "id='current'";
                } ?> ><a class="nav-link" href="inf_bec_colg.php">Consultas Becas</a></li>	
                    <?php
                    if ($_SESSION["colegio"] == 'Universidad Panamericana') {
                        ?>		
                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 6) {
                            echo "id='current'";
                        } ?> ><a  class="nav-link" href="reporte_up_col.php">Validación UP</a></li>										
                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 9) {
                    echo "id='current'";
                } ?> ><a class="nav-link" href="report_ingresos_up.php">Ingresos Familia</a></li>						

                        <?php }//UP 
                    ?>


                    <?php
                    if ($_SESSION["colegio"] != 'Universidad Panamericana') {
                        ?>
                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 9) {
                            echo "id='current'";
                        } ?> ><a class="nav-link" href="report_ingresos.php">Ingresos Familia</a></li>																
                                <?php
                            }
                        } elseif ($_SESSION["tipo_priv"] == "Administrador") {
                            ?>

                    <li class="nav-item" <?php if ($_SESSION["var_ban"] == 1) {
                                echo "id='current'";
                            } ?> ><a  class="nav-link"href="test.php">Inicio</a></li>
                    <li class="nav-item" <?php if ($_SESSION["var_ban"] == 2) {
                            echo "id='current'";
                        } ?> ><a class="nav-link" href="users.php">Usuarios</a></li>
                    <li class="nav-item dropdown" <?php if ($_SESSION["var_ban"] == 3) {
                            echo "id='current'";
                        } ?> ><a  class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Consultas</a>
                        <div class="dropdown-menu">
    <?php if ($_SESSION["tipo_priv"] == "Colegio") { ?>	

                                <a class="dropdown-item" href="inf_bec_colg.php">Solicitudes Terminadas</a>
        <?php
        if ($_SESSION["colegio"] == 'Universidad Panamericana') {
            ?>
                                    <a class="dropdown-item" href="report_col.php">Usuarios Familia</a>
            <?php
        }
        ?>

    <?php } elseif ($_SESSION["tipo_priv"] == "Administrador") { ?>
                               
                                <a class="dropdown-item" href="report1.php">Usuarios Familia</a>
                         
                                <a class="dropdown-item" href="inf_ft.php">Información Familia</a>
                                <!-- <li><a href="inf_mt.php">Información Madre</a></li>-->
                                <a class="dropdown-item" href="inf_alumno.php">Información Alumnos</a>
                                <?php if($_SESSION['periodo']=='periodo_actual'){?>
                                <a class="dropdown-item" href="inf_bec.php">Solicitudes Terminadas</a> 
                                <?php }  else{?>
                                    <a class="dropdown-item" href="inf_bec.php">Solicitudes Terminadas Proximo Periodo</a> 
                                <?php } ?>

                                       <a class="dropdown-item" href="report_up_pagares.php">Pagares Los Altos</a>
                               <?php } ?>


                        </div>
                    </li>
                    <li class="nav-item" <?php if ($_SESSION["var_ban"] == 5) {
                    echo "id='current'";
                } ?> ><a class="nav-link" href="date_in.php">Configuración</a></li>
                    <li class="nav-item" <?php if ($_SESSION["var_ban"] == 6) {
                    echo "id='current'";
                } ?> ><a class="nav-link" href="reporte_up.php">Validación UP</a></li> 
    <!--                                <li class="nav-item" <?php /* if($_SESSION["var_ban"]==6){echo "id='current'";} */ ?> ><a href="soc_pr.php">Precio Estudio</a></li> -->
                    <li class="nav-item" <?php if ($_SESSION["var_ban"] == 7) {
                    echo "id='current'";
                } ?> ><a class="nav-link" href="delete_all_db.php">Reinicio Sistema</a></li>
            <!--					<li class="nav-item" <?php if ($_SESSION["var_ban"] == 8) {
                    echo "id='current'";
                } ?> ><a href="sincroniza_db.php">Sincronizar</a></li> -->
                    <li class="nav-item" <?php if ($_SESSION["var_ban"] == 8) {
                    echo "id='current'";
                } ?> ><a class="nav-link" href="sincroniza_familias1.php">Sincronizar</a></li>
                 <li class="nav-item" <?php if ($_SESSION["var_ban"] == 9) {
                    echo "id='current'";
                } ?> ><a class="nav-link" href="estudiosAdmin.php">Estudios</a></li>
    <?php
    if ($_SESSION["colegio"] != 'Universidad Panamericana') {
        ?>
                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 10) {
            echo "id='current'";
        } ?> ><a class="nav-link" href="reporte_ingresos.php">Ingresos Familia</a></li>																
        <?php
    }
       ?> <li style="margin: auto;">
      <?php if($_SESSION['periodo']=='periodo_actual'){
          $checked_actual="checked";
          $checked_proximo="";
      }else{
          $checked_actual="";
          $checked_proximo="checked";  
      }?>
 <input type="radio" name="interruptor" id="prendido" <?php echo $checked_proximo; ?> > 
  <input type="radio" name="interruptor" id="apagado"  <?php echo $checked_actual; ?> > 

  <div class="interruptor-cuerpo" >
      <div class="interruptor-tecla">
          <label class="label_dis" id="prendido" for="prendido" title="Desactivado">___</label>
          <label class="label_dis" id="apagado" for="apagado" title="Activado">___</label>
      </div>    
  </div>
    </li><?php
}elseif($_SESSION["tipo_priv"] == "Coordinador"){?>
       <li class="nav-item"><a class="nav-link" href="test.php">Inicio</a></li>
                       <li class="nav-item" ><a class="nav-link" href="inf_bec_coor.php">Consultas Becas Tzitlacalli</a></li> 
                  <li class="nav-item"><a class="nav-link" href="report_ingresos.php">Ingresos Familia</a></li>  
<?php }
?>  <li class="nav-item"><a href="#" class="nav-link"> <label></label></a></li>
            </ul>
          
            <form class="form-inline my-2 my-lg-0">
                <a href="logout.php" id="logout">
                    Cerrar Sesión <i class="fas fa-sign-out-alt"></i>
                </a>
            </form>
        </div>
    </nav>
    <div style="clear: both;"></div>

</div> <!-- End #header -->


<!--div id="header-status">    
      <div class="container_12">
          <div class="grid_8">
                              <div style=" float:left; color: #6dc6e7; margin-top:10px;">
         
          </div>
          </div>
          <div class="grid_4">
              <a href="logout.php" id="logout">
              Cerrar Sesión <i class="fas fa-sign-out-alt"></i>
              </a>
          </div>
      </div>    
      <div style="clear:both;"></div>
  </div> <!-- End #header-status -->

<!-- Header. Main part -->
<!--div id="header-main">
    <div class="container_12">
        <div class="grid_12">
            <div id="logo">
                <ul id="nav">
                
<?php if ($_SESSION["tipo_priv"] == "Coordinador") { ?>	

                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 1) {
        echo "id='current'";
    } ?> ><a href="test.php">Inicio</a></li>
                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 3) {
        echo "id='current'";
    } ?> ><a href="inf_bec_coor.php">Consultas Becas Tzitlacalli</a></li>
                    
<?php } elseif ($_SESSION["tipo_priv"] == "Colegio") { ?>	

                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 1) {
        echo "id='current'";
    } ?> ><a href="test.php">Inicio</a></li>
                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 3) {
        echo "id='current'";
    } ?> ><a href="inf_bec_colg.php">Consultas Becas</a></li>	
    <?php
    if ($_SESSION["colegio"] == 'Universidad Panamericana') {
        ?>		
                                                                            <li class="nav-item" <?php if ($_SESSION["var_ban"] == 6) {
            echo "id='current'";
        } ?> ><a href="report_up_col.php">Validaci&oacute;n UP</a></li>										
                                                                            <li class="nav-item" <?php if ($_SESSION["var_ban"] == 9) {
            echo "id='current'";
        } ?> ><a href="report_ingresos_up.php">Ingresos Familia</a></li>						
                                                                    
        <?php }//UP 
    ?>
                                                                
                                                        
    <?php
    if ($_SESSION["colegio"] != 'Universidad Panamericana') {
        ?>
                                                                    <li class="nav-item" <?php if ($_SESSION["var_ban"] == 9) {
            echo "id='current'";
        } ?> ><a href="report_ingresos.php">Ingresos Familia</a></li>																
        <?php
    }
} elseif ($_SESSION["tipo_priv"] == "Administrador") {
    ?>
                    
                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 1) {
        echo "id='current'";
    } ?> ><a href="test.php">Inicio</a></li>
                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 2) {
        echo "id='current'";
    } ?> ><a href="users.php">Usuarios</a></li>
                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 3) {
        echo "id='current'";
    } ?> ><a href="report.php">Consultas</a></li>
                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 5) {
        echo "id='current'";
    } ?> ><a href="date_in.php">Configuraci&oacute;n</a></li>
                                                        <li class="nav-item" <?php if ($_SESSION["var_ban"] == 6) {
        echo "id='current'";
    } ?> ><a href="report_up.php">Validaci&oacute;n UP</a></li> 
    <!--                                <li class="nav-item" <?php /* if($_SESSION["var_ban"]==6){echo "id='current'";} */ ?> ><a href="soc_pr.php">Precio Estudio</a></li> -->
                        <!--li class="nav-item" <?php if ($_SESSION["var_ban"] == 7) {
        echo "id='current'";
    } ?> ><a href="del_all_db.php">Reinicio Sistema</a></li>
                <!--					<li class="nav-item" <?php if ($_SESSION["var_ban"] == 8) {
        echo "id='current'";
    } ?> ><a href="sincroniza_db.php">Sincronizar</a></li> -->
                <!--li class="nav-item" <?php if ($_SESSION["var_ban"] == 8) {
        echo "id='current'";
    } ?> ><a href="sincroniza_familias.php">Sincronizar</a></li>
    <?php
    if ($_SESSION["colegio"] != 'Universidad Panamericana') {
        ?>
                                                                            <li class="nav-item" <?php if ($_SESSION["var_ban"] == 9) {
            echo "id='current'";
        } ?> ><a href="report_ingresos.php">Ingresos Familia</a></li>																
        <?php
    }
}
?>
                </ul>
            </div><!-- End. #Logo -->
<!--/div><!-- End. .grid_12-->
<!--div style="clear: both;"></div>
</div><!-- End. .container_12 -->
<!--/div--> <!-- End #header-main -->
<?php if ($_SESSION["var_ban"] == 3 || $_SESSION["var_ban"] == 4) { ?>
    <!-- Sub navigation -->
    <!--div id="subnav">
    <?php if ($_SESSION["var_ban"] == 3) { ?>
            <div class="container_12">
                <div class="grid_12">
                    <ul>
        <?php if ($_SESSION["tipo_priv"] == "Coordinador") { ?>	

                            <li><a class="" href="inf_bec_coor.php">Solicitudes Terminadas Tzitlacalli</a></li>
                                
        <?php } elseif ($_SESSION["tipo_priv"] == "Colegio") { ?>	

                                <li><a class href="inf_bec_colg.php">Solicitudes Terminadas</a></li>
            <?php
            if ($_SESSION["colegio"] == 'Universidad Panamericana') {
                ?>
                                                                    <li><a href="report_col.php">Usuarios Familia</a></li>
                <?php
            }
            ?>
                            
        <?php } elseif ($_SESSION["tipo_priv"] == "Administrador") { ?>
                            
                                <li><a class href="report.php">Usuarios Familia</a></li>
                                <li><a class href="inf_ft.php">Información Familia</a></li>
            <!-- <li><a href="inf_mt.php">Información Madre</a></li>-->
            <!--li><a class href="inf_al.php">Información Alumnos</a></li>
            <li><a class href="inf_bec.php">Solicitudes Terminadas</a></li> 
        <?php } ?>

        </ul>
        </div><!-- End. .grid_12-->
        <!--/div><!-- End. .container_12 -->
    <?php } ?>
    <?php if ($_SESSION["var_ban"] == 4 && $_SESSION["tipo_priv"] == "Administrador") { ?>
        <!--div class="container_12">
            <div class="grid_12">
                <ul>
                    <li><a class href="import.php">Importación Usuarios Familia</a></li>
                    <li><a class href="import_ft.php">Importación Padre</a></li>
                    <li><a class href="import_mt.php">Importación Madre</a></li>
                    <li><a class href="import_al.php">Importación Alumnos</a></li>
                </ul>
            </div><!-- End. .grid_12-->
        <!--/div><!-- End. .container_12 -->
    <?php } ?>
    <!--div style="clear: both;"></div>
    </div--> <!-- End #subnav -->
<?php } ?>   