<?php 
session_start();
/*if($_SESSION["tipo"] == "Administrador")
	{
		
	} else{ header ("Location: index.php"); }
	*/
$lib_rel_path = (count(explode("/", $_SERVER['SCRIPT_NAME']))-2 > 0 ? str_repeat("../", count(explode("/", $_SERVER['SCRIPT_NAME']))-2) : "");

include_once("../z_script/db_class.php");
mysql_select_db($bd_becas,$link);

include_once("../z_script/PHPExcel/Classes/PHPExcel.php");

$consulta="SELECT * FROM user_fam, inf_alum where user_fam.id_fam=inf_alum.id_fam
and alum_colg='Universidad Panamericana' ORDER BY id_fam ASC";
$result=$pdo->query($consulta);
$i = 4;
$band=1;

$objPHPExcel = new PHPExcel();

$objPHPExcel->
	getProperties()
		->setCreator("Becas Administrator")
		->setLastModifiedBy("Becas Administrator")
		->setTitle("Usuarios Familias")
		->setSubject("Usuarios")
		->setDescription("Documento generado por Colmenares");

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('B1', 'Informacion Usuarios de las Familias')
			->setCellValue('A3', 'Nombre Familia')
			->setCellValue('B3', 'Usuario')
			->setCellValue('C3', 'Contraseña')
			->setCellValue('D3', 'Adeudo')
			->setCellValue('E3', 'Acep. Terminos')
			->setCellValue('F3', 'Pago Soc-Eco.')
			->setCellValue('G3', 'Codigo Verif.')
			->setCellValue('H3', 'Fecha Inicial')
			->setCellValue('I3', 'Fecha Final')
			->setCellValue('J3', 'Observaciones');
			/*->setCellValue('A3', 'Usuario')
			->setCellValue('B3', 'Contraseña')
			->setCellValue('C3', 'Adeudo')
			->setCellValue('D3', 'Acep. Terminos')
			->setCellValue('E3', 'Pago Soc-Eco.')
			->setCellValue('F3', 'Codigo Verif.')
			->setCellValue('G3', 'Fecha Inicial')
			->setCellValue('H3', 'Fecha Final')
			->setCellValue('I3', 'Observaciones');*/
			
foreach ($result as $row)
{

	$fam_user	= $row["fam_user"];
	$fam_pass	= $row["fam_pass"];
	$fam_ade	= $row["fam_ade"];
	$fam_term	= $row["fam_term"];
	$pago_soc	= $row["pago_soc"];
	$cod_verif	= $row["cod_verif"];
	$fech_inc	= $row["fech_inc"];
	$fech_fin	= $row["fech_fin"];
	$obser_fam	= $row["obser_fam"];
	$nfamilia	= $row["nombre_familia"];


	$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue("A".$i."", utf8_encode($nfamilia))
			->setCellValue("B".$i."", $fam_user)
			->setCellValue("C".$i."", $fam_pass)
            ->setCellValue("D".$i."", $fam_ade)
			->setCellValue("E".$i."", $fam_term)
			->setCellValue("F".$i."", $pago_soc)
			->setCellValue("G".$i."", $cod_verif)
			->setCellValue("H".$i."", $fech_inc)
			->setCellValue("I".$i."", $fech_fin)
			->setCellValue("J".$i."", utf8_encode($obser_fam));
            /*->setCellValue("A".$i."", $fam_user)
			->setCellValue("B".$i."", $fam_pass)
            ->setCellValue("C".$i."", $fam_ade)
			->setCellValue("D".$i."", $fam_term)
			->setCellValue("E".$i."", $pago_soc)
			->setCellValue("F".$i."", $cod_verif)
			->setCellValue("G".$i."", $fech_inc)
			->setCellValue("H".$i."", $fech_fin)
			->setCellValue("I".$i."", $obser_fam);*/						
	$i++;


}

for ($col = 'A'; $col != 'J'; $col++) {
	$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->setTitle('Usuarios Familias Administrador');
$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="userfam_info.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;


?>