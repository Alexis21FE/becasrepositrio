<?php
session_start();
header('Content-type: application/json; charset=utf-8');
$jsonarray=array();
include('conexion.php');
$action=$_REQUEST['accion'];
$idFam=$_REQUEST['id_fam'];
$userFam=$_REQUEST['usuario'];

if($action=='addAlumno'){
	$nombreAlumno=$_REQUEST['nombreAlumno'];
	$apellidoPaterno=$_REQUEST['apellidoPaterno'];
	$apellidoMaterno=$_REQUEST['apellidoMaterno'];
	$fechaNacimiento=$_REQUEST['fechaNacimientoAlumno'];
	$edadAlumno=$_REQUEST['edadAlumno'];
	$promedio=$_REQUEST['promedio'];
	$GradoAIngresar=$_REQUEST['GradoAIngresar'];
	$colegioIngresar=$_REQUEST['colegioIngresar'];
	$porcentaje=$_REQUEST['porcentaje'];
	$porcentaje2=$_REQUEST['porcentaje2'];
	$sqlInsert="Insert into inf_alum (idFam,userFam,nombre,apellidoPaterno,apellidoMaterno,fechaNacimiento,edad,nivelgradoIngresar,colegioIngresar,promedio,porcentajeActual,porcentajeSolicitado) values($idFam,'".$userFam."','".$nombreAlumno."','".$apellidoPaterno."','".$apellidoMaterno."','".$fechaNacimiento."',$edadAlumno,'".$GradoAIngresar."',$promedio,$colegioIngresar,$porcentaje,$porcentaje2)";
	try{
		$res=$pdo_estudios->query($sqlInsert);
		if($res){
			$jsonarray['status']=true;
			$jsonarray['alumno']=array("Nombre"=>$nombreAlumno." ".$apellidoPaterno." ".$apellidoMaterno,"fechaNacimiento"=>$fechaNacimiento,"Edad"=>$edadAlumno,"GradoAIngresar"=>$GradoAIngresar,"Promedio"=>$promedio,"colegioIngresar"=>$colegioIngresar,"porcentaje"=>$porcentaje,"porcentaje2"=>$porcentaje2);
			echo json_encode($jsonarray);
		}else{
			$jsonarray['status']=false;
			$jsonarray['mensaje']="Error al insertar el usuario";
			echo json_encode($jsonarray);
		}
	}catch(Exception $e){
		echo "error";
	}
}if($action=='deleteAlumno'){
	$res=$pdo_estudios->query("Delete from inf_alum where id=".$_REQUEST['idAlumno']." ");
	if($res){
		$jsonarray['status']=true;
		echo json_encode($jsonarray);
	}else{
		$jsonarray['status']=false;
		echo json_encode($jsonarray);
	}
}
if($action=='addAsociacion'){
	$perteneceA=$_REQUEST['perteneceA'];
	$marque=$_REQUEST['marque'];
	$nombreAsociacion=$_REQUEST['nombreAsociacion'];
	$cargoOcupa=$_REQUEST['cargoOcupa'];
	$sqlInsert="Insert into interesCultural (idFam,userFam,perteneceA,marque,nombreAsociacion,cargoOcupa) values(".$idFam.",'".$userFam."','".$perteneceA."','".$marque."','".$nombreAsociacion."','".$cargoOcupa."')";
//echo $sqlInsert;
  $res=$pdo_estudios->query($sqlInsert);

	if($res){
    $sql="SELECT max(idAso) as idAso FROM interesCultural";

    $res=$pdo_estudios->query($sql);
		$jsonarray['perteneceA']=$perteneceA;
		$jsonarray['marque']=$marque;
		$jsonarray['nombreAsociacion']=$nombreAsociacion;
		$jsonarray['cargoOcupa']=$cargoOcupa;
    foreach($res as $row){
      $jsonarray['idAso']=$row['idAso'];
    }
$jsonarray['status']=true;
		$jsonarray['mensaje']="Exito al agregar la asociacion";
	}else{
		$jsonarray['status']=false;
		$jsonarray['mensaje']="Ha ocurrido un error";
	}
	echo json_encode($jsonarray);
}if($action=='delete_asociacion'){
	$id=$_REQUEST['idAso'];
	$sqlDeleteInt="Delete from interesCultural where idAso=".$id." ";
	if($pdo_estudios->query($sqlDeleteInt)){
		$jsonarray['status']=true;
		$jsonarray['mensaje']="Exito al eliminar la asociacion";
	}else{
		$jsonarray['status']=false;
		$jsonarray['mensaje']="Ha ocurrido un error";
	}
	echo json_encode($jsonarray);
}if($action=='addRefenrecia'){
	$nombreReferencia=$_REQUEST['nombreReferencia'];
	$telefonoReferencia=$_REQUEST['telefonoReferencia'];
	$relacionReferencia=$_REQUEST['relacionReferencia']	;
	$comentarioRefenrecia=$_REQUEST['comentarioRefenrecia'];
	$sqlInsert="Insert into referenciaFamilia (idFam,userFam,nombre,relacion,telefono,comentarios) values  ( $idFam,'".$userFam."','".$nombreReferencia."','".$telefonoReferencia."','".$relacionReferencia."','".$comentarioRefenrecia."')";
//echo $sqlInsert;
  $res=$pdo_estudios->query($sqlInsert);

  if($res){
    $sql="SELECT max(idRef) as idRef FROM referenciaFamilia";

    $res=$pdo_estudios->query($sql);
    $jsonarray['nombreReferencia']=$nombreReferencia;
    $jsonarray['telefonoReferencia']=$telefonoReferencia;
    $jsonarray['relacionReferencia']=$relacionReferencia;
    $jsonarray['comentarioRefenrecia']=$comentarioRefenrecia;
    foreach($res as $row){
      $jsonarray['idRef']=$row['idRef'];
    }
$jsonarray['status']=true;
    $jsonarray['mensaje']="Exito al agregar la referencia";
  }else{
    $jsonarray['status']=false;
    $jsonarray['mensaje']="Ha ocurrido un error";
  }

	echo json_encode($jsonarray);
}if($action=='deleteRef'){
  $id=$_REQUEST['idRef'];
	$sqlDeleteInt="Delete from referenciaFamilia where idRef=".$id." ";
	if($pdo_estudios->query($sqlDeleteInt)){
		$jsonarray['status']=true;
		$jsonarray['mensaje']="Exito al eliminar la referencia";
	}else{
		$jsonarray['status']=false;
		$jsonarray['mensaje']="Ha ocurrido un error";
	}
	echo json_encode($jsonarray);
}if($action=='add_dependiente'){

	(!empty($_REQUEST['name_dep']))?$nombre=$_REQUEST['name_dep']:$nombre='';
	(!empty($_REQUEST['edad_dep']))?$edad=$_REQUEST['edad_dep']:$edad=0;
	(!empty($_REQUEST['ocupacion_dep']))?$ocu_=$_REQUEST['ocupacion_dep']:$ocu_='';
	(!empty($_REQUEST['dep_escolaridad']))?$dep_escolaridad=$_REQUEST['dep_escolaridad']:$grad=0;
	(!empty($_REQUEST['pornce_dep']))?$porc=$_REQUEST['pornce_dep']:$porc=0;
	(!empty($_REQUEST['dep_institucion']))?$dep_institucion=$_REQUEST['dep_institucion']:$plan='';
	$ciclo=date('Y');

	$sql_dep=" INSERT INTO dep_econom(id_fam, nomb_comp, dep_edad, dep_ocup, dep_escolaridad, dep_institucion, dep_porc_beca, usuario, ciclo) VALUES (".$idFam.",'".$nombre."','".$edad."','".$ocu_."','".$dep_escolaridad."','".$dep_institucion."','".$porc."','".$userFam."',".$ciclo.") ";

	$result_dep = $pdo_estudios->query($sql_dep);

	if($result_dep){
		$sql="SELECT max(id_dep) as id_dep FROM dep_econom";

		$res=$pdo_estudios->query($sql);
		foreach($res as $row){
			$id_dep=$row['id_dep'];
		}
		$jsonarray['status']=true;
		$jsonarray['mensaje']="Se ha agregado correctamente";
		$jsonarray['id_fam']=$idFam;
		$jsonarray['nombre']=$nombre;
		$jsonarray['edad']=$edad;
		$jsonarray['ocu']=$ocu_;
		$jsonarray['dep_institucion']=$dep_institucion;
		$jsonarray['dep_escolaridad']=$dep_escolaridad;
		$jsonarray['porc']=$porc;
		$jsonarray['usuario']=$userFam;
		$jsonarray['id_dep']=$id_dep;
	}else{
		$jsonarray['status']=false;
		$jsonarray['mensaje']="Ha ocurrido un error";
	}

	echo json_encode($jsonarray);

}if($action=='deleteDependiente'){
	$id=$_REQUEST['idDep'];
	$sqlDeleteDep="Delete from dep_econom where id_dep=".$id." ";
  //echo $sqlDeleteDep;
	if($pdo_estudios->query($sqlDeleteDep)){
		$jsonarray['status']=true;
		$jsonarray['mensaje']="Exito al eliminar dependienter";
	}else{
		$jsonarray['status']=false;
		$jsonarray['mensaje']="Ha ocurrido un error";
	}
	echo json_encode($jsonarray);
}if($action=='agregar_veh'){
	$marca_veh= $_REQUEST['marca_veh'];
	$veh_nombre=$_REQUEST['veh_nombre'];
	$veh_modelo=$_REQUEST['veh_modelo'];
	$veh_estimado=$_REQUEST['veh_estimado'];
	$veh_prop=$_REQUEST['veh_prop'];
	$sql_veh=" INSERT INTO vehiculos(id_fam, marca, nombre, modelo,estimado,propietario, usuario, ciclo) VALUES ($idFam,'".$marca_veh."','".$veh_nombre."','".$veh_modelo."','".$veh_estimado."','".$veh_prop."','".$userFam."',2019) ";

	$res=$pdo_estudios->query($sql_veh);
	if($res){
		$jsonarray['status']=true;
		$jsonarray['mensaje']="Se ha agregado el vehiculo correctamente";
		$jsonarray['veh_modelo']=$veh_modelo;
		$jsonarray['veh_nombre']=$veh_nombre;
		$jsonarray['marca_veh']=$marca_veh;
		$jsonarray['veh_prop']=$veh_prop;
		$jsonarray['veh_estimado']=$veh_estimado;
		$sql_="Select max(id_veh) as id_veh from vehiculos";

		$res=$pdo->query($sql_);
		foreach($res as $row){
			$id_veh=$row['id_veh'];
		}
		$jsonarray['id_veh']=$id_veh;
	}else{
		$jsonarray['status']=false;
		$jsonarray['mensaje']="Ha ocurrido un error al agregar el vehiculo";
	}
	echo json_encode($jsonarray);
}if($action=='deleteVehiculo'){
	$id=$_REQUEST['idVeh'];
	$sqldeleVehiculo="Delete from vehiculos where id_veh=".$id." ";
	if($pdo_estudios->query($sqldeleVehiculo)){
		$jsonarray['status']=true;
		$jsonarray['mensaje']="Exito al eliminar vehiculo";
	}else{
		$jsonarray['status']=false;
		$jsonarray['mensaje']="Ha ocurrido un error";
	}
	echo json_encode($jsonarray);
}if($action=='GuardarCerrar'){
$res=guardarDatos('Cerrar');
if($res){
		$jsonarray['status']=true;
		$jsonarray['action']='close';
		$jsonarray['mensaje']="Exito al guardar y cerrar solicitud";
	}else{
		$jsonarray['status']=false;
		$jsonarray['action']='close';
		$jsonarray['mensaje']="Ha ocurrido un error";
	}
	echo json_encode($jsonarray);
}if($action=='GuardarOnly'){
$res=guardarDatos('Guardar');
	if($res){
		$jsonarray['status']=true;
		$jsonarray['mensaje']="Exito al guardar la solicitud";
	}else{
		$jsonarray['status']=false;
		$jsonarray['mensaje']="Ha ocurrido un error";
	}
	echo json_encode($jsonarray);
}
 function guardarDatos($action){

$idFam=$_REQUEST['id_fam'];
$userFam=$_REQUEST['usuario'];

include('conexion.php');
$fechaEntrevista=$_REQUEST['fechaEntrevista']; 
$noFamilia=$_REQUEST['noFamilia']; 
$Familia=$_REQUEST['Familia']; 
$domicilio=$_REQUEST['domicilio']; 
$municipio=$_REQUEST['municipio']; 
$colonia=$_REQUEST['colonia']; 
$codigo_postal=$_REQUEST['codigo_postal']; 
$tel_casa=$_REQUEST['tel_casa']; 
$telefono2=$_REQUEST['telefono2']; 
$linkGoogle=$_REQUEST['linkGoogle']; 
$padre_nombre=$_REQUEST['padre_nombre']; 
$padre_ap=$_REQUEST['padre_ap']; 
$padre_mt=$_REQUEST['padre_mt']; 
$fechaNacimiento=$_REQUEST['fechaNacimiento']; 
$edad=$_REQUEST['edad']; 
$lugarNacimiento=$_REQUEST['lugarNacimiento']; 
$estadoCivil=$_REQUEST['estadoCivil']; 
$padre_ocupacion=$_REQUEST['padre_ocupacion']; 
$padre_celular=$_REQUEST['padre_celular']; 
$padre_email=$_REQUEST['padre_email']; 
$padre_empresa=$_REQUEST['padre_empresa']; 
$padre_empresa_giro=$_REQUEST['padre_empresa_giro']; 
$padre_puesto=$_REQUEST['padre_puesto']; 
$padre_antiguedad=$_REQUEST['padre_antiguedad']; 
$Sueldo=$_REQUEST['Sueldo']; 
$otrosIngresos=$_REQUEST['otrosIngresos']; 
$observacionesPadre=$_REQUEST['observacionesPadre']; 
$madre_nombre=$_REQUEST['madre_nombre']; 
$madre_ap=$_REQUEST['madre_ap']; 
$madre_mt=$_REQUEST['madre_mt']; 
$madrefechaNacimiento=$_REQUEST['madrefechaNacimiento']; 
$madreEdad=$_REQUEST['madreEdad']; 
$madreLugarNacimiento=$_REQUEST['madreLugarNacimiento']; 
$madreEstadoCivil=$_REQUEST['madreEstadoCivil']; 
$madre_celular=$_REQUEST['madre_celular']; 
$madre_email=$_REQUEST['madre_email']; 
$madre_empresa=$_REQUEST['madre_empresa']; 
$madre_puesto=$_REQUEST['madre_puesto']; 
$madre_antiguedad=$_REQUEST['madre_antiguedad']; 
$madreSueldo=$_REQUEST['madreSueldo']; 
$madreOtrosIngresos=$_REQUEST['madreOtrosIngresos']; 
$madreObservacionesPadre=$_REQUEST['madreObservacionesPadre']; 
$ciclo=date('Y');
$sqlInserInfoFam="insert into inf_familia (id_fam,
Familia,
domicilio,
municipio,
colonia,
codigo_postal,
tel_casa,
telefono2,
linkGoogle,
padre_nombre,
padre_ap,
padre_mt,
fechaNacimiento,
edad,
lugarNacimiento,
estadoCivil,
padre_ocupacion,
padre_celular,
padre_email,
padre_empresa,
padre_empresa_giro,
padre_puesto,
padre_antiguedad,
Sueldo,
otrosIngresos,
observacionesPadre,
madre_nombre,
madre_ap,
madre_mt,
madrefechaNacimiento,
madreEdad,
madreLugarNacimiento,
madreEstadoCivil,
madre_celular,
madre_email,
madre_empresa,
madre_puesto,
madre_antiguedad,
madreSueldo,
madreOtrosIngresos,
madreObservacionesPadre,ciclo,userFam) value(".$idFam.",'".$Familia."',
'".$domicilio."',
'".$municipio."',
'".$colonia."',
'".$codigo_postal."',
'".$tel_casa."',
'".$telefono2."',
'".$linkGoogle."',
'".$padre_nombre."',
'".$padre_ap."',
'".$padre_mt."',
'".$fechaNacimiento."',
'".$edad."',
'".$lugarNacimiento."',
'".$estadoCivil."',
'".$padre_ocupacion."',
'".$padre_celular."',
'".$padre_email."',
'".$padre_empresa."',
'".$padre_empresa_giro."',
'".$padre_puesto."',
'".$padre_antiguedad."',
'".$Sueldo."',
'".$otrosIngresos."',
'".$observacionesPadre."',
'".$madre_nombre."',
'".$madre_ap."',
'".$madre_mt."',
'".$madrefechaNacimiento."',
'".$madreEdad."',
'".$madreLugarNacimiento."',
'".$madreEstadoCivil."',
'".$madre_celular."',
'".$madre_email."',
'".$madre_empresa."',
'".$madre_puesto."',
'".$madre_antiguedad."',
'".$madreSueldo."',
'".$madreOtrosIngresos."',
'".$madreObservacionesPadre."',".$ciclo.",'".$userFam."')";
$res=$pdo_estudios->query("DELETE FROM inf_familia WHERE id_fam=".$idFam." ");
//echo "DELETE FROM inf_familia WHERE  id_fam=".$idFam."";
//echo $sqlInserInfoFam;
$res1=$pdo_estudios->query($sqlInserInfoFam);
/*if($res && $res1){
$jsonarray['status']=true;
		$jsonarray['mensaje']="Exito al actualizar datos";
	}else{
		$jsonarray['status']=false;
		$jsonarray['mensaje']="Ha ocurrido un error";
	}*/
	//echo json_encode($jsonarray);

/*Bienes */
$ubicacionTerreno=$_REQUEST['ubicacionTerreno']; 
$valorEstimadoTerreno=$_REQUEST['valorEstimadoTerreno']; 
$propiedadNombre=$_REQUEST['propiedadNombre']; 
$añosViviendo=$_REQUEST['añosViviendo']; 
$numeroPersonas=$_REQUEST['numeroPersonas']; 
$tipoVivienda=$_REQUEST['tipoVivienda']; 
$statusVivienda=$_REQUEST['statusVivienda']; 
$superficie=$_REQUEST['superficie']; 
$construccionMetros=$_REQUEST['construccionMetros']; 
$valorEstimado=$_REQUEST['valorEstimado']; 
$domitorios=$_REQUEST['domitorios']; 
$numeroBaños=$_REQUEST['numeroBaños']; 
$numeroSalas=$_REQUEST['numeroSalas']; 
$numeroPatios=$_REQUEST['numeroPatios']; 
$numeroCorredores=$_REQUEST['numeroCorredores']; 
$cocheras=$_REQUEST['cocheras']; 
$otrosHabitaciones=$_REQUEST['otrosHabitaciones']; 
$numeroTelefonos=$_REQUEST['numeroTelefonos']; 
$numeroComputadoras=$_REQUEST['numeroComputadoras']; 
$microondas=$_REQUEST['microondas']; 
$numeroDvd=$_REQUEST['numeroDvd']; 
$teatroEnCasa=$_REQUEST['teatroEnCasa']; 
$mozo=$_REQUEST['mozo']; 
$jardinero=$_REQUEST['jardinero']; 
$internet=$_REQUEST['internet']; 
$empleadoDomestico=$_REQUEST['empleadoDomestico']; 
$chofer=$_REQUEST['chofer']; 
$cocinera=$_REQUEST['cocinera']; 
$tvxCable=$_REQUEST['tvxCable']; 
$plusValia=$_REQUEST['plusValia']; 
$condicionesInmueble=$_REQUEST['condicionesInmueble']; 
$observaciones=$_REQUEST['observaciones']; 
$resDelete=$pdo_estudios->query("Delete from bien_pat where id_fam=".$idFam." ");
$sqlInsertBien="insert into bien_pat
(id_fam,
ciclo,
userFam,
ubicacionTerreno,
valorEstimadoTerreno,
propiedadNombre,
añosViviendo,
numeroPersonas,
tipoVivienda,
statusVivienda,
superficie,
construccionMetros,
valorEstimado,
domitorios,
numeroBaños,
numeroSalas,
numeroPatios,
numeroCorredores,
cocheras,
otrosHabitaciones,
numeroTelefonos,
numeroComputadoras,
microondas,
numeroDvd,
teatroEnCasa,
mozo,
jardinero,
internet,
empleadoDomestico,
chofer,
cocinera,
tvxCable,
plusValia,
condicionesInmueble,
observaciones) values (
".$idFam.",
".$ciclo.",
'".$userFam."',
'".$ubicacionTerreno."',
'".$valorEstimadoTerreno."',
'".$propiedadNombre."',
'".$añosViviendo."',
'".$numeroPersonas."',
'".$tipoVivienda."',
'".$statusVivienda."',
'".$superficie."',
'".$construccionMetros."',
'".$valorEstimado."',
'".$domitorios."',
'".$numeroBaños."',
'".$numeroSalas."',
'".$numeroPatios."',
'".$numeroCorredores."',
'".$cocheras."',
'".$otrosHabitaciones."',
'".$numeroTelefonos."',
'".$numeroComputadoras."',
'".$microondas."',
'".$numeroDvd."',
'".$teatroEnCasa."',
'".$mozo."',
'".$jardinero."',
'".$internet."',
'".$empleadoDomestico."',
'".$chofer."',
'".$cocinera."',
'".$tvxCable."',	
'".$plusValia."',
'".$condicionesInmueble."',
'".$observaciones."' )";
//	echo $sqlInsertBien;
$resInsert=$pdo_estudios->query($sqlInsertBien);

/*Salud*/
$estadoSaludFamilia=$_REQUEST['estadoSaludFamilia']; 
$espeficique=$_REQUEST['espeficique']; 
$tipoServicioMedico=$_REQUEST['tipoServicioMedico']; 
$cuentaSeguro=$_REQUEST['cuentaSeguro'];
$costoPoliza=$_REQUEST['costoPoliza']; 
$sumaPoliza=$_REQUEST['sumaPoliza']; 
$observacionesMedicas=$_REQUEST['observacionesMedicas']; 
$hipertencion=(isset($_REQUEST['hipertencion'])?$_REQUEST['hipertencion']:0);
$cancer=(isset($_REQUEST['cancer'])?$_REQUEST['cancer']:0);
$diabetes=(isset($_REQUEST['diabetes'])?$_REQUEST['diabetes']:0);
$alcoholismo=(isset($_REQUEST['alcoholismo'])?$_REQUEST['alcoholismo']:0);
$otraEnfermedad=(isset($_REQUEST['otraEnfermedad'])?$_REQUEST['otraEnfermedad']:0);
$sqlInsertSalud="insert into saludFamiliar (
id_fam,
ciclo,
estadoSaludFamilia,
espeficique,
tipoServicioMedico,
cuentaSeguro,
costoPoliza,
sumaPoliza,
observacionesMedicas,
hipertencion,
cancer,
diabetes,
alcoholismo,
otraEnfermedad
) values(
".$idFam.",
".$ciclo.",
'".$estadoSaludFamilia."',
'".$espeficique."',
'".$tipoServicioMedico."',
'".$cuentaSeguro."',
'".$costoPoliza."',
'".$sumaPoliza."',
'".$observacionesMedicas."',
".$hipertencion.",
".$cancer.",
".$diabetes.",
".$alcoholismo.",
".$otraEnfermedad."
)";
$pdo_estudios->query("delete from saludFamiliar where id_fam=".$idFam." ");
$pdo_estudios->query($sqlInsertSalud);
/*Ingresos mensuales*/
$ing_ft=$_REQUEST['ing_ft']; 
$ing_mt=$_REQUEST['ing_mt']; 
$ing_vales=$_REQUEST['ing_vales']; 
$ing_otro=$_REQUEST['ing_otro']; 
$ing_to=$_REQUEST['ing_to']; 
$sqlinserIngresos="INSERT INTO ingre_mensu (
id_fam,
ing_ft,
ing_mt,
ing_vales,
ing_otro,
ing_to,
ciclo
) values (
".$idFam.",
".$ing_ft.",
".$ing_mt.",
".$ing_vales.",
".$ing_otro.",
".$ing_to.",
".$ciclo."
)";
$pdo_estudios->query("delete from ingre_mensu where id_fam=".$idFam." ");
$pdo_estudios->query($sqlinserIngresos);
/*Gastos*/
$gast_alm=$_REQUEST['gast_alm']; 
$gast_rent=$_REQUEST['gast_rent']; 
$gast_prest=$_REQUEST['gast_prest']; 
$gast_elect=$_REQUEST['gast_elect']; 
$gast_agua=$_REQUEST['gast_agua']; 
$gast_gas=$_REQUEST['gast_gas']; 
$gast_auto=$_REQUEST['gast_auto']; 
$gast_hipo=$_REQUEST['gast_hipo']; 
$gast_pred=$_REQUEST['gast_pred']; 
$gast_tel=$_REQUEST['gast_tel']; 
$gast_cable=$_REQUEST['gast_cable']; 
$gast_int=$_REQUEST['gast_int']; 
$gast_colg=$_REQUEST['gast_colg']; 
$gast_clas=$_REQUEST['gast_clas']; 
$gast_serv=$_REQUEST['gast_serv']; 
$gast_lib=$_REQUEST['gast_lib']; 
$gast_diver=$_REQUEST['gast_diver']; 
$gast_medi=$_REQUEST['gast_medi']; 
$gast_denti=$_REQUEST['gast_denti']; 
$gast_vest=$_REQUEST['gast_vest']; 
$gast_seg=$_REQUEST['gast_seg']; 
$gast_viaj=$_REQUEST['gast_viaj']; 
$gast_otros=$_REQUEST['gast_otros']; 
$des_otr_gas=$_REQUEST['des_otr_gas']; 
$gast_total=$_REQUEST['gast_total']; 
$total=$_REQUEST['total']; 
$sqlInsertGastos="Insert into gastos (
id_fam,
usuario,
ciclo,
gast_alm,
gast_rent,
gast_prest,
gast_elect,
gast_agua,
gast_gas,
gast_auto,
gast_hipo,
gast_pred,
gast_tel,
gast_cable,
gast_int,
gast_colg,
gast_clas,
gast_serv,
gast_lib,
gast_diver,
gast_medi,
gast_denti,
gast_vest,
gast_seg,
gast_viaj,
gast_otros,
des_otr_gas,
gast_total,
total
) values (
".$idFam.",
'".$userFam."',
".$ciclo.",
".$gast_alm.",
".$gast_rent.",
".$gast_prest.",
".$gast_elect.",
".$gast_agua.",
".$gast_gas.",
".$gast_auto.",
".$gast_hipo.",
".$gast_pred.",
".$gast_tel.",
".$gast_cable.",
".$gast_int.",
".$gast_colg.",
".$gast_clas.",
".$gast_serv.",
".$gast_lib.",
".$gast_diver.",
".$gast_medi.",
".$gast_denti.",
".$gast_vest.",
".$gast_seg.",
".$gast_viaj.",
".$gast_otros.",
'".$des_otr_gas."',
".$gast_total.",
".$total."
)";
$resDeleteGastos=$pdo_estudios->query("delete from gastos where id_fam=".$idFam." ");

$resGastos=$pdo_estudios->query($sqlInsertGastos);
/*Otros datos*/
$cubreDeficit=$_REQUEST['cubreDeficit']; 
$prestamoDesglose=$_REQUEST['prestamoDesglose']; 
$tiempoLibre=$_REQUEST['tiempoLibre']; 
$frecuenciaDiversion=$_REQUEST['frecuenciaDiversion']; 
$lugarSalidas=$_REQUEST['lugarSalidas']; 
$frecuenciaVacaciones=$_REQUEST['frecuenciaVacaciones']; 
$lugarVacaciones=$_REQUEST['lugarVacaciones']; 
$ultimaVacion=$_REQUEST['ultimaVacion']; 
$obervacionesInters=$_REQUEST['obervacionesInters']; 
$conclusiones=$_REQUEST['conclusiones']; 
$situacionEconomica=$_REQUEST['situacionEconomica']; 
$sqlInsertOtrosDatos="insert into otrosDatos (
id_fam,
usuario,
ciclo,
cubreDeficit,
prestamoDesglose,
tiempoLibre,
frecuenciaDiversion,
lugarSalidas,
frecuenciaVacaciones,
lugarVacaciones,
ultimaVacion,
obervacionesInters,
conclusiones,
situacionEconomica
) values (
".$idFam.",
'".$userFam."',
".$ciclo.",
'".$cubreDeficit."',
'".$prestamoDesglose."',
'".$tiempoLibre."',
'".$frecuenciaDiversion."',
'".$lugarSalidas."',
'".$frecuenciaVacaciones."',
'".$lugarVacaciones."',
'".$ultimaVacion."',
'".$obervacionesInters."',
'".$conclusiones."',
'".$situacionEconomica."'
)";
$pdo_estudios->query("delete from otrosDatos where id_fam=".$idFam." ");
$pdo_estudios->query($sqlInsertOtrosDatos);
if($action=='Cerrar'){
	$resTermSol=$pdo_estudios->query("Select * from termSolicitud where id_fam=".$idFam." ");
	if($resTermSol){
		$updateTermSol="Update termSolicitud set fechaCierre=now(), cierre=1 where id_fam=".$idFam." ";
		$resUpdate=$pdo_estudios->query($updateTermSol);
		if($resUpdate){
					 	$insertBdBecasCierre="update estudios_familias set cierre=1, fechaCierre=now() where idFam=".$idFam." ";
		 	//echo $insertBdBecasCierre;
		 	$pdo->query($insertBdBecasCierre);

			return true;
		}else{
			return false;
		}
	}else{
		$inserTermSol="insert into termSolicitud (id_fam,usuario,fechaGuardar,fechaCierre,cierre,fechaMod,ciclo)
		 values(".$idFam.",'".$userFam."',now(),now(),1,now(),".$ciclo.") ";
		 $resInsertTermSol=$pdo_estudios->query($inserTermSol);
		 if($resInsertTermSol){
		 	$insertBdBecasCierre="update estudios_familias set cierre=1, fechaCierre=now() where idFam=".$idFam." ";
		 	//echo $insertBdBecasCierre;
		 	$pdo->query($insertBdBecasCierre);

		 	return true;
		 }else{
		 	return false;
		 }
	}
}else{
	$resTermSol=$pdo_estudios->query("Select * from termSolicitud where id_fam=".$idFam." ");
	if($resTermSol){
		$updateTermSol="Update termSolicitud set fechaMod=now(), cierre=0 where id_fam=".$idFam." ";
		$resUpdate=$pdo_estudios->query($updateTermSol);
		if($resUpdate){
			return true;
		}else{
			return false;
		}
	}else{

		$inserTermSol="insert into termSolicitud (id_fam,usuario,fechaGuardar,cierre,fechaMod,ciclo)
		 values(".$idFam.",'".$userFam."',now(),0,now(),".$ciclo.") ";
		 
		 $resInsertTermSol=$pdo_estudios->query($inserTermSol);
		 if($resInsertTermSol){
		 	return true;
		 }else{
		 	return false;
		 }
	}
}
}
?>
