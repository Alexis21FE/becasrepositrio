<?php 
session_start();
$_SESSION["var_ban"]=3;

if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Administrador") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}

$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}

$error_bak =0;
$band_total=0;
$error_carg=0;
$error_null=0;
$msg_null="";
extract($_POST);

set_time_limit ( 80 );

//si action tiene como valor UPLOAD haga algo (el value de este hidden es es UPLOAD iniciado desde el value
if ($action == "upload"){ 

	//cargamos el archivo al servidor con el mismo nombre(solo le agregue el sufijo bak_)
	$archivo = $_FILES['excel']['name']; //captura el nombre del archivo
	$tipo = $_FILES['excel']['type']; //captura el tipo de archivo (2003 o 2007)	
	$destino = "bak_".$archivo; //lugar donde se copiara el archivo
	//si dese copiar la variable excel (archivo).nombreTemporal a destino (bak_.archivo) (si se ha dejado copiar)
	
	if (copy($_FILES['excel']['tmp_name'],$destino)){ }else{
		$error_carg=1;
	}

		//validacion para saber si el archivo ya existe previamente
		if (file_exists ("bak_".$archivo)){ 
		/*INVOCACION DE CLASES Y CONEXION A BASE DE DATOS*/
		/** Invocacion de Clases necesarias */
		require_once('z_script/PHPExcel/Classes/PHPExcel.php');
		require_once('z_script/PHPExcel/Classes/PHPExcel/Reader/Excel2007.php');
		//DATOS DE CONEXION A LA BASE DE DATOS
		//$cn = mysql_connect ("localhost","root","") or die ("ERROR EN LA CONEXION");
		//$db = mysql_select_db ("colm_db",$cn) or die ("ERROR AL CONECTAR A LA BD");
		include("z_script/db_class.php");
		mysql_select_db($bd_becas,$link);
		
		
		// Cargando la hoja de calculo
		$objReader = new PHPExcel_Reader_Excel2007(); //instancio un objeto como PHPExcelReader(objeto de captura de datos de excel)
		$objPHPExcel = $objReader->load("bak_".$archivo); //carga en objphpExcel por medio de objReader,el nombre del archivo
		$objFecha = new PHPExcel_Shared_Date();
		
		// Asignar hoja de excel activa
		$objPHPExcel->setActiveSheetIndex(0); //objPHPExcel tomara la posicion de hoja (en esta caso 0 o 1) con el setActiveSheetIndex(numeroHoja)
		
		// Llenamos un arreglo con los datos del archivo xlsx
		$i=1; //celda inicial en la cual empezara a realizar el barrido de la grilla de excel
		$param=0;
		$contador=0;
		$contador_rep=0;
		$totalIngresados_rep=0;
		$totalIngresados=0;
		
		while($param==0){ //mientras el parametro siga en 0 (iniciado antes) que quiere decir que no ha encontrado un NULL entonces siga metiendo datos
			
			$id_fam			= $objPHPExcel->getActiveSheet()->getCell("A".$i)->getCalculatedValue();
			$alum_mat		= $objPHPExcel->getActiveSheet()->getCell("B".$i)->getCalculatedValue();
			$alum_name 		= $objPHPExcel->getActiveSheet()->getCell("C".$i)->getCalculatedValue();
			$alum_ap 		= $objPHPExcel->getActiveSheet()->getCell("D".$i)->getCalculatedValue();
			$alum_am 		= $objPHPExcel->getActiveSheet()->getCell("E".$i)->getCalculatedValue();
			$alum_sec		= $objPHPExcel->getActiveSheet()->getCell("F".$i)->getCalculatedValue();
			$alum_grd		= $objPHPExcel->getActiveSheet()->getCell("G".$i)->getCalculatedValue();
			$alum_gru		= $objPHPExcel->getActiveSheet()->getCell("H".$i)->getCalculatedValue();
			$alum_colg		= $objPHPExcel->getActiveSheet()->getCell("I".$i)->getCalculatedValue();
			$alum_porc		= $objPHPExcel->getActiveSheet()->getCell("J".$i)->getCalculatedValue();
			
			
			if($id_fam == NULL || $alum_mat  == NULL || $alum_name == NULL || $alum_ap == NULL || $alum_sec == NULL){ //pregunto que si ha encontrado un valor null en una columna inicie un parametro en 1 que indicaria el fin del ciclo while
				$param=1; //para detener el ciclo cuando haya encontrado un valor NULL
				$error_null=1;
				
				if($id_fam == NULL){
					$msg_null="Error al cargar la celda A".$i.". Formato Erroneo.";
				}else if($alum_mat == NULL){
					$msg_null="Error al cargar la celda B".$i.". Formato Erroneo.";
				}else if($alum_name == NULL){
					$msg_null="Error al cargar la celda C".$i.". Formato Erroneo.";
				}else if($alum_ap == NULL){
					$msg_null="Error al cargar la celda D".$i.". Formato Erroneo.";//si no
				}else if($alum_am == NULL){
					$msg_null="Error al cargar la celda E".$i.". Formato Erroneo.";
				}else if($alum_sec == NULL){
					$msg_null="Error al cargar la celda F".$i.". Formato Erroneo.";
				}else if($alum_grd == NULL){
					$msg_null="Error al cargar la celda G".$i.". Formato Erroneo.";
				}else if($alum_gru == NULL){
					$msg_null="Error al cargar la celda H".$i.". Formato Erroneo.";
				}else if($alum_colg == NULL){
					$msg_null="Error al cargar la celda I".$i.". Formato Erroneo.";
				}
	
			}else{

				$sql="SELECT * FROM `inf_alum` WHERE `alum_mat`='".$alum_mat."'"; 
				 $result=mysql_query($sql);
				 
				if ($result == 0 || !isset($result) || empty($result)) {   
				  $numRows = 0;  
				} else {
				  $numRows = mysql_num_rows($result); 
				}
				
				if($numRows == 1){
					
				$c="UPDATE `inf_alum` SET `alum_name`='".utf8_decode($alum_name)."',`alum_ap`='".utf8_decode($alum_ap)."',`alum_am`='".utf8_decode($alum_am)."',`alum_sec`='".$alum_sec."',`alum_grd`='".$alum_grd."', `alum_gru`='".$alum_gru."',`alum_colg`='".$alum_colg."',`alum_porc`='".$alum_porc."' WHERE `alum_mat`='".$alum_mat."'";	
					$r_c=mysql_query($c);
					
					$contador_rep=$contador_rep+1;
					$totalIngresados_rep=$contador_rep;	
					$celdas_rep[$contador_rep]=$i;	

					
				}else{
					
				$c="INSERT INTO `inf_alum`(`id_fam`, `alum_mat`, `alum_name`, `alum_ap`, `alum_am`, `alum_sec`, `alum_grd`,`alum_gru`, `alum_colg`, `alum_porc`) VALUES ('".$id_fam."','".$alum_mat."','".utf8_decode($alum_name)."','".utf8_decode($alum_ap)."','".utf8_decode($alum_am)."','".$alum_sec."','".$alum_grd."','".$alum_gru."','".$alum_colg."','".$alum_porc."')";
		$r_c=mysql_query($c);
		
				$contador=$contador+1;
				$totalIngresados=$contador;
				
				}
				
				$i++;
				
				$band_total=1;
				
				$asunto2=$objPHPExcel->getActiveSheet()->getCell("A".($i))->getCalculatedValue();
				
				if($asunto2 == NULL){
					$param=1;
					}
				
			}
	
		}
			
			unlink($destino); //desenlazar a destino el lugar donde salen los datos(archivo)
		}
	
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php
            include_once("z_script/header.php");
        ?>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
		<div class="container_12">
            
            <div style="clear:both;"></div>
            
            
            
            <div class="grid_12">
                
                <!-- Notification boxes -->
                <?php 
    				if($error_null == 1)
					{
				?>
						<!--  start message-red -->
						<span class="notification n-error"><?php echo $msg_null; ?></span>
						<!--  end message-red -->
				<?php 
					}
			
					if($error_carg == 1)
					{
				?>
						<span class="notification n-error">Error al cargar el archivo. Por favor, de revisar bien su archivo.</span>
				<?php 
					}
			
					if($error_bak == 1)
					{
				?>
					<!--  start message-red -->
						<span class="notification n-error">Necesitas primero importar el archivo.</span>
					<!--  end message-red -->
				<?php 
					}
					
					if($band_total == 1)
					{
				?> 
						<?php if($totalIngresados != 0 || $totalIngresados != NULL){ ?>
						 	<span class="notification n-success">Archivo cargado con exito. Total: <?php echo $totalIngresados; ?> usuarios.</span>
						<?php }?>	
                        	
                      	<?php if($totalIngresados_rep != 0 || $totalIngresados_rep != NULL){ ?>   
                          <span class="notification n-attention">Usuarios modificados con exito. Total: <?php echo $totalIngresados_rep; ?> usuarios. Celdas repetidas: <?php for($r=1; $r<=$totalIngresados_rep; $r++){ echo " ".$celdas_rep[$r];}?></span>
						<?php }?>
                            
                            <span class="notification n-success">Total de celdas encontradas en el archivo : <?php echo $totalIngresados + $totalIngresados_rep; ?> celdas .</span>
                            
				<?php 
					}
				?>
    
                <div class="module">
                	<h2><span>Importación de los Alumnos</span></h2>
                    	<div class="module-body">
                        <h4>La importación solo aceptará los siguientes valores en el archivo Excel: Id Familia, Matrícula del Alumno, Nombre, Apellido Paterno, Apellido Materno, Sección a ingresar, Grado a ingresar,	Grupo a ingresar y Colegio a ingresar.</h4>                  	 
                      <h4>Instrucciones de preparación de archivo para la importación de datos, haga click <a href="al_guia.php" style="color:#00C;" target="_blank">aquí</a>.<br />
                      </h4>
                          <hred>
                            <h4>Nota:  Antes de adjuntar su archivo de Excel, por favor lea la Guía de importación.</h4>
                          </hred>
                          <p>&nbsp;</p>
                          <h4>Seleccione el archivo a importar:                        </h4>
                        <form name="importa" method="post" action="import_al.php" enctype="multipart/form-data" >
                            <input name="excel" type="file" size="30" /><br />
                            <br />
                            <fieldset>
                            <input class="submit-green" style="width:100px; height:30px"  name="enviar" id="enviar" type='submit'  value="Importar" />
                            </fieldset>  
                            <input type="hidden" value="upload" name="action" />
                        </form>
                    	<div style="clear: both"></div>
                     </div> <!-- End .module-body -->

            </div> <!-- End .module --></div> <!-- End .grid_12 -->
                
            <!-- Categories list --><!-- End .grid_6 -->
            
            <!-- To-do list --><!-- End .grid_6 -->
          <div style="clear:both;"></div>
            
            <!-- Form elements --><!-- End .grid_12 -->
                
            <!-- Settings-->
            <div class="grid_6">
                 <!-- End .module -->
            </div> <!-- End .grid_6 -->
                
            <!-- Password --><!-- End .grid_6 -->
          <div style="clear:both;"></div><!-- End .grid_3 --><!-- End .grid_3 --><!-- End .grid_6 -->

            
          <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
        <?php include_once("z_script/footer.php") ?>
	</body>
</html>