<?php 
session_start();
$_SESSION["var_ban"]=3;

if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Administrador") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}
	
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}

$band_error_msg=(isset ($_GET["red_msg"]) ? $_GET["red_msg"]: "");
$band_modf_msg=(isset ($_GET["modf_msg"]) ? $_GET["modf_msg"]: "");	
extract($_POST, EXTR_PREFIX_ALL, "x");
include("z_script/db_class.php");

//paginador
if(isset($_GET['page'])) //verifica pagina
{
	$page= $_GET['page'];
}else{
	$page=1;
}

	if(isset($x_busc)){
		
		if($x_tip_bus == NULL && $x_input_bus == NULL){
			
			$consulta="SELECT * FROM inf_familia";
		} else if($x_tip_bus == "name"){
				 $consulta="SELECT * FROM `inf_familia` where concat_ws(' ',`ft_name`,`ft_ap`,`ft_mat`) LIKE '%".utf8_decode($x_input_bus)."%'";
				
		}else if($x_tip_bus == "fam"){
                 
				$consulta="SELECT * FROM inf_familia where usuario LIKE'%".$x_input_bus."%'"; 
			
		}else if($x_tip_bus == "nombre_fam"){
				$consulta="SELECT * FROM inf_familia where concat(ft_ap,' ',mt_ap) LIKE'%".$x_input_bus."%'"; 
			
		}else if($x_tip_bus == "tel"){
				$consulta="SELECT * FROM inf_familia where ft_name LIKE'%".$x_input_bus."%'"; 
			
		}else if($x_tip_bus == "cel"){
				$consulta="SELECT * FROM inf_familia where mt_name LIKE'%".$x_input_bus."%'"; 
			
		}else if($x_tip_bus == "email"){
			
			$consulta="SELECT * FROM inf_familia where ft_email LIKE'%".$x_input_bus."%'"; 
			
		}
		
	}else if(isset($x_resg_busc)){
		
		$consulta="SELECT * FROM inf_familia";
		
	}else{
	
		
		$consulta="SELECT * FROM inf_familia";
		
	}
       $consulta.=" order by ft_ap asc";
if($consulta!='')
{
$datos=$pdo->query($consulta);
$num_rows=$datos->rowCount();
}
$rows_per_page= 100;
$lastpage= ceil($num_rows / $rows_per_page);
$page=(int)$page;
$band_del=0;
if($page > $lastpage)
{
	$page= $lastpage;
}

if($page < 1)
{
	$page=1;
}

$limit= 'LIMIT '. ($page -1) * $rows_per_page . ',' .$rows_per_page;
$consulta .=" $limit";
$peliculas=$pdo->query($consulta);

////

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php
            include_once("z_script/header.php");
        ?>
        <script type="text/javascript">
			function opcion(id,name, usuario)
			{
				var answer = confirm("Esta seguro de querer eliminar la familia: "+ name)
			 
				if (answer)
				{
					window.location="actions/ft_del.php?id="+ id+"&usuario="+ usuario;
					 
				}else { 
				 return false; 
				}
				
			}
		</script>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
		<div class="container_12">

                    <center><label>                        
                            Información Padres de Familia</label></center>
            <hr>
            
            
            <div class="grid_12">
                <!-- Notification boxes -->
                <?php
					if($band_error_msg=="display"){
						
					?>
                    <span class="notification n-success"> Usuario <?php echo $_SESSION["del_ft_name"];?> ha sido eliminado exitosamente.</span>
                    <?php
						unset($_SESSION["del_ft_name"]);
					}
					?>	
                    
                    <?php
					if($band_modf_msg=="display_mod"){
						
					?>
                    <span class="notification n-success"> Usuario <?php echo $_SESSION["mod_ft_name"];?> ha sido modificado exitosamente.</span>
                    <?php
					unset($_SESSION["mod_ft_name"]);
					}
					?>
                
              <div class="bottom-spacing">
                <div class="float-left">
					<form action="inf_ft.php" method="post" enctype="multipart/form-data">
                        <div class="row">
                     
                          	<div class="col">
                            	<select class="form-control" id="tip_bus" name="tip_bus">
                                    <option value="" selected="selected">Selecciona..</option>
                                    <option value="fam">Familia</option>
                                    <option value="nombre_fam">Nombre Familia</option>
                                    <option value="name_p">Nombre padre</option>
                                    <option value="name_m">Nombre madre</option>
                              	</select>
                            </div>
                            <div class="col"><input class="form-control" name="input_bus" id="input_bus" type="text" /></div>
                            <div class="col">
                            <button class="btn btn-success" name="busc" id="busc" type='submit'>Buscar</button>
                            </div>
                            <div class="col">
                            <?php if(isset($x_busc) && $x_tip_bus != NULL && $x_input_bus != NULL){ ?>
                         	 
                           <button class="btn btn-success" name="resg_busc" id="resg_busc" type='submit'   >Regresar</button>
                             
							<?php }?>
                            </div>
                         
                        </div>
					</form>
                  		
                		
                </div>
                  
                <div class="float-right">
                <div class="row">
                          
                          	<div class="col">
                            	<a href="import_ft.php">
                                 
                                    <button class="btn btn-success"  name="enviar" id="enviar" type='submit'  >Importar</button>
                               
                              	</a>
                            </div>
                            <div class="col"><a target="_blank" href="actions/ft_ex.php">
                     
                            <button class="btn btn-success"  name="enviar" id="enviar" type='button'  >Exportar</button>
                     
                  </a></div>
                           
                        </div>
           		  
                           
                </div>
              </div>
                
                <!-- Example table -->
                <div class="module">
                	
                    
                    <div class="module-table-body" style="margin-top: 2%;">
                    	<form action="">
                        <table id="myTable" class="tablesorter table">
                            <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Familia</th>
                                    <th>Nombre Familia</th>
                                    <th >Nombre Padre</th>
                                    <th >Celular Padre</th>
                                    <th >Correo electrónico Padre</th>
                                    <th >Nombre Madre</th>
                                    <th >Celular Madre</th>
                                    <th >Correo electrónico Madre</th>
                                    <th >Teléfono</th>
                                    <th>Opciones</th>
                                </tr>
                        </thead>
                            <?php
								if($page == 1){
								
								$band_id=1;
								
								}else{
								
								$band_id=($page * $rows_per_page) - ($rows_per_page) + 1;
								
								}
							if($peliculas!='')
							{
								foreach ($peliculas as $row){
								$x_id_fam=$row["id_fam"];	
								$x_ft_name=$row["ft_name"];
								$x_ft_ap=$row["ft_ap"];
								$x_ft_mat=$row["ft_mat"];
								$x_ft_tel=$row["ft_tel"];
								$x_ft_cel=$row["ft_cel"];
								$x_ft_email=$row["ft_email"];
																	
								$x_mt_name=$row["mt_name"];
								$x_mt_ap=$row["mt_ap"];
								$x_mt_mat=$row["mt_mat"];
								$x_mt_cel=$row["mt_cel"];
								$x_mt_email=$row["mt_email"];
								
								$usuario=$row["usuario"];
								
								if($x_ft_cel == NULL || $x_ft_cel == 0){ $x_ft_cel="";}
								if($x_ft_tel == NULL || $x_ft_tel == 0){ $x_ft_tel="";}
								if($x_ft_email == NULL){ $x_ft_email="";}
								
								if($x_mt_cel == NULL || $x_mt_cel == 0){ $x_mt_cel="";}
								if($x_mt_email == NULL){ $x_mt_email="";}
								
								$name_del_ft=$x_ft_name." ".$x_ft_ap." ".$x_ft_mat;
								$name_del_mt=$x_mt_name." ".$x_mt_ap." ".$x_mt_mat;
								
								?>
                            <tbody>
                                <tr>
             <td class="align-center"><?php echo $band_id;?></td>
             <td class="align-center"><?php echo $usuario;?></td>
             <td class="align-center"><?php echo $x_ft_ap." ".$x_mt_ap;?></td>
             <td><?php echo $x_ft_name." ".$x_ft_ap." ".$x_ft_mat;?></td>             
             <td><?php echo $x_ft_cel;?></td>
             <td><?php echo $x_ft_email;?></td>
			 
			 <td><?php echo $x_mt_name." ".$x_mt_ap." ".$x_mt_mat;?></td>             
             <td><?php echo $x_mt_cel;?></td>
             <td><?php echo $x_mt_email;?></td>
			 
			 <td><?php echo $x_ft_tel;?></td>
             <td>
                                    
<a href="ft_perfil.php?id=
<?php echo $x_id_fam;?>&usuario=
<?php echo $usuario;?>">
<i class="far fa-user"></i></a>
<a href="ft_update.php?id=
<?php echo $x_id_fam;?>&usuario=
<?php echo $usuario;?>">
    <i class="far fa-edit"></i></a>

<a onclick="opcion(<?php echo $x_id_fam." , '".$name_del_ft."', '".$usuario."'";?>);" ><i class="fas fa-user-minus"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                          <?php $band_id++; }  ?>
                        </table>
                        </form>
                    	<div style="clear: both"></div>
                  </div> <!-- End .module-table-body -->
                </div> <!-- End .module -->
                <div style="float:left;">
                Ver Perfil: <img src="images/user.gif" width="16" height="16" alt="Perfil" /> &nbsp; &nbsp; Modificar: <img src="images/bin.gif" width="16" height="16" alt="Modificar" /> &nbsp; &nbsp; Eliminar: <img src="images/minus-circle.gif" width="16" height="16" alt="delete" />
                </div>
               
               <div class="pagination">           
                		 <?php
							//UNA VEZ Q MUESTRO LOS DATOS TENGO Q MOSTRAR EL BLOQUE DE PAGINACIÓN SIEMPRE Y CUANDO HAYA MÁS DE UNA PÁGINA
							if($num_rows != 0)
							{
							   $nextpage= $page +1;
							   $prevpage= $page -1;
						?>
						<?php
						//SI ES LA PRIMERA PÁGINA DESHABILITO EL BOTON DE PREVIOUS, MUESTRO EL 1 COMO ACTIVO Y MUESTRO EL RESTO DE PÁGINAS
								  if($page == 1) 
								   {
									?>
                       <!-- <a href="" class="button"><span><img src="images/arrow-180-small.gif"  height="9" width="12" alt="Previous" /> Anterior</span></a>-->
                        <div class="numbers">
                            <span>Paginas:</span> 
                            <span class="current">1</span> 
                            <span>|</span>
                            
                            <?php
							  for($i= $page+1; $i<= $lastpage ; $i++)
							  {
							?> 
                            	<a href="inf_ft.php?page=<?php echo $i;?>"><?php echo $i;?></a> 
                            	<span>|</span>
                            <?php 
							  }
							?>
                            
                            <?php
                             //Y SI LA ULTIMA PÁGINA ES MAYOR QUE LA ACTUAL MUESTRO EL BOTON NEXT O LO DESHABILITO
										if($lastpage >$page )
										{
									?>    
                               </div>        
                       	  <a href="inf_ft.php?page=<?php echo $nextpage;?>" class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> 
                         <?php
										}else{
									?>
                       			</div>
                                   		<!--	 <a class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> -->
                        <?php
										}
									} else{
									//EN CAMBIO SI NO ESTAMOS EN LA PÁGINA UNO HABILITO EL BOTON DE PREVIUS Y MUESTRO LAS DEMÁS
								?>
                       <a href="inf_ft.php?page=<?php echo $prevpage;?>" class="button"><span><img src="images/arrow-180-small.gif"  height="9" width="12" alt="Previous" /> Anterior</span></a>
                      <div class="numbers"> 
                      <span>Paginas:</span>
                      
                        <?php
										 for($i= 1; $i<= $lastpage ; $i++)
										 {
													   //COMPRUEBO SI ES LA PÁGINA ACTIVA O NO
											if($page == $i)
											{
								?>     
                                  
                            
                                <span class="current"><a><?php echo $i;?></a></span><span>|</span>
                                
                                <?php
											}
											else
											{
								?>
                                
                                    
                                <a href="inf_ft.php?page=<?php echo $i;?>"><?php echo $i;?></a>
                                <span>|</span>
                                
                                <?php
											}
										}
								?>
                                
                                <?php		
										 //Y SI NO ES LA ÚLTIMA PÁGINA ACTIVO EL BOTON NEXT     
										if($lastpage >$page )
										{   
								?>   
                            </div>   
                       <a href="inf_ft.php?page=<?php echo $nextpage;?>" class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> 
                      
                                <?php
										}
									 else
										{
								?>  
                                   </div> 
                                 <!--  <a class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> -->
                                   
								   <?php
										}
								}     
							  }
							  
							  
							 }
							?>
                            
                       <div style="clear: both;"></div> 
                     </div>
                

                
			</div> <!-- End .grid_12 -->

          <div style="clear:both;"></div>

        </div> <!-- End .container_12 -->
		
         <?php include_once("z_script/footer.php") ?>
	</body>
</html>