<?

$ade_minimo=100; 

function fMesRel($aiMes)
{
  	include('connection.php');
  	$linkFMesRel = mysql_connect($server, $userName, $password);
	mySql_select_db($DB)or die("No se pudo seleccionar DB");
	if (!$linkFMesRel) die('No se pudo conectar: ' . mysql_error());
	
	// LEE DATOS DE PAR�METROS
	$sSqlQ = "select mes_relativo from mes_relativo where mes = '$aiMes'";
	$resultP=mysql_query($sSqlQ,$linkFMesRel)or die(mysql_error());
	$rowP = mysql_fetch_array($resultP);
	$iMesRelativo=$rowP["mes_relativo"];
 //	mysql_close($linkFMesRel);
  
	return $iMesRelativo;
}


function fCalculaRecargos ($adFecha, $aiConcepto, $adImporte, $aiPeriodo, $aiMesPago, $alAlumno, $asBeca, $aiPeriodoPago ) {
  	include('connection.php');
  	$link = mysql_connect($server, $userName, $password);
	mySql_select_db($DB)or die("No se pudo seleccionar DB");
	if (!$link) die('No se pudo conectar: ' . mysql_error());
	// LEE DATOS DE PAR�METROS
	$sSqlQ = "select mes_inicial_periodo_actual, recargos_sab_dom, factor_redondeo from parametros";
	$resultP=mysql_query($sSqlQ,$link)or die(mysql_error());
	$rowP = mysql_fetch_array($resultP);
	$iMesInicialPeriodoActual=$rowP["mes_inicial_periodo_actual"];
	$cRecargosSabDom=$rowP["recargos_sab_dom"];
	$dFactorRedondeo=$rowP["factor_redondeo"];
	// LEE DATOS DE CONCEPTOS
	$sSqlQ = "select * from conceptos where concepto = ' $aiConcepto ' and ciclo = ' $aiPeriodo '";
	$resultP=mysql_query($sSqlQ,$link)or die(mysql_error());
	$rowP = mysql_fetch_array($resultP);
	$iDiaRecargo1=$rowP["dia_recargo_1"];
	$iDiaRecargo2=$rowP["dia_recargo_2"];
	$iDiaRecargo3=$rowP["dia_recargo_3"];
	$cCantOPorc1=$rowP["cant_o_porc_1"];
	$cCantOPorc2=$rowP["cant_o_porc_2"];
	$cCantOPorc3=$rowP["cant_o_porc_3"];
	$dRecargo1=$rowP["recargo_1"];
	$dRecargo2=$rowP["recargo_2"];
	$dRecargo3=$rowP["recargo_3"];
	$cMesCompleto=$rowP["mes_completo"];
	$cCantOPorcMes=$rowP["cant_o_porc_mes"];
	$dRecargoMes=$rowP["recargo_mes"];
	$cAcumularMes=$rowP["acumular_mes"];
	$cInscripColegOtros=$rowP["inscrip_coleg_otros"];
	// CHECA SI EL ALUMNO TIENE VENCIMIENTO ESPECIAL O NO APLICA RECARGOS
	if ($cInscripColegOtros=='C'){
		$sSqlQ = "select dia_vencimiento_colegiaturas, aplica_recargos from alumnos where alumno = ' $alAlumno '";
		$resultP=mysql_query($sSqlQ,$link)or die(mysql_error());
		$rowP = mysql_fetch_array($resultP);
		$iDiaVencimientoColegiaturas=$rowP["dia_vencimiento_colegiaturas"];
		$cAplicaRecargos=$rowP["aplica_recargos"];
		if ($iDiaVencimientoColegiaturas > 0) $iDiaRecargo1 = $iDiaVencimientoColegiaturas;
		if ($cAplicaRecargos=='N') return 0;
	} 
	
		
 //	mysql_close($link);

	// SI EL IMPORTE ES 0 -> NO CALCULA RECARGOS
	if ($adImporte == 0) return 0;
	// CALCULA PERIODO DE LA FECHA
	$iPeriodoActual = $adFecha["year"];

	if ($adFecha["mon"] < $iMesInicialPeriodoActual) $iPeriodoActual = $iPeriodoActual - 1;
	// CALCULA A�O DE VENCIMIENTO DEL CARGO (PARA EFECTOS DE NO COBRO DE 
	//		RECARGOS EN S�BADO O DOMINGO)
	$iAnoVencimiento = $aiPeriodo;
	if ($aiMesPago < $iMesInicialPeriodoActual)	$iAnoVencimiento = $iAnoVencimiento + 1;

	// SI NO SE CALCULAN RECARGOS A PARTIR DE S�BADO O DOMINGO -> AJUSTA D�AS DE INICIO EN CASO NECESARIO
	if ($cRecargosSabDom=='N'){
		if ($iDiaRecargo1 > 0){
			$dFechaInicioRecargos = getDate(mktime(0,0,0,$aiMesPago,$iDiaRecargo1 - 1,$iAnoVencimiento));
			$iDiaSemana = $dFechaInicioRecargos["wday"];
			while ( $iDiaSemana == 0 OR $iDiaSemana == 6) {
				$iDiaRecargo1 = $iDiaRecargo1  + 1;
				$dFechaInicioRecargos = getDate(mktime(0,0,0,$aiMesPago,$iDiaRecargo1 - 1,$iAnoVencimiento));
				$iDiaSemana = $dFechaInicioRecargos["wday"];
			}
		}
		if ($iDiaRecargo2 > 0){
			$dFechaInicioRecargos = getDate(mktime(0,0,0,$aiMesPago,$iDiaRecargo2 - 1,$iAnoVencimiento));
			$iDiaSemana = $dFechaInicioRecargos["wday"];
			while ( $iDiaSemana == 0 OR $iDiaSemana == 6) {
				$iDiaRecargo2 = $iDiaRecargo2  + 1;
				$dFechaInicioRecargos = getDate(mktime(0,0,0,$aiMesPago,$iDiaRecargo2 - 1,$iAnoVencimiento));
				$iDiaSemana = $dFechaInicioRecargos["wday"];
			}
		}
		if ($iDiaRecargo3 > 0){
			$dFechaInicioRecargos = getDate(mktime(0,0,0,$aiMesPago,$iDiaRecargo3 - 1,$iAnoVencimiento));
			$iDiaSemana = $dFechaInicioRecargos["wday"];
			while ( $iDiaSemana == 0 OR $iDiaSemana == 6) {
				$iDiaRecargo3 = $iDiaRecargo3  + 1;
				$dFechaInicioRecargos = getDate(mktime(0,0,0,$aiMesPago,$iDiaRecargo3 - 1,$iAnoVencimiento));
				$iDiaSemana = $dFechaInicioRecargos["wday"];
			}
		}
	}
	//	SI SE TRATA DE PROXIMO PERIODO O EL MES ES POSTERIOR O ES EL MISMO MES Y
	//	(NO HAY RECARGOS POR DIA O TODAVIA NO ES EL DIA DE RECARGOS O EL ALUMNO
	//	 CUENTA CON BECA ESPECIAL (NO RECARGOS EN EL MES))
	IF ($aiPeriodoPago >= $iPeriodoActual){
		IF (fMesRel($aiMesPago) > fMesRel($adFecha["mon"]) OR $aiMesPago == $adFecha["mon"] AND ($iDiaRecargo1 == 0 OR $iDiaRecargo1 > $adFecha["mday"] ) OR $aiPeriodo > $iPeriodoActual) RETURN 0;
		}

	// ls_cant_o_porc:  'P' = Porcentaje fijo
	//						  'C' = Cantidad Fija
	//						  'D' = Porcentaje por D�a aplicando desde el d�a primero para los atrasados
	//						  'E' = (Especial), Cantidad por D�a
	//						  'F' = Porcentaje por D�a aplicando desde dia_recargo_1 para los atrasados
	// CALCULA RECARGOS DEL MISMO MES
	
	$iDiaHoy = $adFecha["mday"];
	// SI EL MES ES MENOR AL DE HOY (Y NO ES RECARGOS POR D�A), ENTONCES li_dia_hoy SE 
	//		CONVIERTE EN EL �LTIMO DEL MES PARA QUE LE COBRE TODOS LOS RECARGOS DEL MES INICIAL
	IF ($aiMesPago < $adFecha["mon"] AND $aiPeriodo <= $iPeriodoActual AND ($cCantOPorc1 == 'P' OR $cCantOPorc1 == 'C')){ ///////
	  	$dFechaFinal = fFechaFinalDelMes( $adFecha["mon"], $adFecha["year"] );
		$iDiaHoy = $dFechaFinal["mday"];
	}
	// RANGO 1 DE RECARGOS O 'D' O 'E'
	IF ($iDiaHoy >= $iDiaRecargo1 AND ($iDiaHoy < $iDiaRecargo2 OR $iDiaRecargo2 == 0) OR ($aiMesPago <> $adFecha["mon"] OR $aiPeriodoPago < $iPeriodoActual)){
		IF ($cCantOPorc1 == 'C'){ //////////
			// CHECA LA SUMA DE LOS RECARgOS DE LOS ABONOS PREVIOS
			$iAbonosPrevios = 0;
			$dRecargosPrevios = 0;
			$dRecargosTotales = 0;
			$sSqlQ = "select count(*) as abonos_previos, sum(recargos) as recargoa_previos from abonos where alumno = ' $alAlumno ' and periodo = ' $aiPeriodo ' and concepto = ' $aiConcepto ' and mes = ' $aiMesPago '";
			$resultP=mysql_query($sSqlQ,$link)or die(mysql_error());
			$rowP = mysql_fetch_array($resultP);
			$iAbonosPrevios=$rowP["abonos_previos"];
			$dRecargosPrevios=$rowP["recargoa_previos"];
			//	si no tiene abonos previos o tiene recargos_previos calcula recargo
			IF ($iAbonosPrevios == 0 OR $dRecargosPrevios > 0) $dRecargosTotales = $dRecargo1 - $dRecargosPrevios;
			return $dRecargo1;
		}
		ELSEIF ($cCantOPorc1 == 'P')
		{
		   $dRecargosTotales = fredondeo($adImporte * $dRecargo1 / 100,$dFactorRedondeo);
		}
		ELSE
		{
			//  importe * % reca     *   dias transcurridos   / (dias del mes - 10)
			$iAnoCargo = $aiPeriodoPago;
			IF ($aiMesPago < $iMesInicialPeriodoActual) $iAnoCargo += 1;
			// LA LINEA EN COMENTARIOS ES TIPO PEREYRA (RECARGOS A PARTIR DEL DIA 10)
			// TIPO PEREYRA QUED� COMO 'F' IGUAL QUE AMERICAN SCHOOL
			// F Y G SON A PARTIR DEL VENCIMIENTO
			IF ($cCantOPorc1 == 'F' OR $cCantOPorc1 == 'G') 
			{
				// OJO: DIA_RECARGO_1 = 31 SIGNIFICA RECARGOS A PARTIR DEL D�A 1ERO DEL SIG. MES
				IF ($iDiaRecargo1 == 31)
				{
					$aiMesPago ++;
					IF ($aiMesPago > 12)
					{
						$aiMesPago = 1;
						$iAnoCargo = $iAnoCargo + 1;
					}
					$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,1,$iAnoCargo)), $adFecha) + 1;
				}
				ELSE
					$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,$iDiaRecargo1,$iAnoCargo)), $adFecha) + 1;
			}
			ELSE
			{
				IF ($iDiaRecargo1 == 31)
				{
					$aiMesPago ++;
					IF ($aiMesPago > 12)
					{
					 	$aiMesPago = 1;
						$iAnoCargo = $iAnoCargo + 1;
					}
					$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,1,$iAnoCargo)), $adFecha) + 1;
				}
				ELSE
					$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,1,$iAnoCargo)), $adFecha) + 1;
			}
			// D Y F= DIAS TRANSCURRIDOS * RECARGO%
				// IMPORTE * % RECA * DIAS TRANSCURRIDOS DESDE EL $iDiaRecargo1 DEL MES DEL CARGO
				// AL DIA DE HOY
			IF ($cCantOPorc1 == 'D' OR $cCantOPorc1 == 'F')
			{
//			  return $dRecargo1;
				$dRecargosTotales = fRedondeo(floor($adImporte * $dRecargo1 * $iDias ) / 100, $dFactorRedondeo);
			}
			// E Y G = DIAS TRANSCURRIDOS * RECARGO$
				//$ RECA * DIAS TRANSCURRIDOS DESDE EL...
			ELSE
				$dRecargosTotales = fRedondeo($dRecargo1 * $iDias, $dFactorRedondeo);
		}
	}
	// RANGO 2
ELSE
{
	IF ($iDiaHoy >= $iDiaRecargo2 AND ($iDiaHoy < $iDiaRecargo3 OR $iDiaRecargo3 == 0))
 {
	IF ($cCantOPorc2 == 'C')
	{
		// CHECA LA SUMA DE LOS RECARgOS DE LOS ABONOS PREVIOS
		$iAbonosPrevios = 0;
			$dRecargosPrevios = 0;
			$dRecargosTotales = 0;
			$sSqlQ = "select count(*) as abonos_previos, sum(recargos) as recargoa_previos from abonos where alumno = ' $alAlumno ' and periodo = ' $aiPeriodo ' and concepto = ' $aiConcepto ' and mes = ' $aiMesPago '";
			$resultP=mysql_query($sSqlQ,$link)or die(mysql_error());
			$rowP = mysql_fetch_array($resultP);
			$iAbonosPrevios=$rowP["abonos_previos"];
			$dRecargosPrevios=$rowP["recargoa_previos"];
			//	si no tiene abonos previos o tiene recargos_previos calcula recargo
			IF ($iAbonosPrevios == 0 OR $dRecargosPrevios > 0) $dRecargosTotales = $dRecargo2 - $dRecargosPrevios;
			return $dRecargo2;
			
	}
	ELSE
	{
		IF ($cCantOPorc2 == 'P')
		{
			$dRecargosTotales = fredondeo($adImporte * $dRecargo2 / 100,$dFactorRedondeo);    ///////////////////////////////////
		}
		ELSE
		{
			//  importe * % reca     *   dias transcurridos   / (dias del mes - 10)
			$iAnoCargo = $aiPeriodoPago;
			IF ($aiMesPago < $iMesInicialPeriodoActual) $iAnoCargo += 1;
		
		// LA LINEA EN COMENTARIOS ES TIPO PEREYRA (RECARGOS A PARTIR DEL DIA 10)
		// TIPO PEREYRA QUED� COMO 'F' IGUAL QUE AMERICAN SCHOOL
		// F Y G SON A PARTIR DEL VENCIMIENTO
		}
		IF ($cCantOPorc2 == 'F' OR $cCantOPorc2 == 'G' OR $cCantOPorc2 == 'S')
		{
		// OJO: DIA_RECARGO_2 = 31 SIGNIFICA RECARGOS A PARTIR DEL D�A 1ERO DEL SIG. MES
			IF ($iDiaRecargo2 == 31)
			{
				$aiMesPago ++;
				IF ($aiMesPago > 12 ){
					$aiMesPago = 1;
					$iAnoCargo = $iAnoCargo + 1;
				}
				$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,1,$iAnoCargo)), $adFecha) + 1;
			}
			ELSE
			{			
				$iDiaRecargo2original = $iDiaRecargo2;			
				IF ($cRecargosSabDom == 'V')
						$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,$iDiaRecargo2original,$iAnoCargo)), $adFecha) + 1;									
				ELSE
					$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,$iDiaRecargo2,$iAnoCargo)), $adFecha) + 1;	
			}																		
		}
		ELSE
		{
			IF ($iDiaRecargo2 == 31)
			{
				$aiMesPago ++;
				IF ($aiMesPago > 12)
				{
					$aiMesPago = 1;
					$iAnoCargo = $iAnoCargo + 1;
				}
				$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,1,$iAnoCargo)), $adFecha) + 1;
			}
			ELSE
			{
				$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,1,$iAnoCargo)), $adFecha) + 1;
			}
		}
		// D Y F= DIAS TRANSCURRIDOS * RECARGO%
		IF ($cCantOPorc2 == 'D' OR $cCantOPorc2 == 'F')
		{
			// IMPORTE * % RECA * DIAS TRANSCURRIDOS DESDE EL li_dia_recargo_2 DEL MES DEL CARGO
			// AL DIA DE HOY
			// OJO: REDONDEA PRIMERO LOS RECARGOS POR D�A PORQUE AS� SE HACE EN LOS BANCOS.
			$dRecargosTotales = fRedondeo(floor($adImporte * $dRecargo2 ) / 100 * $iDias, $dFactorRedondeo);
		// E Y G = DIAS TRANSCURRIDOS * RECARGO$
		}
		ELSE
		{
			IF ($cCantOPorc2 == 'E' OR $cCantOPorc2 == 'G')
			{
			//$ RECA * DIAS TRANSCURRIDOS DESDE EL...
			$dRecargosTotales = fRedondeo($dRecargo2 * $iDias, $dFactorRedondeo);			
			}
			ELSE
			{
			//$ RECA * SEMANAS TRANSCURRIDAS DESDE EL...
			$dRecargosTotales = fRedondeo($dRecargo2 * ($iDias+6)/7, $dFactorRedondeo);			
			}
	 	}
// RANGO 3
}}

ELSE
{
	IF ($cCantOPorc3 == 'C')
	{
		// CHECA LA SUMA DE LOS RECARgOS DE LOS ABONOS PREVIOS
		$iAbonosPrevios = 0;
		$dRecargosPrevios = 0;
		$dRecargosTotales = 0;
		$sSqlQ = "select count(*) as abonos_previos, sum(recargos) as recargoa_previos from abonos where alumno = ' $alAlumno ' and periodo = ' $aiPeriodo ' and concepto = ' $aiConcepto ' and mes = ' $aiMesPago '";
			$resultP=mysql_query($sSqlQ,$link)or die(mysql_error());
			$rowP = mysql_fetch_array($resultP);
			$iAbonosPrevios=$rowP["abonos_previos"];
			$dRecargosPrevios=$rowP["recargoa_previos"];
			//	si no tiene abonos previos o tiene recargos_previos calcula recargo
			IF ($iAbonosPrevios == 0 OR $dRecargosPrevios > 0) 
			$dRecargosTotales = $dRecargo3 - $dRecargosPrevios;
	}
	ELSE
	{
		IF ($cCantOPorc3 == 'P')
			$dRecargosTotales = fRedondeo(adImporte + $dRecargo3/100 , $dFactorRedondeo);					
		ELSE
		{
			//  importe * % reca     *   dias transcurridos   / (dias del mes - 10)
			$iAnoCargo = $aiPeriodoPago;
			IF ($aiMesPago < $iMesInicialPeriodoActual)
			{
				$iAnoCargo += 1;
			}
		// LA LINEA EN COMENTARIOS ES TIPO PEREYRA (RECARGOS A PARTIR DEL DIA 10)
		// TIPO PEREYRA QUED� COMO 'F' IGUAL QUE AMERICAN SCHOOL
		// F Y G SON A PARTIR DEL VENCIMIENTO
			IF ($cCantOPorc3 == 'F' OR $cCantOPorc3 == 'G' OR $cCantOPorc3 == 'S')
			{
			// OJO: DIA_RECARGO_3 = 31 SIGNIFICA RECARGOS A PARTIR DEL D�A 1ERO DEL SIG. MES
				IF ($dRecargo3 == 31)
				{
					$aiMesPago ++;
					IF ($aiMesPago > 12)
					{
						$aiMesPago = 1;
						$iAnoCargo = $iAnoCargo + 1;
					}
					$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,$iAnoCargo)), $adFecha) + 1;
				}ELSE
				{
					$iDiaRecargo3original = $iDiaRecargo3;			
					IF ($cRecargosSabDom == 'V')
						$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,$iDiaRecargo3original,$iAnoCargo)), $adFecha) + 1;									
					ELSE
						$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,$iDiaRecargo3,$iAnoCargo)), $adFecha) + 1;				
				}
			}ELSE
			{
				IF ($iDiaRecargo3 == 31)
				{
					$aiMesPago ++;
					IF ($aiMesPago > 12)
					{
						$aiMesPago = 1;
						$iAnoCargo = $iAnoCargo + 1;
					}
					$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,1,$iAnoCargo)), $adFecha) + 1;
				}ELSE
				{
					$iDias = fDiasEntre(getdate(mktime(0,0,0,$aiMesPago,1,$iAnoCargo)), $adFecha) + 1;
				}
			}
			// D Y F= DIAS TRANSCURRIDOS * RECARGO%
			IF ($cCantOPorc3 == 'D' OR $cCantOPorc3 == 'F')
			{
				// IMPORTE * % RECA * DIAS TRANSCURRIDOS DESDE EL li_dia_recargo_3 DEL MES DEL CARGO
				// AL DIA DE HOY
				// OJO: REDONDEA PRIMERO LOS RECARGOS POR D�A PORQUE AS� SE HACE EN LOS BANCOS.
				$dRecargosTotales = fRedondeo(floor($adImporte * $dRecargo3 ) / 100 * $iDias, $dFactorRedondeo);
			// E Y G = DIAS TRANSCURRIDOS * RECARGO$
			}
			ELSE
			{ 
				IF ($cCantOPorc3 == 'E' OR $cCantOPorc3 == 'G')
				{
					//$ RECA * DIAS TRANSCURRIDOS DESDE EL...
					$dRecargosTotales = fRedondeo($dRecargo3 * $iDias, $dFactorRedondeo);
				}
				ELSE
				{
					//$ RECA * SEMANAS TRANSCURRIDAS DESDE EL...
					$dRecargosTotales = fRedondeo($dRecargo3 * ($iDias+6)/7, $dFactorRedondeo);
				}
			}
		}
		}		
			// SI NO SON POR D�A, ACUMULA ADEM�S LOS RECARGOS X MES
	IF	(! ($cCantOPorc1 == 'D' OR $cCantOPorc1 == 'E' OR $cCantOPorc1 == 'F' OR $cCantOPorc1 == 'G' OR	$cCantOPorc2 == 'D' OR $cCantOPorc2 == 'E' OR $cCantOPorc2 == 'F' OR $cCantOPorc2 == 'G' OR $cCantOPorc3 == 'D' OR $cCantOPorc3 == 'E' OR $cCantOPorc3 == 'F' OR $cCantOPorc3 == 'G' ))
	{
		$iMesesAnteriores = 0;
		$iMeses = 0;
		IF ($aiPeriodoPago < $iPeriodoActual)
		{
			// NUMERO DE MESES TRANSCURRIDOS DESDE EL INICIO DEL PERIODO DEL CARGO
			$iMesesAnteriores = $aiMesPago - $iMesInicialPeriodoActual;
			IF ($iMesesAnteriores < 0)
			{
				$iMesesAnteriores = $iMesesAnteriores + 12;
			}
			$iMesesAnteriores = (12 * ($iPeriodoActual - $aiPeriodoPago )) - $iMesesAnteriores;
		}
		IF ($aiPeriodoPago < $iPeriodoActual)
			$iMesesActual = MONTH($adFecha) - $iMesInicialPeriodoActual;
		ELSE
			$iMesesActual = MONTH($adfecha) - $aiMesPago;	
		IF ($iMesesActual < 0 )
		{
			$iMesesActual = $iMesesActual + 12;
		}	
		$iMesesActual ++;
		// MES COMPLETO = 'N' CALCULA A PARTIR DEL D�A EN QUE VENCE EL CARGO
		//					 = 'S' CALCULA A PARTIR DEL D�A 1ERO
		//					 = 'F' CALCULA DESPU�S DE FIN DE MES
		//				OJO: CUANDO ES FIN DE MES, CALCULA LOS RECARGOS MULTIPLICANDO POR
		//					EL N�MERO DE MESES, EN 'N' Y 'S', LOS CALCULA COMO LA SUMA
		//					DE LOS DEL MES M�S LOS DE LOS OTROS MESES
		IF ($cMesCompleto =='N')
		{
			IF (DAY($adfecha) < $iDiaRecargo1);
			{
				$iMesesActual = $iMesesActual - 1;
			}
		}
		ELSE
		{
			IF ($cMesCompleto == 'F')
			{
				$iMesesActual = $iMesesActual - 1;
			}
			$iMeses = $iMesesAnteriores + $iMesesActual;
		// mmm, PERO COMO EL 1ER MES YA NO CUENTA PORQUE AHORA SE SUMAN LOS RECARGOS CALCULADOS
		//		DEL PRIMER MES -> SE RESTA 1
		//		MMMM, SI ES HASTA FIN DE MES SIEMPRE SI CUENTA ;)
		}
		IF ($cMesCompleto <> 'F')
			$iMeses = $iMeses - 1;	
			// POR SI LAS DUDAS. PARA NO REVISARLO TODO
		IF ($iMeses < 0)
			$iMeses = 0;
		// SI NO SE ACUMULAN LOS RECARGOS POR MES -> S�LO APLICA UN MES
		IF ($cAcumularMes == 'N' AND $iMeses > 1)
			$iMeses = 1;
		// SI SE ACUMULAN S�LO 'lc_acumular_mes' MESES, ENTONCES ESE ES EL M�XIMO DE MESES QUE
		//		SE PUEDEN COBRAR
		ELSE
		{
			IF ($cAcumularMes <> 0 )
			{
				$iMeses = MIN(iMeses, INTEGER($cAcumularMes));//////////////////////////
			}
		$recargos = 0;
		}
		IF ($cCantOPorcMes == 'C' )
			$recargos = $iMeses * $dRecargoMes;
		ELSE
		{
			$recargos = $iMeses * $dRecargoMes * $adImporte / 100;
		}	
		//  * RETURN INT(ld_recargos / Paramet->FctRed + .5) * Paramet->FctRed
		// SI ES HASTA FIN DE MES -> NO SE INCLUYEN LOS DEL 1ER MES
		IF ($cMesCompleto <> 'F' )
			$dRecargosTotales = $dRecargosTotales + fredondeo($recargos, $dFactorRedondeo);
		ELSE
		{
			IF ($recargos > 0 )
				$dRecargosTotales = f_redondeo($recargos, gd_factor_redondeo);		
		}	
	}
}}
RETURN $dRecargosTotales;
}
?>