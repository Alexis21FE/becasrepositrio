<?php 
session_start();
include '../admin/z_script/db_class.php';
$funcion=$_REQUEST['funcion'];
$adeudo_inscripcion='';
$query_ciclo="Select * from ciclos";
$res=$pdo->query($query_ciclo);
foreach ($res as $key => $value) {
  $ciclo_config=$value['ciclo_config'];
}
if($ciclo_config=='ac'){
  $table="inf_alum";
  $mes_estado_cc="mes in(8,9,10,11,12,1,2,3,4,5,6)";
  $mes_pago="mes_pago in(8,9,10,11,12,1,2,3,4,5,6)";
}else{
  $table="inf_alum_pp";
  $mes_estado_cc="mes in(8,9,10,11,12,1,2)";
  $mes_pago="mes_pago in(8,9,10,11,12,1,2)";
}
$_SESSION['table']=$table;
$_SESSION['ciclo_config']=$ciclo_config;
$_SESSION['mes_estado_cc']=$mes_estado_cc;
$_SESSION['mes_pago']=$mes_pago;


if($funcion=='login'){
    
    header('Content-type: application/json; charset=utf-8');
   $jsondata = array();   
        $usuario=$_REQUEST['user'];
    $password=$_REQUEST['pass'];
    $tipo_beca=$_REQUEST['tipo_beca'];
     $_SESSION['usuario']=$usuario;
     $_SESSION['tipo_beca']=$tipo_beca;
    $sql_user_pass="select * from user_fam where fam_user='".$usuario."'";
    $res_user=$pdo->query($sql_user_pass);
    foreach($res_user as $row){
        $id_fam=$row['id_fam'];
        $pass_db=$row['fam_pass'];
        $fecha_user_ini=$row['fech_inc'];
        $fecha_user_fin=$row['fech_fin'];
 
    }
    $_SESSION['id_fam']=$id_fam;
    $acceso_fechas='';
    $sql_fechas="Select * from fech_bec";
    $res_fechas=$pdo->query($sql_fechas);
    foreach ($res_fechas as $fechas){
        $fecha_inicio=$fechas['fech_inc'];
        $fecha_fin=$fechas['fech_fin'];
        $fecha_interna_inicio=$fechas['fech_in_pp'];
        $fecha_interna_fin=$fechas['fech_fin_pp'];
        $fecha_apn_inicio=$fechas['fech_in_fn'];
        $fecha_apn_fin=$fechas['fech_fin_fn'];
    }
    $fecha_actual=date('Y-m-d');
    if($fecha_inicio<=$fecha_actual && $fecha_fin>=$fecha_actual){
        $acceso_fechas='S';
    }elseif($fecha_interna_inicio<=$fecha_actual && $fecha_interna_fin>=$fecha_actual){
        $acceso_fechas='S';
    }else{
        $acceso_fechas='N';
    }
if($acceso_fechas!='S'){
 if($fecha_user_ini<=$fecha_actual && $fecha_user_fin>=$fecha_actual){
        $acceso_fechas='S';
    }
}
    
    $sqlA="SELECT ".$table.".* FROM ".$table." WHERE usuario='$usuario'";
 
    $res=$pdo->query($sqlA);
    $num_alumnos_activos=$res->rowCount();
     if($pass_db!=$password){
      $user_pass_correct=false;

    }else{
        
        $user_pass_correct=true;
    }  
    if($num_alumnos_activos!=0 && $acceso_fechas=='S' && $user_pass_correct==true){
        $sql_full_access="Select * from user_fam where fam_user='".$usuario."'";
        $res_full_access=$pdo->query($sql_full_access);
        foreach($res_full_access as $row){
            $full_access=$row['acc_directo'];
        }
        if($full_access=='S'){
            $jsondata['mensaje']="Bienvenido";
           $jsondata['status']=true;
            $jsondata['pass_hijos']="";
           echo json_encode($jsondata);
        }else{
       
           if($tipo_beca=='bec_intof'){
               $res_inscripcion= checar_inscripcion($usuario,$tipo_beca);
               if($res_inscripcion!=true){
                     $jsondata['mensaje']="Aun debe inscripcion";
           $jsondata['status']=false;
            $jsondata['pass_hijos']="";
           
               }else{
                   $res_cargos_abonos=cargos_abonos($usuario, $tipo_beca);
                  
           
                   if($res_cargos_abonos!=true){
             $jsondata['mensaje']="Aun Tiene otros adeudos";
           $jsondata['status']=false; $jsondata['pass_hijos']="";
           
      
                   }else{
                       $jsondata['mensaje']="Bienvenido";
           $jsondata['status']=true;
            $jsondata['pass_hijos']="";
      
                   }
               }
                echo json_encode($jsondata);
           }elseif($tipo_beca=='bec_famnum_pp'){
               $jsondata = array(); 
               if($num_alumnos_activos==3){
                   $porcentaje_beca=.90;
               }elseif($num_alumnos_activos>=4){
                   $porcentaje_beca=.75;
               }
               $res_inscripcion = checar_inscripcion($usuario, $tipo_beca,$porcentaje_beca);
           
               if($res_inscripcion!=true){
                     $jsondata['mensaje']="Aun debe inscripcion";
           $jsondata['status']=false; $jsondata['pass_hijos']="";
           
            
               }else{
                   $res_cargos_abonos=cargos_abonos($usuario, $tipo_beca);
                  
           
                   if($res_cargos_abonos!=true){
             $jsondata['mensaje']="Aun Tiene otros adeudos";
           $jsondata['status']=false; $jsondata['pass_hijos']="";
           
         
                   }else{
                      $_SESSION['porcentaje_apn']=$porcentaje_beca;
                       $jsondata['mensaje']="Bienvenido";
           $jsondata['status']=true; $jsondata['pass_hijos']="";
            }         
         }
               echo json_encode($jsondata);
           }elseif($tipo_beca=='bec_orfandad'){
               $jsondata = array(); 
               $res_cargos_abonos=cargos_abonos($usuario, $tipo_beca);
                  
           
                   if($res_cargos_abonos!=true){
             $jsondata['mensaje']="Aun Tiene otros adeudos";
           $jsondata['status']=false; $jsondata['pass_hijos']="";
           
       
                   }else{
                       $jsondata['mensaje']="Bienvenido";
           $jsondata['status']=true; $jsondata['pass_hijos']="";
                   }
                   echo json_encode($jsondata);
           }
        }
    }else{
        if($user_pass_correct==false){
            $mensaje="Usuario y/o contraseña incorrectos";
      $status=false;
      $aux="Usuario y/o contraseña incorrectos";
        }else{
        $mensaje="Fuera del periodo de solicitud de beca";
        $aux="Fuera del periodo de solicitud de beca";
        }
        $jsondata['mensaje']=$mensaje;
           $jsondata['status']=false;
           $jsondata['pass_hijos']=$aux;
            echo json_encode($jsondata);
    }
}
 function checar_inscripcion($usuario_,$tipo_beca_,$porcentaje_beca_){
  $table=$_SESSION['table'];
                
     include '../admin/z_script/db_class.php';
      $dsn = "mysql:host=$db_server;dbname=$bd_becas";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
 $sqlA="SELECT ".$table.".* FROM ".$table." WHERE usuario='$usuario_'";
    $res=$pdo->query($sqlA);
 $pago='';
 $pago_ant=0;
$monto_pago=0;
$sql_porcentaje_inscripcion="Select * from inscripcion";
$res_sql_porcentaje_inscricpcion=$pdo->query($sql_porcentaje_inscripcion);
foreach($res_sql_porcentaje_inscricpcion as $row){
    $porcentaje_inscrpcion=$row['porcentaje'];
    $porcentaje_inscrpcion_aux=$porcentaje_inscrpcion/100;
   // $porcentaje_inscrpcion=1-$porcentaje_inscrpcion_aux;

}
 
$porcentaje_aux=1-$porcentaje_beca_;

 foreach($res as $row){
     $dsn = "mysql:host=$db_server;dbname=$bd_becas";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
 $monto_pago=0;
 $bd_colegio='';
      $alumno_matricula=$row['alum_mat'];
      $id_fam=$row['id_fam'];
      $colegio_alumno=$row['alum_colg'];
      $sql_bd_colegios="Select * from colegios where nombre='".$colegio_alumno."'";

      $res_colegios=$pdo->query($sql_bd_colegios);
  
     $ciclo_actual=date('Y');
       foreach($res_colegios as $row){
          $bd_colegio=$row['nombre_bd'];

           $sede_alumno=$row['sede'];
      }///////////Revisar inscricpion Interna oficial
  if($colegio_alumno=='Modena'){
     $sql_bd_colegios="Select * from colegios where nombre='".$colegio_alumno."'";

      $res_colegios=$pdo->query($sql_bd_colegios);
  
          foreach($res_colegios as $row){
          $bd_colegio=$row['nombre_bd'];

           $sede_alumno=$row['sede'];
      }
  }

      if($tipo_beca_=='bec_intof'){
 
                if($colegio_alumno=='Los Altos'){


           $dsn = "mysql:host=$db_server;dbname=$bd_colegio";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
$sql_inscripcion_altos="Select * from cargos where concepto in(210,220,230,303,332,333) and periodo=".$ciclo_actual." and pagado!='S' and alumno=".$alumno_matricula."";
$res_inscripcion=$pdo->query($sql_inscripcion_altos);
foreach($res_inscripcion as $row_){
  $importecargomes=$row_['importe'];
}
if($res_inscripcion->rowCount()==0){
  $adeudo_inscripcion='N';
}else{
   $sql_inscripcion_altos="SELECT distinct(familias.familia), sum(importe) as importe FROM abonos, familias, alumnos where familias.familia=alumnos.familia and alumnos.alumno=abonos.alumno and abonos.concepto in(210,220,230,303,332,333) and abonos.periodo_pago=$ciclo_actual 
  and abonos.alumno=$alumno_matricula";
  $res_sql_inscrpcion_altos=$pdo->query($sql_inscripcion_altos);
        foreach($res_sql_inscrpcion_altos as $row){
            $monto_pago=$row['importe']+$monto_pago;
        }
      
            $importe_cargo_mes=$importecargomes*$porcentaje_inscrpcion_aux;
            if($importe_cargo_mes<=$monto_pago){
                $adeudo_inscripcion='N';
                
            }else{
                    $sql_importe_inscricpion="Select * from cargos where alumno=$alumno_matricula and (concepto=210 or concepto=220 or concepto=230 or concepto=303 or concepto=332 or concepto=333) and periodo=".$ciclo_actual." ";
              
                    $res_importe_inscripcion=$pdo->query($sql_importe_inscricpion);
                    foreach($res_importe_inscripcion as $row){
                        $importe_inscricpion=$row['importe'];
                    }
                    $importe_porcentaje=$importe_inscricpion*$porcentaje_aux;
                    if($monto_pago>=$importe_porcentaje){
                        $adeudo_inscripcion='N';
                    }else{
                        $res_pagares=pagares_altos($alumno_matricula);
                        if($res_pagares!=false){
                        $adeudo_inscripcion='N';
                              
                    }else{
                        return false;
                    }
                    }
                    }
}
     
            
        }
        else{
          
            $slq_estado_cuenta="SELECT * FROM estado_de_cuenta where alumno=$alumno_matricula and ciclo_escolar=$ciclo_actual  and id_concepto in(1,2,3,4,16) and pagado!='S' ";
            $res_estado_cuenta=$pdo->query($slq_estado_cuenta);
            $count=$res_estado_cuenta->rowCount();
       
            if($count==0){
              $adeudo_inscripcion='N';
            }else{
              $dsn = "mysql:host=$db_server;dbname=$bd_colegio";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
                    $sql_importe_inscricpion="Select * from cargos where alumno=$alumno_matricula and cargos.concepto in(1,2,3,4,16) and periodo=".$ciclo_actual." ";
                    $res_importe_inscripcion=$pdo->query($sql_importe_inscricpion);
                    foreach($res_importe_inscripcion as $row){
                        $importe_inscricpion=$row['importe'];
                   
                    }
                    $importe_porcentaje=$importe_inscricpion*$porcentaje_inscrpcion_aux;
                
                    if($monto_pago>=$importe_porcentaje){
                        $adeudo_inscripcion='N';
                    }else{

                        $res_carta=carta($alumno_matricula);
                        if($res_carta!=false){
                        $adeudo_inscripcion='N';
                             
                    }else{
                        return false;
                    }
                    }
                    
            }
            }
            $dsn = "mysql:host=$db_server;dbname=$bd_becas";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
          
      }elseif($tipo_beca_=='bec_famnum_pp')////////////////////////Revisar inscricpion Apoyo familia numerosa
          {

          
      $dsn = "mysql:host=$db_server;dbname=$bd_becas";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
          $ciclo_actual=date('Y');
          $sql_importe_afn="select * from importe_afn where alumno=$alumno_matricula";

      $res_importe_afn=$pdo->query($sql_importe_afn);
      foreach($res_importe_afn as $row){
          $importe_afn=$row['importe'];
          $importecargomes=$row['ImporteCargoMes'];
      }
       
      if($colegio_alumno=='Los Altos'){
        $monto_pago=0;
           $dsn = "mysql:host=$db_server;dbname=$bd_colegio";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
        $sql_inscripcion_altos="SELECT distinct(familias.familia), sum(importe) as importe FROM abonos, familias, alumnos where familias.familia=alumnos.familia and alumnos.alumno=abonos.alumno and (abonos.concepto=210  or abonos.concepto=220 or abonos.concepto=230 or abonos.concepto=303 or abonos.concepto=332 or abonos.concepto=333 ) and abonos.periodo_pago=$ciclo_actual 
	and abonos.alumno=$alumno_matricula";
        $res_sql_inscrpcion_altos=$pdo->query($sql_inscripcion_altos);
        foreach($res_sql_inscrpcion_altos as $row){
            $monto_pago=$row['importe']+$monto_pago;
        }
        if($monto_pago>=$importe_afn){
            $adeudo_inscripcion='N';
            
        }else{
            $importe_cargo_mes=$importecargomes*$porcentaje_beca_;
            
            if($importe_cargo_mes>=$monto_pago){
                $adeudo_inscripcion='N';
                
            }else{
                    $sql_importe_inscricpion="Select * from cargos where alumno=$alumno_matricula and (concepto=210 or concepto=220 or concepto=230 or concepto=303 or concepto=332 or concepto=333) and periodo=".$ciclo_actual." ";

                    $res_importe_inscripcion=$pdo->query($sql_importe_inscricpion);
                    foreach($res_importe_inscripcion as $row){
                        $importe_inscricpion=$row['importeConcepto'];
                    }
              
                    $importe_porcentaje=$importe_inscricpion*$porcentaje_inscrpcion_aux;
                    if($monto_pago>=$importe_porcentaje){
                        $adeudo_inscripcion='N';
                    }else{
                        $res_carta=pagares_altos($alumno_matricula);
                        if($res_carta!=false){
                        $adeudo_inscripcion='N';
                              
                    }else{
                        return false;
                    }
                    }
                    }
            }
        }
       else{
              $monto_pago=0;
             $ciclo_actual=date('Y');
           $slq_estado_cuenta_="SELECT * FROM estado_de_cuenta where alumno=$alumno_matricula and ciclo_escolar=$ciclo_actual  and id_concepto in(1,2,3,4,16) and pagado!='S' ";

            $res_estado_cuenta=$pdo->query($slq_estado_cuenta_);
            
            $cuenta=$res_estado_cuenta->rowCount();
            if($cuenta==0){
            $adeudo_inscripcion='N';
            
            }else{

                $dsn = "mysql:host=$db_server;dbname=$bd_colegio";
                
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
            $sql_inscripcion_colegios="SELECT distinct(familias.familia), sum(importe) as importe FROM abonos, familias, alumnos where familias.familia=alumnos.familia and alumnos.alumno=abonos.alumno and abonos.concepto in (1,2,3,4,16) and abonos.periodo_pago=$ciclo_actual 
	and abonos.alumno=$alumno_matricula";
            $res_inscripcion_afn=$pdo->query($sql_inscripcion_colegios);
            foreach($res_inscripcion_afn as $row){
            $monto_pago=$row['importe']+$monto_pago;
            }
            if($monto_pago>=$importe_afn){
               $adeudo_inscripcion='N';
            }else{
                $importe_cargo_mes=$importecargomes*$porcentaje_beca_;

               if($importe_cargo_mes<=$monto_pago){
                $adeudo_inscripcion='N';
            }else{
               $dsn = "mysql:host=$db_server;dbname=$bd_colegio";
               
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
                    $sql_importe_inscricpion_="Select * from cargos where alumno=$alumno_matricula and cargos.concepto in(1,2,3,4,16) and periodo=".$ciclo_actual." ";

                    $res_importe_inscripcion_=$pdo->query($sql_importe_inscricpion_);

                    foreach($res_importe_inscripcion_ as $row__){
          
                        $importe_inscricpion=$row__['importe_concepto'];
                    }
           
                    $importe_porcentaje=$importe_inscricpion*$porcentaje_inscrpcion_aux;

                    if($monto_pago>=$importe_porcentaje){
                        $adeudo_inscripcion='N';
                    }else{
                        $res_carta=carta($alumno_matricula);
                        if($res_carta!=false){
                        $adeudo_inscripcion='N';
                                return true;
                    }else{
                        return false;
                    }
                    }
                    }
            }
          }
            
            $dsn = "mysql:host=$db_server;dbname=$bd_becas";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
      }
 }}
      if($adeudo_inscripcion=='N'){
          return true;
      
 
 
 }}

function carta($alumno_matricula){
  
     include '../admin/z_script/db_class.php';
     $table=$_SESSION['table'];
     $dsn = "mysql:host=$db_server;dbname=$bd_becas";
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
$sql_carta="Select * from ".$table." where alum_mat=".$alumno_matricula."";

$res_carta=$pdo->query($sql_carta);
foreach($res_carta as $alumno_carta){
    $carta=$alumno_carta['carta'];
}
if($carta=='S'){
    return true;
}else{
    return false;
}
}
function cargos_abonos($usuario_,$tipo_beca_){
     include '../admin/z_script/db_class.php';
     $dsn = "mysql:host=$db_server;dbname=$bd_becas";
      $table=$_SESSION['table'];
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
$sqlA="SELECT ".$table.".* FROM ".$table." WHERE usuario='$usuario_'";

    $res=$pdo->query($sqlA);

 $pago='';
 $pago_ant=0;
$monto_pago=0;
foreach ($res as $row){
   
$alumno_matricula=$row['alum_mat'];
      $id_fam=$row['id_fam'];
      $colegio_alumno=$row['alum_colg'];
      $sql_bd_colegios="Select * from colegios where nombre='".$colegio_alumno."'";
      $res_colegios=$pdo->query($sql_bd_colegios);
 
      
       foreach($res_colegios as $row_){
        
          $bd_colegio=$row_['nombre_bd'];
          $sede_=$row_['sede'];
     
      }
if($tipo_beca_=='bec_intof'){
        $res_adeudo=checar_adeudos($alumno_matricula,$usuario_,$bd_colegio,$colegio_alumno,$sede_);
        if($res_adeudo!=true){
      return false;
        }
}elseif($tipo_beca_=='bec_famnum_pp'){
     $res_adeudo=checar_adeudos($alumno_matricula,$id_fam,$bd_colegio,$colegio_alumno,$sede_);
        if($res_adeudo!=true){
        return false;
        }
}elseif($tipo_beca_='bec_orfandad'){
     $res_adeudo=checar_adeudos($alumno_matricula,$id_fam,$bd_colegio,$colegio_alumno,$sede_);
        if($res_adeudo!=true){
     return false;
        }
}
}
if($res_adeudo==true){
    return true;
}
}

function checar_adeudos($alumno_matricula_,$id_fam_,$bd_colegio_,$colegio_alumno_,$sede_){
    include '../admin/z_script/db_class.php';
   $table=$_SESSION['table'];
   $mes_pago=$_SESSION['mes_pago'];
   $mes_estado_cc=$_SESSION['mes_estado_cc'];
  $dsn = "mysql:host=$db_server;dbname=$bd_colegio_";
  $ciclo_actual=date('Y');
  $ciclo_anterior=$ciclo_actual-1;
  $sum_cargos_alumno=0;
$pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

    if($colegio_alumno_=='Los Altos'){
           
            $sql_cargos="Select * from cargos where (periodo between 2014 and $ciclo_anterior) and ".$mes_pago." and alumno=$alumno_matricula_  and cargos.concepto not in(210,220,230,303,332,333) and pagado!='S' and cargos.periodo_pago<2019 ";
         // echo $sql_cargos;
            $res_cargos=$pdo->query($sql_cargos);
       
            foreach ($res_cargos as $row){
                         
                                $sum_cargos_alumno=$row['importe']+$sum_cargos_alumno;
                            }
                        
            
             $sql_cargos="Select * from abonos where (periodo between 2014 and $ciclo_anterior) and ".$mes_pago." and alumno=$alumno_matricula_ and abonos.concepto not in(210,220,230,303,332,333)";
           // echo $sql_cargos;
             $res_abonos=$pdo->query($sql_abonos);
            foreach ($res_abonos as $row){
                $sum_abonos=$row['importe']+$sum_abonos;
            }
             
            if($sum_abonos>=$sum_cargos_alumno){
                return true;
            }else{
                return false;//////////Checar pagares altos
            }
            }else{   
                  
                   
               
               $sql_cargos="Select * from cargos where (periodo between 2014 and $ciclo_anterior) and ".$mes_estado_cc." and alumno=$alumno_matricula_  and cargos.concepto not in (1,2,3,4,16) and pagado!='S' and cargos.periodo_pago<2019";
            $res_cargos=$pdo->query($sql_cargos);
           
            foreach ($res_cargos as $row){
                            if($row['pagado'!='S']){
                                $sum_cargos_alumno=$row['importe']+$sum_cargos_alumno;
                            }
            }
             $sql_cargos="Select * from abonos where (periodo between 2014 and $ciclo_anterior) and alumno=$alumno_matricula_  and abonos.concepto not in (1,2,3,16)";
            $res_abonos=$pdo->query($sql_abonos);
            foreach ($res_abonos as $row){
                $sum_abonos=$row['importe']+$sum_abonos;
            }
            if($sum_abonos>=$sum_cargos_alumno){
                return true;
            }else{
              
               $res=checa_edo_cta($alumno_matricula_,$id_fam_,$sede_);
               if($res!=false){
                return true;
               }else{
                return false;
               }
               }   
               
            
              

           
            
        }
}
function checa_edo_cta($alumno_matricula_,$familia_,$sede_){
 $table=$_SESSION['table'];
 $mes_estado_cc=$_SESSION['mes_estado_cc'];
    include '../admin/z_script/db_class.php';
  $ciclo_actual=date('Y'); 
   $dsn = "mysql:host=$db_server;dbname=$bd_becas";
  $pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
   /*$sql_convenio_adeudo="Select * from convenios_aduedos where alumno=".$alumno_matricula_." and sede=".$sede_." and (ciclo_escolar between 2014 and $ciclo_actual and st not in ('A','P') )";*/
  // echo $sql_convenio_adeudo;
    $sql_edo_cuenta="Select * from estado_de_cuenta where alumno=$alumno_matricula_ and ".$mes_estado_cc." and id_concepto not in (1,2,3,4,16) and (ciclo_escolar between 2014 and ".$ciclo_actual.") and pagado!='S'";

    $res_edo_cta=$pdo->query($sql_edo_cuenta);
    if($res_edo_cta->rowCount()>=1){
   
    
    foreach($res_edo_cta as $row){
    if($row['pagado']!='S'){

        $concepto_id=$row['concepto_id'];
        $concepto_nombre=$row['concepto'];
        $id_convenio=$row['convenio_folio'];
         $res_conv=checa_convenios($concepto_id,$concepto_nombre,$alumno_matricula_,$sede_,$id_convenio);
         if(!$res_conv){
          return false;
         }
    }else{
     return true;
    }
        
    }
    if(res_conv){
      return true;
    }
  }else{
    return true;
  }

  }

    function checa_convenios($concepto_id,$concepto_nombre,$alumno_matricula_,$sede_,$id_convenio){

      include '../admin/z_script/db_class.php';
 $table=$_SESSION['table'];
      if($id_convenio!=''){
       $sql_convenios="Select * from convenios_adeudos where id_convenio=".$id_convenio." and alumno=".$alumno_matricula_." and st in ('A')";
      $res=$pdo_general->query($sql_convenios);
      if($res->rowCount()==0){
        return false;
      }else{
        return true;
      }
      }else{
        return false;
      }
     
    }

    function pagares_altos($alum_mat){
        include '../admin/z_script/db_class.php';
        $bd='ecolmena_losaltos';
           $dsn = "mysql:host=$db_server;dbname=$bd";
  $pdo = new PDO($dsn, $usernamedb, $passworddb, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
  $sql_familias="Select * from pagares where alumno=".$alum_mat." ";
  $res=$pdo->query($sql_familias);
  if($res->rowCount()>=1){
    return true;
  }else{
    return false;
  }

    }
    
?>
