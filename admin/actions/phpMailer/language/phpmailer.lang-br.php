<?php
/**
 * PHPMailer language file.
 * Portuguese Version
 * By Paulo Henrique Garcia - paulo@controllerweb.com.br
 */

$PHPMAILER_LANG = array();
$PHPMAILER_LANG["provide_address"]      = 'Você deve fornecer pelo menos um endereço de destinat&aacute;rio de email.';
$PHPMAILER_LANG["mailer_not_supported"] = ' mailer não suportado.';
$PHPMAILER_LANG["execute"]              = 'Não foi poss&iacute;vel executar: ';
$PHPMAILER_LANG["instantiate"]          = 'Não foi poss&iacute;vel instanciar a função mail.';
$PHPMAILER_LANG["authenticate"]         = 'Erro de SMTP: Não foi poss&iacute;vel autenticar.';
$PHPMAILER_LANG["from_failed"]          = 'Os endereços de rementente a seguir falharam: ';
$PHPMAILER_LANG["recipients_failed"]    = 'Erro de SMTP: Os endereços de destinat&aacute;rio a seguir falharam: ';
$PHPMAILER_LANG["data_not_accepted"]    = 'Erro de SMTP: Dados não aceitos.';
$PHPMAILER_LANG["connect_host"]         = 'Erro de SMTP: Não foi poss&iacute;vel conectar com o servidor SMTP.';
$PHPMAILER_LANG["file_access"]          = 'Não foi poss&iacute;vel acessar o arquivo: ';
$PHPMAILER_LANG["file_open"]            = 'Erro de Arquivo: Não foi poss&iacute;vel abrir o arquivo: ';
$PHPMAILER_LANG["encoding"]             = 'Codificação desconhecida: ';
$PHPMAILER_LANG["signing"]              = 'Signing Error: ';

?>

