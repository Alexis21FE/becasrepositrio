<?php 
session_start();
$_SESSION["var_ban"]=3;

if(!$_SESSION['log_in_adm']) // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}


if($_SESSION["tipo_priv"] != "Administrador") // If the user IS NOT logged in, forward them back to the login page
	{
		header("location: index.php");
	}	
	
$inactive = 1200;
if(isset($_SESSION['start']) ) {
	$session_life = time() - $_SESSION['start'];
	if($session_life > $inactive){
	header("Location: logout.php");
	}else{
		$_SESSION['start'] = time();
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php
            include_once("z_script/header.php");
        ?>
         <!-- Add jQuery library -->
		<script type="text/javascript" src="z_script/fancy_box/lib/jquery-1.10.1.min.js"></script>
    
        <!-- Add mousewheel plugin (this is optional) -->
        <script type="text/javascript" src="z_script/fancy_box/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    
        <!-- Add fancyBox main JS and CSS files -->
        <script type="text/javascript" src="z_script/fancy_box/source/jquery.fancybox.js?v=2.1.5"></script>
        <link rel="stylesheet" type="text/css" href="z_script/fancy_box/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    
        <!-- Add Button helper (this is optional) -->
        <link rel="stylesheet" type="text/css" href="z_script/fancy_box/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
        <script type="text/javascript" src="z_script/fancy_box/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    
        <!-- Add Thumbnail helper (this is optional) -->
        <link rel="stylesheet" type="text/css" href="z_script/fancy_box/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
        <script type="text/javascript" src="z_script/fancy_box/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    
        <!-- Add Media helper (this is optional) -->
        <script type="text/javascript" src="z_script/fancy_box/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
        
        <script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});


		});
	</script>
	</head>
	<body>
    	<?php
		include_once("z_script/menu.php");
		?>
        
		<div class="container_12">

          <div style="clear:both;"></div>
                
                <!-- Example table -->
                <div class="module" >
                <h2><span>Guia para Importación de Información Alumnos Solicitantes de Beca</span></h2>
                <div style=" margin-left:18px; border-right: 1px solid #CFCFCF;">
                	<br /><br />
	        <h4>Preparación del archivo Excel:<br />
	        </h4>
	          <p>1. Abrir un nuevo archivo de Excel</p>
	          
	          <p>2. Ingresamos la información de los alumnos solicitantes de beca empezando en la columna A1 finalizando en la columna J1.</p>
	          
	        
               <a class="fancybox-effects-a" href="images/guia/paso1_alum.png" ><img src="images/guia/paso1_alum_thumb.png" alt="" /></a>
	            
	          <p><strong>Nota: Las únicas celdas con información son desde la Columna A a la J, las demás celdas deberán estar vacias porque si no 
			  generaría un error en la importación del archivo.</strong>	          </p>
	          <p>&nbsp;</p>
	          
	          <p>3. Revisar el archivo que no tenga faltas de ortografía. Si ya todo está revisado nuestro archivo esta listo para la importación.</p>
              <a class="fancybox-effects-a" href="images/guia/paso2_alum.png" ><img src="images/guia/paso2_alum_thumb.png" alt="" /></a>
	          <p>&nbsp;</p>
                
                </div>

                
			</div> <!-- End .grid_12 -->
                
            <!-- Categories list --><!-- End .grid_6 -->
            
            <!-- To-do list --><!-- End .grid_6 -->
          <div style="clear:both;"></div>
            
            <!-- Form elements --><!-- End .grid_12 -->
                
            <!-- Settings-->
            <div class="grid_6">
                 <!-- End .module -->
            </div> <!-- End .grid_6 -->
                
            <!-- Password --><!-- End .grid_6 -->
          <div style="clear:both;"></div><!-- End .grid_3 --><!-- End .grid_3 --><!-- End .grid_6 -->

            
          <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
		
         <?php include_once("z_script/footer.php") ?>
	</body>
</html>