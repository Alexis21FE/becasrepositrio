 <?php
 session_start();
include('conexion.php');
 ?>
 
    <table id="example" class="table table-striped table-bordered table-sm" cellspacing="0" >

            <thead class="thead-dark">
                <tr>
                    <th  class="th-sm">Nombre Familia</th>
                   
                    <th  class="th-sm">Fecha Asignado</th> 
                    <th  class="th-sm">Fecha Realizado</th>
                    <th  class="th-sm">Encuesta</th>
                </tr>
            </thead>
             <tbody>
              
    <?php 
 
         $variable=$pdo->query("SELECT * FROM estudios_familias ef INNER JOIN inf_familia inf ON ef.userfam=inf.usuario AND ef.ciclo=2020 where ef.idempresa=".$_SESSION['idEmpresa']." ");
      
     foreach ($variable as $value) { ?>
    <tr>
    <td><?php echo $value['ft_ap']." ".$value['mt_ap']?></td>              
                
    <td><?php echo $value['fechaAsignado'];?>   </td>              
    <td><?php echo $value['fechaCierre'];?></td>              
    <td><a href="Funciones/formulario.php?usuario=<?php echo $value['usuario'];?>&idFam=<?php echo $value['idFam']?>"><i class="far fa-edit"></i></a></td>              
    </tr>
    
<?php  }

  ?>
        
            </tbody>
           <tfoot>
    <tr>
<th  class="th-sm">Nombre Familia</th>
                    
                    <th  class="th-sm">Fecha Asignado</th> 
                    <th  class="th-sm">Fecha Realizado</th>
                    <th  class="th-sm">Encuesta</th>
    </tr>
  </tfoot>
        </table>

    <script type="text/javascript">
    $('#example').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    } );

    </script>
