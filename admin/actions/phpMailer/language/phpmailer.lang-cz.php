<?php
/**
 * PHPMailer language file.
 * Czech Version
 */

$PHPMAILER_LANG = array();

$PHPMAILER_LANG["provide_address"]      = 'Mus&iacute;te zadat alespoò jednu ' .
                                          'emailovou adresu pø&iacute;jemce.';
$PHPMAILER_LANG["mailer_not_supported"] = ' mailový klient nen&iacute; podporov&aacute;n.';
$PHPMAILER_LANG["execute"]              = 'Nelze prov&eacute;st: ';
$PHPMAILER_LANG["instantiate"]          = 'Nelze vytvoøit instanci emailov&eacute; funkce.';
$PHPMAILER_LANG["authenticate"]         = 'SMTP Error: Chyba autentikace.';
$PHPMAILER_LANG["from_failed"]          = 'N&aacute;sleduj&iacute;c&iacute; adresa From je nespr&aacute;vn&aacute;: ';
$PHPMAILER_LANG["recipients_failed"]    = 'SMTP Error: Adresy pø&iacute;jemcù ' .
                                          'nejsou spr&aacute;vn&eacute; ' .
$PHPMAILER_LANG["data_not_accepted"]    = 'SMTP Error: Data nebyla pøijata';
$PHPMAILER_LANG["connect_host"]         = 'SMTP Error: Nelze nav&aacute;zat spojen&iacute; se ' .
                                          ' SMTP serverem.';
$PHPMAILER_LANG["file_access"]          = 'Soubor nenalezen: ';
$PHPMAILER_LANG["file_open"]            = 'File Error: Nelze otevø&iacute;t soubor pro èten&iacute;: ';
$PHPMAILER_LANG["encoding"]             = 'Nezn&aacute;m&eacute; k&oacute;dov&aacute;n&iacute;: ';
$PHPMAILER_LANG["signing"]              = 'Signing Error: ';

?>
