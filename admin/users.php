<?php
session_start();

if (!$_SESSION['log_in_adm']) { // If the user IS NOT logged in, forward them back to the login page
    header("location: index.php");
}

if ($_SESSION["tipo_priv"] != "Administrador") { // If the user IS NOT logged in, forward them back to the login page
    header("location: index.php");
}

$inactive = 1200;
if (isset($_SESSION['start'])) {
    $session_life = time() - $_SESSION['start'];
    if ($session_life > $inactive) {
        header("Location: logout.php");
    } else {
        $_SESSION['start'] = time();
    }
}

$_SESSION["var_ban"] = 2;
extract($_POST, EXTR_PREFIX_ALL, "x");
$x_band_modf_msg = "";
if (isset($_GET["modf_msg"]))
    $x_band_modf_msg = $_GET["modf_msg"];

$x_band_error_msg = (isset($_GET["red_msg"]) ? $_GET["red_msg"] : "");

$x_band_warnin_msg = "";
if (isset($_GET["yellow_msg"]))
    $x_band_warnin_msg = $_GET["yellow_msg"];

$x_band_alta_msg = "";
if (isset($_GET["add_msg"]))
    $x_band_alta_msg = $_GET["add_msg"];

include("z_script/db_class.php");


//paginador
$sql = "SELECT * FROM adm_users";
if (isset($_GET['page'])) { //verifica pagina
    $page = $_GET['page'];
} else {
    $page = 1;
}


if (isset($x_busc)) {

    if ($x_tip_bus == NULL && $x_input_bus == NULL) {

        $consulta = "SELECT * FROM adm_users";
    } else if ($x_tip_bus == "name") {

        //$consulta="SELECT * FROM adm_users where name LIKE'%".$x_input_bus."%'"; 
        $consulta = "SELECT * FROM `adm_users` where concat_ws(' ',`name`,`apellido_p`,`apellido_m`) LIKE '%" . $x_input_bus . "%'";
    } else if ($x_tip_bus == "colg") {

        $consulta = "SELECT * FROM adm_users where colegio LIKE'%" . $x_input_bus . "%'";
    } else if ($x_tip_bus == "user") {

        $consulta = "SELECT * FROM adm_users where user LIKE'%" . $x_input_bus . "%'";
    } else if ($x_tip_bus == "pass") {

        $consulta = "SELECT * FROM adm_users where pass LIKE'%" . $x_input_bus . "%'";
    }
} else if (isset($x_resg_busc)) {

    $consulta = "SELECT * FROM adm_users";
} else {

    $consulta = "SELECT * FROM adm_users";
}


$datos = $pdo->query($consulta);
$num_rows = $datos->rowCount();
$rows_per_page = 15;
$lastpage = ceil($num_rows / $rows_per_page);
$page = (int) $page;
$band_del = 0;
if ($page > $lastpage) {
    $page = $lastpage;
}

if ($page < 1) {
    $page = 1;
}

$limit = 'LIMIT ' . ($page - 1) * $rows_per_page . ',' . $rows_per_page;
$consulta .= " $limit";
$peliculas = $pdo->query($consulta);

////
?>

<html>
    <head>
<?php
include_once("z_script/header.php");
?>
    </head>
    <body>
<?php
include_once("z_script/menu.php");
?>
        <div class="container_12">



            <!-- Dashboard icons -->
            <!-- End .grid_7 -->

            <div class="grid_12">

                <!-- Notification boxes -->
<?php
if ($x_band_alta_msg == "display") {
    ?>
                    <span class="notification n-success"> Usuario dado de alta exitosamente..</span>
            <?php
        }
        ?>

<?php
if ($x_band_error_msg == "display") {
    ?>
                    <span class="notification n-success"> Usuario eliminado exitosamente.</span>
    <?php
} else if ($x_band_warnin_msg == "display") {
    ?>
                    <span class="notification n-attention">Usuario no eliminado.</span>
                    <?php
                }
                ?>  




                <?php
                if ($x_band_modf_msg == "display") {
                    ?>
                    <span class="notification n-success">Usuario modificado exitosamente.</span>
                    <?php
                }//error
                ?>

                <?php /*
                  <span class="notification n-attention">No se puede eliminar al usuario en uso.</span>
                 */
                ?>
                <!-- Example table --><!-- End .module -->



                <form action="users.php" method="post" enctype="multipart/form-data">
                    <center>  <label>
                            Control de Usuarios
                        </label></center>
                    <hr>
                    <div class="row" style="margin-bottom:1%;">

                        <div class="col">
                            <select class="form-control" id="tip_bus" name="tip_bus">
                                <option value="" selected="selected">Selecciona..</option>
                                <option value="name">Nombre</option>
                                <option value="colg">Colegio</option>
                                <option value="user">Usuario</option>
                                <option value="pass">Contraseña</option>
                            </select>
                        </div>
                        <div class="col"><input class="form-control" name="input_bus" id="input_bus" type="text" /></div>
                        <div class="col">
                            <button class="btn btn-success"  name="busc" id="busc" type='submit'  value="Buscar">Buscar</button></div>
<?php if (isset($x_busc) && $x_tip_bus != NULL && $x_input_bus != NULL) { ?>
                            <div class="col">
                                <button class="btn btn-success" name="resg_busc" id="resg_busc" type='submit'   >Regresar</button>

                            </div> 
<?php } ?>


                </form>







                <div class="col">
          

                        <button class="btn btn-success"  name="enviar" id="exportButton" type='button' >Exportar</button>

                    
                    <a href="add_usuario.php">

                     <button class="btn btn-success" name="nuevo_user" type="button">Nuevo Usuario
 </button>
                    </a>  </div>



            </div>


        </div>



        <div class="module">

            <div class="module-table-body">
                <form action="" name="usuarios">
                    <table id="myTable" class="tablesorter table">
                        <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Colegio</th>
                                <th>Tipo de Usuario</th>
                                <th>Usuario</th>
                                <th>Contraseña</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>

<?php
if ($page == 1) {

    $band_id = 1;
} else {

    $band_id = ($page * $rows_per_page) - ($rows_per_page) + 1;
}

foreach ($peliculas as $row) {
    $x_id = $row["id_user"];
    $x_name = $row["name"];
    $x_apellido_p = $row["apellido_p"];
    $x_apellido_m = $row["apellido_m"];
    $x_colegio = $row["colegio"];
    $x_user = $row["user"];
    $x_pass = $row["pass"];
    $x_tipo_priv = $row["tipo_priv"];
    ?>
                                <tr>
                                    <td class="align-center"><?php echo $band_id; ?></td>
                                    <td><?php echo $x_name; ?> <?php echo $x_apellido_p ?> <?php echo $x_apellido_m; ?></td>
                                    <td><?php echo ($x_colegio); ?></td>
                                    <td><?php echo ($x_tipo_priv); ?></td>
                                    <td><?php echo $x_user; ?></td>
                                    <td><?php echo $x_pass; ?></td>
                                    <td><a href="users_update.php?id=<?php echo $x_id; ?>" title="Editar usuario"><i class="far fa-edit"></i></a>

                                        <a href="actions/users_del.php?id=<?php echo $x_id; ?>" title="Eliminar usuario"><i class="fas fa-user-minus"></i></a>
                                    </td>
                                </tr>
                                <?php
                                $band_id++;
                            }
                            ?>
                        </tbody>
                    </table>
                </form>
                <div style="clear: both"></div>
            </div> <!-- End .module-table-body -->
        </div> <!-- End .module -->
        <div class="">


            <div >  

<?php
//UNA VEZ Q MUESTRO LOS DATOS TENGO Q MOSTRAR EL BLOQUE DE PAGINACIÓN SIEMPRE Y CUANDO HAYA MÁS DE UNA PÁGINA
if ($num_rows != 0) {
    $nextpage = $page + 1;
    $prevpage = $page - 1;
    ?>
                                <?php
                                //SI ES LA PRIMERA PÁGINA DESHABILITO EL BOTON DE PREVIOUS, MUESTRO EL 1 COMO ACTIVO Y MUESTRO EL RESTO DE PÁGINAS
                                if ($page == 1) {
                                    ?>
            <!-- <a href="" class="button"><span><img src="images/arrow-180-small.gif"  height="9" width="12" alt="Previous" /> Anterior</span></a>-->
                    </div><div class="numbers">


                        <span>Paginas:</span> 
                        <span class="current">1</span> 
                        <span>|</span>

        <?php
        for ($i = $page + 1; $i <= $lastpage; $i++) {
            ?> 
                            <a href="users.php?page=<?php echo $i; ?>"><?php echo $i; ?></a> 
                            <span>|</span>
                            <?php
                        }
                        ?>

                        <?php
                        //Y SI LA ULTIMA PÁGINA ES MAYOR QUE LA ACTUAL MUESTRO EL BOTON NEXT O LO DESHABILITO
                        if ($lastpage > $page) {
                            ?>    
                            <a href="users.php?page=<?php echo $nextpage; ?>" class="button"><span>Siguiente <i class="fas fa-arrow-right"></i></span></a> 
                        </div>        
            <?php
        } else {
            ?>
            <!-- <a class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> -->
            <?php
        }
    } else {
        //EN CAMBIO SI NO ESTAMOS EN LA PÁGINA UNO HABILITO EL BOTON DE PREVIUS Y MUESTRO LAS DEMÁS
        ?>
                    <a href="users.php?page=<?php echo $prevpage; ?>" class="button "><span><i class="fas fa-arrow-left"></i>Anterior</span></a>
                    <div class="numbers"> 
                        <span>Paginas:</span>

                        <?php
                        for ($i = 1; $i <= $lastpage; $i++) {
                            //COMPRUEBO SI ES LA PÁGINA ACTIVA O NO
                            if ($page == $i) {
                                ?>     


                                <span class="current"><a><?php echo $i; ?></a></span><span>|</span>

                                <?php
                            } else {
                                ?>


                                <a href="users.php?page=<?php echo $i; ?>"><?php echo $i; ?></a>
                                <span>|</span>

                            <?php
                        }
                    }
                    ?>

        <?php
        //Y SI NO ES LA ÚLTIMA PÁGINA ACTIVO EL BOTON NEXT     
        if ($lastpage > $page) {
            ?>   
                            <a href="users.php?page=<?php echo $nextpage; ?>" class="button"><span>Siguiente <i class="fas fa-arrow-right"></i></span></a> 
                        </div>   

                            <?php
                        } else {
                            ?>  
                    </div> 
                   <!-- <a class="button"><span>Siguiente <img src="images/arrow-000-small.gif" height="9" width="12" alt="Next" /></span></a> -->

                            <?php
                        }
                    }
                }
                ?>

        <div style="clear: both;"></div> 
    </div>




</div> <!-- End .grid_12 -->

<div style="clear:both;"></div>
</div> <!-- End .container_12 -->

                <?php include_once("z_script/footer.php") ?>   

</body>
<script text="text/script">
    
        
</script>
</html>