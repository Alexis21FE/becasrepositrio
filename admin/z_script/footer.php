<!-- Footer -->
        <div id="footer">
        	<div class="container_12">
            	<div class="grid_12">
                	<!-- You can change the copyright line for your own -->
                	<div style="float:left;">2016 &copy; <a href="http://www.colmenares.org.mx/" title="Grupo Colmenares" target="_blank">Grupo Colmenares. Todos los derechos reservados.</a></div>
 <!--                    
 <div style="float:right; width:240px"><div style="float:left;">
<a href="http://orange-eye.com.mx/"><img src="images/orange_eye.png" width="34" height="34" alt="orange_eye" /></a></div>

<div style="margin-top:7px; margin-right:25px; float:right;"><a href="http://orange-eye.com.mx/" target="_blank" style="color:#F60;">Desarrollado por Orange Eye</a></div>
                    </div>
        		</div>-->
            </div>
            <div style="clear:both;"></div>
        </div> <!-- End #footer -->
        <script type="text/javascript">
  $("#exportButton").click(function(){
        $("#myTable").table2excel({

    // exclude CSS class

    exclude: ".noExl",

   name: "Worksheet Name",

    filename: "Reporte.xls" //do not include extension

  });
    });
        $(".interruptor-tecla").click(function(){
          
        if($("#prendido").prop('checked')){
            
           $.ajax({
                url:"actions/funciones.php",
                type:"post",
                dataType:"json",
                data:{
                    funcion:"cambio",
                    cambio_periodo:"periodo_actual"
                },
                success:function(response){
  
                    if(response.status==true){
                        window.location.reload();
                    }
                },
                error:function(){}
            });
        }if($("#apagado").prop('checked')){
            
            $.ajax({
                url:"actions/funciones.php",
                type:"post",
                dataType:"json",
                data:{
                    funcion:"cambio",
                    cambio_periodo:"proximo_periodo"
                },
                success:function(response){
                    console.log(response);
                    if(response.status==true){
                        window.location.reload();
                    }
                },
                error:function(){}
            });
        }
                 
    });

        </script>