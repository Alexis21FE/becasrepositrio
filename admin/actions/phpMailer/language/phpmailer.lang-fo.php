<?php
/**
 * PHPMailer language file.
 * Faroese Version [language of the Faroe Islands, a Danish dominion]
 * This file created: 11-06-2004
 * Supplied by D&aacute;vur Sørensen [www.profo-webdesign.dk]
 */

$PHPMAILER_LANG = array();

$PHPMAILER_LANG["provide_address"]      = 'T&uacute; skal uppgeva minst ' .
                                          'm&oacute;ttakara-emailadressu(r).';
$PHPMAILER_LANG["mailer_not_supported"] = ' er ikki supporterað.';
$PHPMAILER_LANG["execute"]              = 'Kundi ikki &uacute;tføra: ';
$PHPMAILER_LANG["instantiate"]          = 'Kuni ikki instantiera mail funkti&oacute;n.';
$PHPMAILER_LANG["authenticate"]         = 'SMTP feilur: Kundi ikki g&oacute;ðkenna.';
$PHPMAILER_LANG["from_failed"]          = 'fylgjandi Fr&aacute;/From adressa miseydnaðist: ';
$PHPMAILER_LANG["recipients_failed"]    = 'SMTP Feilur: Fylgjandi ' .
                                          'm&oacute;ttakarar miseydnaðust: ';
$PHPMAILER_LANG["data_not_accepted"]    = 'SMTP feilur: Data ikki g&oacute;ðkent.';
$PHPMAILER_LANG["connect_host"]         = 'SMTP feilur: Kundi ikki knýta samband við SMTP vert.';
$PHPMAILER_LANG["file_access"]          = 'Kundi ikki tilganga f&iacute;lu: ';
$PHPMAILER_LANG["file_open"]            = 'F&iacute;lu feilur: Kundi ikki opna f&iacute;lu: ';
$PHPMAILER_LANG["encoding"]             = '&Oacute;kend encoding: ';
$PHPMAILER_LANG["signing"]              = 'Signing Error: ';

?>
